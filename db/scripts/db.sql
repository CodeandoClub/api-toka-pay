create database tokapay;

use tokapay;

create table Tb_SmsCodigos(
	IdSmsCodigo int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion date default null,
    NumeroTelefono varchar(10) not null,
    Codigo varchar(6) not null,
    Validado bit default 0,
    Activo bit default 1,
    primary key(IdSmsCodigo)
) engine = InnoDB;

create table Tb_Errores(
	IdError int auto_increment not null,
    FechaRegistro datetime default now(),
    IdSqlState varchar(6) null,
    MysqlError varchar(500) null,
    Mensaje varchar(500) null,
    SchemaName varchar(50) null,
    TableName varchar(100) null,
    primary key(IdError)
) ENGINE = InnoDB;

DELIMITER $$
drop procedure if exists sp_tp_SMSAgregar$$
create procedure sp_tp_SMSAgregar(
	in _NumeroTelefono varchar(10),
    in _Codigo varchar(6)
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select @sqlstate * (-1) AS Id, @text AS Mensaje;
	end;
	
	start transaction;
    
	insert into tb_smscodigos(
		NumeroTelefono,
        Codigo
    ) values (
		_NumeroTelefono,
        _Codigo
    );
    
    -- Validamos si fue exitosa la transactión
	COMMIT;
	
	SET _Id = LAST_INSERT_ID();
	SELECT _Id AS Id, 'El código se guardo con exito' as Mensaje;
    
END $$
DELIMITER ;

DELIMITER $$
drop procedure if exists sp_tp_ErrorAgregar$$
create procedure sp_tp_ErrorAgregar(
	in _IdSqlState varchar(6),
    in _MysqlError varchar(500),
    in _Mensaje varchar(500),
    in _SchemaName varchar(50),
    in _TableName varchar(100)
)
begin
    declare _Rollback bit default 0;
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
	end;
	
	start transaction;
    
	insert into Tb_Errores(
		IdSqlState,
		MysqlError,
		Mensaje,
		SchemaName,
		TableName
    ) values (
		_IdSqlState,
		left(_MysqlError, 500),
		left(_Mensaje, 500),
		left(_SchemaName, 50),
		left(_TableName, 100)
    );
    
    COMMIT;
    
END $$
DELIMITER ;

DELIMITER $$
drop procedure if exists sp_tp_SMSActualizar$$
create procedure sp_tp_SMSActualizar(
	in _IdSmsCodigo int,
    in _NumeroTelefono varchar(10),
    in _Codigo varchar(6)
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select @sqlstate * (-1) AS Id, @text AS Mensaje;
	end;
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if not exists (select * from tb_smscodigos where Codigo = _Codigo and NumeroTelefono = _NumeroTelefono and Activo = 1) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El código no se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
    -- Obtenemos el id
    set _Id = (select IdSmsCodigo from tb_smscodigos where Codigo = _Codigo and NumeroTelefono = _NumeroTelefono and Activo = 1);
    
	update tb_smscodigos
    set
		Activo = 0,
        Validado = 1,
        FechaActualizacion = now()
    where
		IdSmsCodigo = _Id;
    
    commit;
        
	select _Id as Id, 'El teléfono se valido con exito' as Mensaje;
    
end $$
DELIMITER ;