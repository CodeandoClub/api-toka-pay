DELIMITER $$
drop procedure if exists sp_tp_DireccionesObtener$$
create procedure sp_tp_DireccionesObtener(
	in _IdDireccion int,
    in _CodigoPostal varchar(10)
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		IdDireccion,
        Estado,
        Municipio,
        Ciudad,
        Colonia,
        CodigoPostal
    from Tb_CatDirecciones
    where
		IdDireccion = case
        when _IdDireccion is null then IdDireccion
        else _IdDireccion
        end
        and CodigoPostal = case
        when _CodigoPostal is null then CodigoPostal
        else _CodigoPostal
        end
        and Activo = 1;
    
END $$
DELIMITER ;