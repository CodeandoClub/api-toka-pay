DELIMITER $$
drop procedure if exists sp_tp_ParentescosObtener$$
create procedure sp_tp_ParentescosObtener(
	in _IdParentesco int
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		IdParentesco,
        Nombre
    from Tb_CatParentesco
    where
		IdParentesco = case
        when _IdParentesco is null then IdParentesco
        else _IdParentesco
        end
        and Activo = 1;
    
END $$
DELIMITER ;