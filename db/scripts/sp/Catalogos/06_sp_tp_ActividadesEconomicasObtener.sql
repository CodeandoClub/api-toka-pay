DELIMITER $$
drop procedure if exists sp_tp_ActividadesEconomicasObtener$$
create procedure sp_tp_ActividadesEconomicasObtener(
	in _IdActividadEconomica int
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		IdActividadEconomica,
        Nombre
    from Tb_CatActividadesEconomicas
    where
		IdActividadEconomica = case
        when _IdActividadEconomica is null then IdActividadEconomica
        else _IdActividadEconomica
        end
        and Activo = 1
        and IdActividadEconomica not in (167,168,169,170,171,172);
    
END $$
DELIMITER ;