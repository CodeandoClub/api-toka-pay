DELIMITER $$
drop procedure if exists sp_tp_PaisesObtener$$
create procedure sp_tp_PaisesObtener(
	in _IdPais int,
    in _ClavePais varchar(1)
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		IdPais,
        Nombre,
        ClavePais
    from Tb_CatPaises
    where
		IdPais = case
        when _IdPais is null then IdPais
        else _IdPais
        end
        and ClavePais = case
        when _ClavePais is null then ClavePais
        else _ClavePais
        end
        and Activo = 1;
    
END $$
DELIMITER ;