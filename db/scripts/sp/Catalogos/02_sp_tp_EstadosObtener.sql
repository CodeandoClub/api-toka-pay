DELIMITER $$
drop procedure if exists sp_tp_EstadosObtener$$
create procedure sp_tp_EstadosObtener(
	in _IdEstado int,
    in _ClaveEstado varchar(1)
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		IdEstado,
        Nombre,
        ClaveEstado
    from Tb_CatEstados
    where
		IdEstado = case
        when _IdEstado is null then IdEstado
        else _IdEstado
        end
        and ClaveEstado = case
        when _ClaveEstado is null then ClaveEstado
        else _ClaveEstado
        end
        and Activo = 1;
    
END $$
DELIMITER ;