DELIMITER $$
drop procedure if exists sp_tp_NacionalidadesObtener$$
create procedure sp_tp_NacionalidadesObtener(
	in _IdNacionalidad int,
    in _ClaveNacionalidad varchar(1)
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		IdNacionalidad,
        Nombre,
        ClaveNacionalidad
    from Tb_CatNacionalidades
    where
		IdNacionalidad = case
        when _IdNacionalidad is null then IdNacionalidad
        else _IdNacionalidad
        end
        and ClaveNacionalidad = case
        when _ClaveNacionalidad is null then ClaveNacionalidad
        else _ClaveNacionalidad
        end
        and Activo = 1
        and IdNacionalidad in (73,65,130,52,83,62,59,58);
    
END $$
DELIMITER ;