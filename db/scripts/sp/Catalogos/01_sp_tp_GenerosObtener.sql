DELIMITER $$
drop procedure if exists sp_tp_GenerosObtener$$
create procedure sp_tp_GenerosObtener(
	in _IdGenero int,
    in _ClaveGenero varchar(1)
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		IdGenero,
        Nombre,
        ClaveGenero
    from Tb_CatGeneros
    where
		IdGenero = case
        when _IdGenero is null then IdGenero
        else _IdGenero
        end
        and ClaveGenero = case
        when _ClaveGenero is null then ClaveGenero
        else _ClaveGenero
        end
        and Activo = 1;
    
END $$
DELIMITER ;