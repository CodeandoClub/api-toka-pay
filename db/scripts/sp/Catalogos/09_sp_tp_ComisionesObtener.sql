DELIMITER $$
drop procedure if exists sp_tp_ComisionesObtener$$
create procedure sp_tp_ComisionesObtener(
	in _IdComision int
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		t0.Idcomision,
        t0.Nombre,
        t0.BaseDePago as Base,
        t0.Costo,
        t1.Nombre as Tipo,
        t0.IVA
    from Tb_CatComisiones t0
    inner join Tb_CatTipos t1 on t1.IdTipo = t0.IdTipo
    where
		t0.IdComision = case
        when _IdComision is null then t0.IdComision
        else _IdComision
        end
        and t0.Activo = 1;
    
END $$
DELIMITER ;