DELIMITER $$
drop procedure if exists sp_tp_UsuariosCuentaClabeAgregar$$
create procedure sp_tp_UsuariosCuentaClabeAgregar(
    in _IdUsuario int,
    in _CuentaClabe varchar(17),
    in _DigitoVerificador varchar(1)
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * (-1) as Id, @text as Mensaje;
	end;
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if not exists (select * from Tb_Usuarios where IdUsuario = _IdUsuario and Activo = 1) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El id usuario no se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
    insert into Tb_UsuariosCuentasClabe
    (
        IdUsuario,
        CuentaClabe,
        DigitoVerificador
    ) values (
        _IdUsuario,
        _CuentaClabe,
        _DigitoVerificador
    );
    
    -- Validamos si fue exitosa la transactión
	COMMIT;
	
	SET _Id = LAST_INSERT_ID();
        
	select _Id as Id, 'La cuenta clabe se guardo con exito' as Mensaje;
    
end $$
DELIMITER ;