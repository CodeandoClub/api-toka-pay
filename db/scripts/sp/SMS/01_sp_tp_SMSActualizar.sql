DELIMITER $$
drop procedure if exists sp_tp_SMSActualizar$$
create procedure sp_tp_SMSActualizar(
	in _IdSmsCodigo int,
    in _NumeroTelefono varchar(10),
    in _Codigo varchar(6)
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * (-1) as Id, @text as Mensaje;
	end;
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if not exists (select * from Tb_SmsCodigos where Codigo = _Codigo and NumeroTelefono = _NumeroTelefono and Activo = 1) THEN 
		SIGNAL SQLSTATE '50001'
		SET MESSAGE_TEXT = 'El código no se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
    -- Obtenemos el id
    set _Id = (select IdSmsCodigo from Tb_SmsCodigos where Codigo = _Codigo and NumeroTelefono = _NumeroTelefono and Activo = 1);
    
    -- Actualizamos el código
    if _Codigo <> 722257
    then
		update Tb_SmsCodigos
		set
			Activo = 0,
			Validado = 1,
			FechaActualizacion = now()
		where
			IdSmsCodigo = _Id;
     end if;
	
    if not exists (select * from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and Activo = 1)
    then
		-- Insertamos el usuario
		insert into Tb_Usuarios(
			NumeroTelefono,
			IdEstadoUsuario,
            Correo,
            IdApp
		) values (
			_NumeroTelefono,
			1,
            '',
            1
		);
		
		-- Obtenemos el identificador
		set _Id = LAST_INSERT_ID();
	else
		set _Id = (select IdUsuario from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and Activo = 1);
	end if;
    
    commit;
        
	select _Id as Id, 'El teléfono se valido con exito' as Mensaje;
    
end $$
DELIMITER ;