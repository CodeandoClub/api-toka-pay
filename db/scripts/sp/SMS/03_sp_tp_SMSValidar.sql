DELIMITER $$
drop procedure if exists sp_tp_SMSValidar$$
create procedure sp_tp_SMSValidar(
	in _NumeroTelefono varchar(10)
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if exists (select * from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and IdEstadoUsuario = 7) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El teléfono ya se encuentra registrado y validado.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
    
    set _Id = (select IdUsuario from Tb_Usuarios where NumeroTelefono = _NumeroTelefono);
    
    if _Id is null
    then
		set _ID = 1;
    end if;
    
    SELECT _Id AS Id, 'Puede proceder con el envio.' as Mensaje;
    
END $$
DELIMITER ;