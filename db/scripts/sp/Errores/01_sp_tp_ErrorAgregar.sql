DELIMITER $$
drop procedure if exists sp_tp_ErrorAgregar$$
create procedure sp_tp_ErrorAgregar(
	in _IdSqlState varchar(6),
    in _MysqlError varchar(500),
    in _Mensaje varchar(500),
    in _SchemaName varchar(50),
    in _TableName varchar(100)
)
begin
    declare _Rollback bit default 0;
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
	end;
	
	start transaction;
    
	insert into Tb_Errores(
		IdSqlState,
		MysqlError,
		Mensaje,
		SchemaName,
		TableName
    ) values (
		_IdSqlState,
		left(_MysqlError, 500),
		left(_Mensaje, 500),
		left(_SchemaName, 50),
		left(_TableName, 100)
    );
    
    COMMIT;
    
END $$
DELIMITER ;