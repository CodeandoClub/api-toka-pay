DELIMITER $$
drop procedure if exists sp_tp_CuentasGenerarObtener$$
create procedure sp_tp_CuentasGenerarObtener(
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		NumeroTelefono
    from Tb_Usuarios
    where
		IdEstadoUsuario >= 4
        and CuentaGenerada = 0
        and Activo = 1
        and IdApp = 1;
    
END $$
DELIMITER ;