DELIMITER $$
drop procedure if exists sp_tp_CuentasGenerarAgregar$$
create procedure sp_tp_CuentasGenerarAgregar(
	in _NumeroTelefono varchar(10)
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if not exists (select * from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and Activo = 1) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El número de teléfono no se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
	update Tb_Usuarios
	set CuentaGenerada = 1
	where NumeroTelefono = _NumeroTelefono;
    
    -- Validamos si fue exitosa la transactión
	COMMIT;
	
	SELECT 1 AS Id, 'La cuenta se actualizo con exito' as Mensaje;
    
END $$
DELIMITER ;