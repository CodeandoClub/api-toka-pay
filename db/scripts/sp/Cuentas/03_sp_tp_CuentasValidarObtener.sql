DELIMITER $$
drop procedure if exists sp_tp_CuentasValidarObtener$$
create procedure sp_tp_CuentasValidarObtener(
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		t1.NumeroTelefono
    from Tb_UsuariosMati t0
    inner join Tb_Usuarios t1 on t1.IdUsuario = t0.IdUsuario
    where
		t0.IdEstadoMati = 3
        and t0.NivelActualizado = 0
        and t0.Activo = 1
        and t1.IdApp = 1;
    
END $$
DELIMITER ;