DELIMITER $$
drop procedure if exists sp_tp_UsuariosBeneficiariosActualizar$$
create procedure sp_tp_UsuariosBeneficiariosActualizar(
	in _IdUsuario int,
    in _Nombre varchar(50),
    in _Paterno varchar(50),
    in _Materno varchar(50),
    in _FechaNacimiento datetime,
    in _Correo varchar(150),
    in _NumeroTelefono varchar(10),
    in _IdParentesco int,
    in _Direccion varchar(300),
    in _Activo bit
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
    
    -- ***********************************************************
    -- Limpiamos las variables
    -- ***********************************************************
    
    set _Nombre = upper(nullif(ltrim(rtrim(_Nombre)),''));
    set _Paterno = upper(nullif(ltrim(rtrim(_Paterno)),''));
    set _Materno = upper(nullif(ltrim(rtrim(_Materno)),''));
    set _Correo = upper(nullif(ltrim(rtrim(_Correo)),''));
    set _NumeroTelefono = upper(nullif(ltrim(rtrim(_NumeroTelefono)),''));
    set _Direccion = upper(nullif(ltrim(rtrim(_Direccion)),''));
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if _IdUsuario is not null and not exists (select * from Tb_Usuarios where IdUsuario = _IdUsuario and Activo = 1) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El usuario no se encuentra registrado en sistema.';
	end if;
    
    if _IdParentesco is not null and not exists (select * from Tb_CatParentesco where IdParentesco = _IdParentesco and Activo = 1) THEN 
		SIGNAL SQLSTATE '50001'
		SET MESSAGE_TEXT = 'El parentesco no se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
    -- Validamos si el usuario ya tiene un beneficiario
    set _Id = (select IdBeneficiario from Tb_UsuariosBeneficiarios where IdUsuario and Activo = 1);
    
    if _Id is not null
    then
		update Tb_UsuariosBeneficiarios
		set
			Nombre = case when _Nombre is null then Nombre else _Nombre end,
			Paterno = case when _Paterno is null then Paterno else _Paterno end,
			Materno = case when _Materno is null then Materno else _Materno end,
			FechaNacimiento = case when _FechaNacimiento is null then FechaNacimiento else _FechaNacimiento end,
			Correo = case when _Correo is null then Correo else _Correo end,
			IdParentesco = case when _IdParentesco is null then IdParentesco else _IdParentesco end,
            Direccion = case when _Direccion is null then Direccion else _Direccion end,
            NumeroTelefono = case when _NumeroTelefono is null then NumeroTelefono else _NumeroTelefono end,
            Activo = case when _Activo is null then Activo else _Activo end
		where
			IdBeneficiario = _Id;
	else
		insert into Tb_UsuariosBeneficiarios (
			IdUsuario,
            Nombre,
            Paterno,
            Materno,
            IdParentesco,
            FechaNacimiento,
            Correo,
            NumeroTelefono,
            Direccion,
            Activo
        )
        values
        (
			_IdUsuario,
            _Nombre,
            _Paterno,
            _Materno,
            _IdParentesco,
            _FechaNacimiento,
            _Correo,
            _NumeroTelefono,
            _Direccion,
            1
        );
        
        SET _Id = LAST_INSERT_ID();
    end if;
    
	COMMIT;
	
	SELECT _Id AS Id, 'El beneficiario se actualizo con exito' as Mensaje;
    
END $$
DELIMITER ;