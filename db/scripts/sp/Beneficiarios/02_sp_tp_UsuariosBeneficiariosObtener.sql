DELIMITER $$
drop procedure if exists sp_tp_UsuariosBeneficiariosObtener$$
create procedure sp_tp_UsuariosBeneficiariosObtener(
	in _IdUsuario int
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		IdUsuario,
		Nombre,
		Paterno,
		Materno,
		IdParentesco,
		FechaNacimiento,
		Correo,
		NumeroTelefono,
		Direccion
    from Tb_UsuariosBeneficiarios
    where
		IdUsuario = _IdUsuario
        and Activo = 1;
    
END $$
DELIMITER ;