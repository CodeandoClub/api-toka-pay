DELIMITER $$
drop procedure if exists sp_tp_UsuariosAgregarFirmaElectronica$$
create procedure sp_tp_UsuariosAgregarFirmaElectronica(
    in _NumeroTelefono varchar(10),
    in _IdUsuario int,
    in _FirmaElectronica varchar(500)
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * (-1) as Id, @text as Mensaje;
	end;
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if not exists (select * from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and Activo = 1) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El número de telefono no se encuentra registrado en sistema.';
	end if;
    
    if not exists (select * from Tb_Usuarios where IdUsuario = _IdUsuario and Activo = 1) THEN 
		SIGNAL SQLSTATE '50001'
		SET MESSAGE_TEXT = 'El id usuario no se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
    -- Validamos si ya existe la firma
    if exists (select * from Tb_UsuariosFirmaElectronica where IdUsuario = _IdUsuario)
    then
		-- Obtenemos el Id
        set _Id = (select IdUsuarioFirmaElectronica from Tb_UsuariosFirmaElectronica where IdUsuario = _IdUsuario);
        
        update Tb_UsuariosFirmaElectronica
        set FirmaElectronica = _FirmaElectronica
        where IdUsuarioFirmaElectronica = _Id;
        
    else
		insert into Tb_UsuariosFirmaElectronica
		(
			FirmaElectronica,
			IdUsuario,
			NumeroTelefono
		) values (
			_FirmaElectronica,
			_IdUsuario,
			_NumeroTelefono
		);
        
        SET _Id = LAST_INSERT_ID();
    end if;
    
    -- Validamos si fue exitosa la transactión
	COMMIT;
        
	select _Id as Id, 'La firma se guardo con exito' as Mensaje;
    
end $$
DELIMITER ;