DELIMITER $$
drop procedure if exists sp_tp_UsuariosActualizarEstado$$
create procedure sp_tp_UsuariosActualizarEstado(
    in _NumeroTelefono varchar(10),
    in _IdUsuario int,
    in _IdEstadoUsuario int
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * (-1) as Id, @text as Mensaje;
	end;
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if not exists (select * from Tb_CatEstadosUsuarios where IdEstadoUsuario = _IdEstadoUsuario) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El IdEstado no se encuentra registrado en sistema.';
	end if;
    
    if not exists (select * from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and Activo = 1) THEN 
		SIGNAL SQLSTATE '50001'
		SET MESSAGE_TEXT = 'El usuario no se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
    update Tb_Usuarios
    set IdEstadoUsuario = _IdEstadoUsuario
    where NumeroTelefono = _NumeroTelefono and Activo = 1;
    
    commit;
    
    -- Obtenemos el IdUsuario
    if _IdUsuario is null
    then
		set _IdUsuario = (select IdUsuario from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and Activo = 1);
    end if;
        
	select _IdUsuario as Id, 'El estado se actualizo con exito' as Mensaje;
    
end $$
DELIMITER ;