DELIMITER $$
drop procedure if exists sp_tp_UsuariosObtenerEstado$$
create procedure sp_tp_UsuariosObtenerEstado(
	in _NumeroTelefono varchar(10),
    in _IdUsuario int
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		IdEstadoUsuario
    from Tb_Usuarios
    where
		NumeroTelefono = case
        when _NumeroTelefono is null then NumeroTelefono
        else _NumeroTelefono
        end
        and IdUsuario = case
        when _IdUsuario is null then IdUsuario
        else _IdUsuario
        end
        and Activo = 1
        and IdApp = 1;
    
END $$
DELIMITER ;