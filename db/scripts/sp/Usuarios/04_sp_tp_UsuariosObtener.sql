DELIMITER $$
drop procedure if exists sp_tp_UsuariosObtener$$
create procedure sp_tp_UsuariosObtener(
	in _NumeroTelefono varchar(10),
    in _Correo varchar(150),
    in _IdUsuario int
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		t0.IdUsuario,
        t0.Nombre,
		t0.Paterno,
		t0.Materno,
		t0.FechaNacimiento,
		t0.Correo,
        t0.NumeroTelefono,
        t0.IdEstado,
        t2.ClaveEstado,
		t0.IdGenero,
        t1.ClaveGenero,
		t0.IdNacionalidad,
		t0.IdActividadEconomica,
        t0.IdEstadoUsuario,
        t0.Contrasenia,
        t0.Salt,
        t0.IdNivel,
        concat(t3.CuentaClabe,t3.DigitoVerificador) as CuentaClabe,
        t4.IdNivelPendiente
    from Tb_Usuarios t0
    left join Tb_CatGeneros t1 on t1.IdGenero = t0.IdGenero
    left join Tb_CatEstados t2 on t2.IdEstado = t0.IdEstado
    left join Tb_UsuariosCuentasClabe t3 on t3.IdUsuario = t0.IdUsuario
    left join Tb_UsuariosMati t4 on t4.IdUsuario = t0.IdUsuario
    where
		t0.NumeroTelefono = case when _NumeroTelefono is null then t0.NumeroTelefono else _NumeroTelefono end
        and t0.Correo = case when _Correo is null then t0.Correo else _Correo end
        and t0.IdUsuario = case when _IdUsuario is null then t0.IdUsuario else _IdUsuario end
        and t0.Activo = 1
        and t0.IdApp = 1;
    
END $$
DELIMITER ;