DELIMITER $$
drop procedure if exists sp_tp_UsuariosActualizarPassword$$
create procedure sp_tp_UsuariosActualizarPassword(
	in _NumeroTelefono varchar(10),
    in _Salt varchar(150),
    in _Password varchar(150)
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
    
    -- ***********************************************************
    -- Limpiamos las variables
    -- ***********************************************************
    
    set _NumeroTelefono = upper(nullif(ltrim(rtrim(_NumeroTelefono)),''));
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if _NumeroTelefono is not null and not exists (select * from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and Activo = 1) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El número de teléfono no se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
	update Tb_Usuarios
    set
		Contrasenia = case when _Password is null then Contrasenia else _Password end,
		Salt = case when _Salt is null then Salt else _Salt end
	where
		NumeroTelefono = _NumeroTelefono;
	COMMIT;
    
    -- Obtenemos el IdUsuario
    set _Id = (select IdUsuario from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and Activo = 1);
	
	SELECT _Id AS Id, 'La contraseña se actualizo con exito' as Mensaje;
    
END $$
DELIMITER ;