DELIMITER $$
drop procedure if exists sp_tp_UsuariosActualizar$$
create procedure sp_tp_UsuariosActualizar(
    in _Nombre varchar(50),
    in _Paterno varchar(50),
    in _Materno varchar(50),
    in _FechaNacimiento datetime,
    in _Correo varchar(150),
    in _NumeroTelefono varchar(10),
    in _IdGenero int,
    in _IdNacionalidad int,
    in _IdEstado int,
    in _IdActividadEconomica int
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
    
    -- ***********************************************************
    -- Limpiamos las variables
    -- ***********************************************************
    
    set _Nombre = upper(nullif(ltrim(rtrim(_Nombre)),''));
    set _Paterno = upper(nullif(ltrim(rtrim(_Paterno)),''));
    set _Materno = upper(nullif(ltrim(rtrim(_Materno)),''));
    set _Correo = upper(nullif(ltrim(rtrim(_Correo)),''));
    set _NumeroTelefono = upper(nullif(ltrim(rtrim(_NumeroTelefono)),''));
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if _IdGenero is not null and not exists (select * from Tb_CatGeneros where IdGenero = _IdGenero and Activo = 1) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El género no se encuentra registrado en sistema.';
	end if;
    
    if _IdEstado is not null and not exists (select * from Tb_CatEstados where IdEstado = _IdEstado and Activo = 1) THEN 
		SIGNAL SQLSTATE '50001'
		SET MESSAGE_TEXT = 'El estado no se encuentra registrado en sistema.';
	end if;
    
    if _IdNacionalidad is not null and not exists (select * from Tb_CatNacionalidades where IdNacionalidad = _IdNacionalidad and Activo = 1) THEN 
		SIGNAL SQLSTATE '50003'
		SET MESSAGE_TEXT = 'La nacionalidad no se encuentra registrado en sistema.';
	end if;
    
    if _IdActividadEconomica is not null and not exists (select * from Tb_CatActividadesEconomicas where IdActividadEconomica = _IdActividadEconomica and Activo = 1) THEN 
		SIGNAL SQLSTATE '50005'
		SET MESSAGE_TEXT = 'La actividad economica no se encuentra registrado en sistema.';
	end if;
    
    if _NumeroTelefono is not null and not exists (select * from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and Activo = 1) THEN 
		SIGNAL SQLSTATE '50006'
		SET MESSAGE_TEXT = 'El número de teléfono no se encuentra registrado en sistema.';
	end if;
    
    if _Correo is not null and exists (select * from Tb_Usuarios where NumeroTelefono <> _NumeroTelefono and Correo = _Correo and Activo = 1) THEN 
		SIGNAL SQLSTATE '50007'
		SET MESSAGE_TEXT = 'El correo eléctronico ya se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
	update Tb_Usuarios
    set
		Nombre = case when _Nombre is null then Nombre else _Nombre end,
		Paterno = case when _Paterno is null then Paterno else _Paterno end,
		Materno = case when _Materno is null then Materno else _Materno end,
		FechaNacimiento = case when _FechaNacimiento is null then FechaNacimiento else _FechaNacimiento end,
		Correo = case when _Correo is null then Correo else _Correo end,
        IdEstado = case when _IdEstado is null then IdEstado else _IdEstado end,
		IdGenero = case when _IdGenero is null then IdGenero else _IdGenero end,
		IdNacionalidad = case when _IdNacionalidad is null then IdNacionalidad else _IdNacionalidad end,
		IdActividadEconomica = case when _IdActividadEconomica is null then IdActividadEconomica else _IdActividadEconomica end
	where
		NumeroTelefono = _NumeroTelefono;
	COMMIT;
    
    -- Obtenemos el IdUsuario
    set _Id = (select IdUsuario from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and Activo = 1);
	
	SELECT _Id AS Id, 'El usuario se actualizo con exito' as Mensaje;
    
END $$
DELIMITER ;