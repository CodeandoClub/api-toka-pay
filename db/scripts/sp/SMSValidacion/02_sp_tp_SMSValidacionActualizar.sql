DELIMITER $$
drop procedure if exists sp_tp_SMSValidacionActualizar$$
create procedure sp_tp_SMSValidacionActualizar(
	in _IdSmsCodigo int,
    in _NumeroTelefono varchar(10),
    in _Codigo varchar(6)
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * (-1) as Id, @text as Mensaje;
	end;
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if not exists (select * from Tb_SmsValidacionCodigos where Codigo = _Codigo and NumeroTelefono = _NumeroTelefono and Activo = 1) THEN 
		SIGNAL SQLSTATE '50001'
		SET MESSAGE_TEXT = 'El código no se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
    -- Obtenemos el id
    set _Id = (select IdSmsCodigo from Tb_SmsValidacionCodigos where Codigo = _Codigo and NumeroTelefono = _NumeroTelefono and Activo = 1);
    
    -- Actualizamos el código
	update Tb_SmsValidacionCodigos
	set
		Activo = 0,
		Validado = 1,
		FechaActualizacion = now()
	where
		IdSmsCodigo = _Id;
	
    -- Obtenemos el Id Usuario
	set _Id = (select IdUsuario from Tb_Usuarios where NumeroTelefono = _NumeroTelefono and Activo = 1);
    
    commit;
        
	select _Id as Id, 'El código se valido con exito' as Mensaje;
    
end $$
DELIMITER ;