DELIMITER $$
drop procedure if exists sp_tp_SMSValidacionAgregar$$
create procedure sp_tp_SMSValidacionAgregar(
	in _NumeroTelefono varchar(10),
    in _Codigo varchar(6)
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	start transaction;
    
    -- Validamos si hay otro código activo
    set _Id = (select IdSmsCodigo from Tb_SmsValidacionCodigos where NumeroTelefono = _NumeroTelefono and Activo = 1);
		
    if _Id is not null
    then
		-- Desactivamos el còdigo anterior
        update Tb_SmsValidacionCodigos
        set Activo = 0
        where IdSmsCodigo = _Id;
    end if;
    
	insert into Tb_SmsValidacionCodigos(
		NumeroTelefono,
        Codigo
    ) values (
		_NumeroTelefono,
        _Codigo
    );
    
    SET _Id = LAST_INSERT_ID();
    
    -- Validamos si fue exitosa la transactión
	COMMIT;
    
	SELECT _Id AS Id, 'El código se guardo con exito' as Mensaje;
    
END $$
DELIMITER ;