DELIMITER $$
drop procedure if exists sp_tp_UsuariosMatiObtener$$
create procedure sp_tp_UsuariosMatiObtener(
	in _MatiUser varchar(50)
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		t0.IdUsuario,
        t0.Nombre,
		t0.IdApp,
        t0.MatiId
    from Tb_UsuariosMati t0
    where
		t0.MatiUser = _MatiUser
        and t0.Activo = 1;
    
END $$
DELIMITER ;