DELIMITER $$
drop procedure if exists sp_tp_MatiActualizar$$
create procedure sp_tp_MatiActualizar(
    in _MatiId varchar(50),
    in _MensajeMati text,
    in _IdEstadoMati int,
    in _UrlVerification varchar (250)
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    declare _IdMati int;
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * (-1) as Id, @text as Mensaje;
	end;
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if not exists (select * from Tb_UsuariosMati where MatiId = _MatiId and Activo = 1) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El id Mati no se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
    -- Validamos si ya existe un registro de mati
    set _IdMati = (select IdUsuarioMati from Tb_UsuariosMati where MatiId = _MatiId and Activo = 1);
    
    update Tb_UsuariosMati
    set 
		IdEstadoMati = _IdEstadoMati,
		IdEstadoMati = _IdEstadoMati,
        Mensaje = _MensajeMati,
        UrlVerification = _UrlVerification
	where
		MatiId = _MatiId;
    
    -- Validamos si fue exitosa la transactión
	COMMIT;
        
	select _IdMati as Id, 'El registro de Mati se actualizo con exito' as Mensaje;
    
end $$
DELIMITER ;