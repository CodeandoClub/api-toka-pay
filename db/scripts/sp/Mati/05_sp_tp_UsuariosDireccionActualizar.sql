DELIMITER $$
drop procedure if exists sp_tp_UsuariosDireccionActualizar$$
create procedure sp_tp_UsuariosDireccionActualizar(
	in _IdUsuario int,
    in _Calle varchar(100),
    in _NumeroExterior varchar(50),
    in _NumeroInterior varchar(50),
    in _IdDireccion int
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
    
    -- ***********************************************************
    -- Limpiamos las variables
    -- ***********************************************************
    
    set _Calle = upper(nullif(ltrim(rtrim(_Calle)),''));
    set _NumeroExterior = upper(nullif(ltrim(rtrim(_NumeroExterior)),''));
    set _NumeroInterior = upper(nullif(ltrim(rtrim(_NumeroInterior)),''));
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    if _IdUsuario is not null and not exists (select * from Tb_Usuarios where IdUsuario = _IdUsuario and Activo = 1) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El usuario no se encuentra registrado en sistema.';
	end if;
    
    if _IdDireccion is not null and not exists (select * from Tb_CatDirecciones where IdDireccion = _IdDireccion and Activo = 1) THEN 
		SIGNAL SQLSTATE '50001'
		SET MESSAGE_TEXT = 'El id dirección no se encuentra registrado en sistema.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
    -- Validamos si el usuario ya tiene un beneficiario
    set _Id = (select IdUsuarioDireccion from Tb_UsuariosDireccion where IdUsuario and Activo = 1);
    
    if _Id is not null
    then
		update Tb_UsuariosDireccion
		set
			Calle = case when _Calle is null then Calle else _Calle end,
			NumeroExterior = case when _NumeroExterior is null then NumeroExterior else _NumeroExterior end,
			NumeroInterior = case when _NumeroInterior is null then NumeroInterior else _NumeroInterior end,
			IdDireccion = case when _IdDireccion is null then IdDireccion else _IdDireccion end
		where
			IdUsuarioDireccion = _Id;
	else
		insert into Tb_UsuariosDireccion (
			IdUsuario,
            Calle,
            NumeroExterior,
            NumeroInterior,
            IdDireccion,
            Activo
        )
        values
        (
			_IdUsuario,
            _Calle,
            _NumeroExterior,
            _NumeroInterior,
            _IdDireccion,
            1
        );
        
        SET _Id = LAST_INSERT_ID();
    end if;
    
	COMMIT;
	
	SELECT _Id AS Id, 'La dirección se actualizo con exito' as Mensaje;
    
END $$
DELIMITER ;