DELIMITER $$
drop procedure if exists sp_tp_MatiDocumentoAgregar$$
create procedure sp_tp_MatiDocumentoAgregar(
	in _IdUsuario int,
    in _IdApp int,
    in _MatiId varchar(50),
    in _IdDocumento int,
    in _IdEstadoDocumento int,
    in _Mensaje text
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    declare _IdDocumentoMati int;
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * (-1) as Id, @text as Mensaje;
	end;
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    -- Verificación para tokapay
    if _IdApp = 1 and not exists (select * from Tb_Usuarios where IdUsuario = _IdUsuario and Activo = 1) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El id usuario no se encuentra registrado en sistema.';
	end if;
    
    -- Verificación externos
    if _IdApp <> 1 and not exists (select * from Tb_Usuarios where IdUsuarioExterno = _IdUsuario and Activo = 1 and IdApp = _IdApp) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El id usuario no se encuentra registrado en sistema.';
    end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
    -- Obtenemos el usuario externo
    
    if _IdApp <> 1 THEN 
		set _IdUsuario = (select IdUsuario from Tb_Usuarios where IdUsuarioExterno = _IdUsuario and Activo = 1 and IdApp = _IdApp);
    end if;
    
    -- Validamos si ya existe un registro de mati
    set _IdDocumentoMati = (select IdUsuarioMatiDocumento from Tb_UsuariosMatiDocumentos where IdUsuario = _IdUsuario and Activo = 1 and IdApp = _IdApp and IdDocumento = _IdDocumento);
    
    if _IdDocumentoMati is null
    then
		insert into Tb_UsuariosMatiDocumentos
		(
			IdUsuario,
			IdApp,
			MatiId,
			IdDocumento,
			Mensaje,
			IdEstadoDocumento
		) values (
			_IdUsuario,
            _IdApp,
            _MatiId,
            _IdDocumento,
            _Mensaje,
            _IdEstadoDocumento
		);
        
		SET _Id = LAST_INSERT_ID();
	else
    
		update Tb_UsuariosMatiDocumentos
        set
			MatiId = _MatiId,
			Mensaje = _Mensaje,
			IdEstadoDocumento = _IdEstadoDocumento,
            FechaActualizacion = now()
		where
			IdUsuarioMatiDocumento = _IdDocumentoMati;
            
		set _Id = _IdDocumentoMati;
    
    end if;
    
    -- Validamos si fue exitosa la transactión
	COMMIT;
        
	select _Id as Id, 'El documento se guardo con exito' as Mensaje;
    
end $$
DELIMITER ;