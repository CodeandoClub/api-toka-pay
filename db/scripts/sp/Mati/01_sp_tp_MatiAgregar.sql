DELIMITER $$
drop procedure if exists sp_tp_MatiAgregar$$
create procedure sp_tp_MatiAgregar(
    in _IdUsuario int,
    in _IdApp int,
    in _Nombre varchar(150),
    in _MatiUser varchar(50),
    in _MatiId varchar(50),
    in _MatiFlowId varchar(50),
    in _IdNivelPendiente int
)
begin
	declare _Id int;
	declare _Mensaje varchar(300);
    declare _IdMati int;
    
    -- ***********************************************************
    -- Manejador de errores
    -- ***********************************************************
    
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		
        -- Hacemos rollback
        if @sqlstate not between 50000 and 50100
        then
			rollback;
        end if;
        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * (-1) as Id, @text as Mensaje;
	end;
    
    -- ***********************************************************
    -- Inicio de validaciones
    -- ***********************************************************
    
    -- Validación para tokapay
    if _IdApp = 1 and not exists (select * from Tb_Usuarios where IdUsuario = _IdUsuario and Activo = 1) THEN 
		SIGNAL SQLSTATE '50000'
		SET MESSAGE_TEXT = 'El id usuario no se encuentra registrado en sistema.';
	end if;
    
    if _IdNivelPendiente = 1 THEN 
		SIGNAL SQLSTATE '50001'
		SET MESSAGE_TEXT = 'El id nivel de cuenta debe ser 2 o 3.';
	end if;
    
    -- ***********************************************************
    -- Fin de validaciones
    -- ***********************************************************
	
	start transaction;
    
    -- Validamos si existe el usuario en mozper
    if _IdApp <> 1 and not exists (select * from Tb_Usuarios where IdUsuarioExterno = _IdUsuario and Activo = 1) THEN 
		if not exists (select * from Tb_Usuarios where IdUsuarioExterno = _IdUsuario and Activo = 1 and IdApp = _IdApp) THEN 
			-- Insertamos usuario para Mati
			insert into Tb_Usuarios (Nombre, IdApp, IdUsuarioExterno) values (substring(_Nombre, 1, case when length(_Nombre) > 50 then 49 else length(_Nombre) end), _IdApp, _IdUsuario);
			
			-- Obtenemos el IdUsuarioMati
			set _IdUsuario = LAST_INSERT_ID();
		else
			-- Obtenemos el ID
            set _Id = (select IdUsuario from Tb_Usuarios where IdUsuarioExterno = _IdUsuario and Activo = 1 and IdApp = _IdApp);
        end if;
    end if;
    
    -- Validamos si ya existe un registro de mati
    set _IdMati = (select IdUsuarioMati from Tb_UsuariosMati where IdUsuario = _IdUsuario and Activo = 1 and IdApp = _IdApp);
    
    if _IdMati is null
    then
		insert into Tb_UsuariosMati
		(
			IdUsuario,
			IdApp,
			Nombre,
			MatiUser,
			MatiId,
			MatiFlowId,
			IdEstadoMati,
			IdNivelPendiente
		) values (
			_IdUsuario,
			_IdApp,
			_Nombre,
			_MatiUser,
			_MatiId,
			_MatiFlowId,
			1,
			_IdNivelPendiente
		);
        
		SET _Id = LAST_INSERT_ID();
	else
    
		update Tb_UsuariosMati
        set
			Nombre = _Nombre,
			MatiUser = _MatiUser,
			MatiId = _MatiId,
			MatiFlowId = _MatiFlowId,
			IdEstadoMati = 1,
			IdNivelPendiente = _IdNivelPendiente
		where
			IdUsuarioMati = _IdMati;
            
		set _Id = _IdMati;
    
    end if;
    
    -- Validamos si fue exitosa la transactión
	COMMIT;
        
	select _Id as Id, 'El registro de Mati se guardo con exito' as Mensaje;
    
end $$
DELIMITER ;