DELIMITER $$
drop procedure if exists sp_tp_UsuariosAccesosObtener$$
create procedure sp_tp_UsuariosAccesosObtener(
	in _IdUsuario int
)
begin
	declare exit handler for SQLEXCEPTION
	begin
		-- Obtenemos los detalles del error
		GET DIAGNOSTICS CONDITION 1
        @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT, @schema = SCHEMA_NAME, @table = TABLE_NAME;
		        
        call sp_tp_ErrorAgregar(@sqlstate, @errno, @text, @schema, @table);
        
        select cast(@sqlstate as signed) * -1 AS Id, @text AS Mensaje;
	end;
	
	select
		t0.IdUsuario,
        t1.FechaRegistro as UltimoAcceso,
        t1.Latitud,
        t1.Longitud
    from Tb_Usuarios t0
    left join Tb_UsuariosAccesos t1 on t1.IdUsuario = t0.IdUsuario
    where
		t0.IdUsuario = _IdUsuario
        and t0.Activo = 1
        and t0.IdApp = 1
	order by t1.FechaRegistro desc;
    
END $$
DELIMITER ;