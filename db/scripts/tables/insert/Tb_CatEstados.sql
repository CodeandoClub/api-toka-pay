insert into Tb_CatEstados (Nombre,ClaveEstado) values ('AGUASCALIENTES','AS');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('BAJA CALIFORNIA','BC');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('BAJA CALIFORNIA SUR','BS');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('CAMPECHE','CC');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('CHIAPAS','CS');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('CHIHUAHUA','CH');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('CIUDAD DE MEXICO','DF');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('COAHUILA','CL');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('COLIMA','CM');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('DURANGO','DG');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('GUANAJUATO','GT');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('GUERRERO','GR');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('HIDALGO','HG');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('JALISCO','JC');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('MÉXICO','MC');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('MICHOACÁN','MN');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('MORELOS','MS');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('NAYARIT','NT');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('NUEVO LEÓN','NL');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('OAXACA','OC');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('PUEBLA','PL');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('QUERÉTARO','QO');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('QUINTANA ROO','QR');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('SAN LUIS POTOSÍ','SP');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('SINALOA','SL');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('SONORA','SR');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('TABASCO','TC');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('TAMAULIPAS','TS');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('TLAXCALA','TL');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('VERACRUZ','VZ');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('YUCATÁN','YN');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('ZACATECAS','ZS');
insert into Tb_CatEstados (Nombre,ClaveEstado) values ('NACIDO EN EL EXTRAJERO','NE');