create table Tb_CatDocumentosApp(
	IdDocumento int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    Nombre varchar(50) not null,
    Activo bit default 1,
    primary key(IdDocumento)
) engine = InnoDB;