create table Tb_Usuarios(
	IdUsuario int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    Nombre varchar(50) null,
    Paterno varchar(50) null,
    Materno varchar(50) null,
    FechaNacimiento datetime null,
    Correo varchar(150) null,
    NumeroTelefono varchar(10) null,
    IdGenero int null,
    IdNacionalidad int null,
    IdEstado int null,
    IdActividadEconomica int null,
    IdEstadoUsuario int null,
    Activo bit default 1,
    index(IdGenero),
    index(IdNacionalidad),
    index(IdEstado),
    index(IdActividadEconomica),
    index(IdEstadoUsuario),
    foreign key(IdGenero) references Tb_CatGeneros(IdGenero),
    foreign key(IdNacionalidad) references Tb_CatNacionalidades(IdNacionalidad),
    foreign key(IdEstado) references Tb_CatEstados(IdEstado),
    foreign key(IdActividadEconomica) references Tb_CatActividadesEconomicas(IdActividadEconomica),
    foreign key(IdEstadoUsuario) references Tb_CatEstadosUsuarios(IdEstadoUsuario),
    primary key(IdUsuario)
) engine = InnoDB;

alter table Tb_Usuarios add column Contrasenia varchar(150) null;
alter table Tb_Usuarios add column Salt varchar(100) null;
alter table Tb_Usuarios add column CuentaGenerada bit default 0;
alter table Tb_Usuarios add column IdNivel int default 1;
alter table Tb_Usuarios add column IdApp int default 1;
alter table Tb_Usuarios add column IdUsuarioExterno int default null;