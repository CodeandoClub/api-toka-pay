create table Tb_CatTipos(
	IdTipo int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    Nombre varchar(50) null,
    Activo bit default 1,
    primary key(IdTipo)
) engine = InnoDB;