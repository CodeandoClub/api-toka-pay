create table Tb_UsuariosMati(
	IdUsuarioMati int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    IdUsuario int not null,
    IdApp int not null,
    IdNivelPendiente int null,
    Nombre varchar(150) null,
    IdEstadoMati int not null,
    Mensaje text null,
    MatiUser varchar(50) not null,
    MatiId varchar(50) not null,
    MatiFlowId varchar(50) not null,
    Activo bit default 1,
    index(IdUsuario),
    index(IdEstadoMati),
    foreign key (IdUsuario) references Tb_Usuarios(IdUsuario),
    foreign key (IdEstadoMati) references Tb_CatEstadosMati(IdestadoMati),
    primary key(IdUsuarioMati)
) engine = InnoDB;

alter table Tb_UsuariosMati add column UrlVerification varchar (250) null;
alter table Tb_UsuariosMati add column NivelActualizado bit default 0;