create table Tb_CatActividadesEconomicas(
	IdActividadEconomica int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    Nombre varchar(50) not null,
    Activo bit default 1,
    primary key(IdActividadEconomica)
) engine = InnoDB;

alter table Tb_CatActividadesEconomicas modify column Nombre varchar(250) not null;