create table Tb_Errores(
	IdError int auto_increment not null,
    FechaRegistro datetime default now(),
    IdSqlState varchar(6) null,
    MysqlError varchar(500) null,
    Mensaje varchar(500) null,
    SchemaName varchar(50) null,
    TableName varchar(100) null,
    primary key(IdError)
) ENGINE = InnoDB;