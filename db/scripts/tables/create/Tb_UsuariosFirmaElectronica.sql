create table Tb_UsuariosFirmaElectronica(
	IdUsuarioFirmaElectronica int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualización datetime default null,
    FirmaElectronica varchar(500) not null,
    IdUsuario int not null,
    NumeroTelefono varchar(10) not null,
    Activo bit default 1,
    index (IdUsuario),
    foreign key(IdUsuario) references Tb_Usuarios(IdUsuario),
    primary key(IdUsuarioFirmaElectronica)
) engine = InnoDB;