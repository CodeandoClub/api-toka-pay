create table Tb_SmsValidacionCodigos(
	IdSmsCodigo int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion date default null,
    NumeroTelefono varchar(10) not null,
    Codigo varchar(6) not null,
    Validado bit default 0,
    Activo bit default 1,
    primary key(IdSmsCodigo)
) engine = InnoDB;