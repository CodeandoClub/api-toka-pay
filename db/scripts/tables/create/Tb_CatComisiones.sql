create table Tb_CatComisiones(
	IdComision int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    Nombre varchar(50) null,
    BaseDePago varchar(50) null,
    Costo decimal(18,2) null,
    IdTipo int null,
    IVA bit null,
    Activo bit default 1,
    index(IdTipo),
    foreign key(IdTipo) references Tb_CatTipos (IdTipo),
    primary key(IdComision)
) engine = InnoDB;