create table Tb_UsuariosMatiDocumentos(
	IdUsuarioMatiDocumento int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    IdUsuario int not null,
    IdApp int not null,
    MatiId varchar(50) not null,
    IdDocumento int not null,
    Mensaje text null,
    IdEstadoDocumento int not null,
    Activo bit default 1,
    index(IdDocumento),
    index(IdEstadoDocumento),
    foreign key(IdDocumento) references Tb_CatDocumentos(IdDocumento),
    foreign key(IdEstadoDocumento) references Tb_CatEstadosDocumentos(IdEstadoDocumento),
    primary key(IdUsuarioMatiDocumento)
) engine  InnoDB;
