create table Tb_UsuariosAccesos(
	IdUsuarioAcceso int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualización datetime default null,
    Latitud decimal(18,2) null,
    Longitud decimal(18,2) null,
    IdUsuario int not null,
    Activo bit default 1,
    index (IdUsuario),
    foreign key(IdUsuario) references Tb_Usuarios(IdUsuario),
    primary key(IdUsuarioAcceso)
) engine = InnoDB;