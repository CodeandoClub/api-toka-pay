create table Tb_CatNacionalidades(
	IdNacionalidad int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    Nombre varchar(50) not null,
    ClaveNacionalidad varchar(2) null,
    Activo bit default 1,
    primary key(IdNacionalidad)
) engine = InnoDB;

alter table Tb_CatNacionalidades modify column ClaveNacionalidad varchar(3) not null;