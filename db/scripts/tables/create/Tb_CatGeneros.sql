create table Tb_CatGeneros(
	IdGenero int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    Nombre varchar(50) not null,
    ClaveGenero varchar(1) not null,
    Activo bit default 1,
    primary key(IdGenero)
) engine = InnoDB;