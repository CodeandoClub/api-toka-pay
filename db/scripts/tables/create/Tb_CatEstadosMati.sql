create table Tb_CatEstadosMati(
	IdEstadoMati int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    Nombre varchar(50) not null,
    Activo bit default 1,
    primary key(IdEstadoMati)
) engine = InnoDB;