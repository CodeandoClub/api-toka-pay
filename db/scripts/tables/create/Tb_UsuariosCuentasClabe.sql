create table Tb_UsuariosCuentasClabe(
	IdUsuarioCuentaClabe int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    IdUsuario int not null,
    CuentaClabe varchar(17) not null,
    DigitoVerificador varchar(1) not null,
    Activo bit default 1,
    index(IdUsuario),
    foreign key (IdUsuario) references Tb_Usuarios(IdUsuario),
    primary key(IdUsuarioCuentaClabe)
) engine = InnoDB;