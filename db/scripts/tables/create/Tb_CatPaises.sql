create table Tb_CatPaises(
	IdPais int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    Nombre varchar(50) not null,
    ClavePais varchar(2) null,
    Activo bit default 1,
    primary key(IdPais)
) engine = InnoDB;