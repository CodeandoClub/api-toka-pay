create table Tb_CatDirecciones(
	IdDireccion int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    Estado varchar(80) null,
    Municipio varchar(80) null,
    Ciudad varchar(80) null,
    Colonia varchar(80) null,
    CodigoPostal varchar(10) null,
    Activo bit default 1,
    primary key(IdDireccion)
) engine = InnoDB;