create table Tb_UsuariosDireccion(
	IdUsuarioDireccion int auto_increment not null,
    FechaRegistro datetime default now(),
    FechaActualizacion datetime default null,
    IdUsuario int,
    Calle varchar(100) null,
    NumeroExterior varchar(50) null,
    NumeroInterior varchar(50) null,
    IdDireccion int null,
    Activo bit default 1,
    index(IdUsuario),
    index(IdDireccion),
    foreign key (IdUsuario) references Tb_Usuarios (IdUsuario),
    foreign key (IdDireccion) references Tb_CatDirecciones (IdDireccion),
    primary key(IdUsuarioDireccion)
) engine = InnoDB;