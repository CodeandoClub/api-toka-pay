﻿using API_Toka_Pay.Models.AsociacionesTarjetas.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface IAsociacionesTarjetasRepository
    {
        Task<Dictionary<int, object>> AgregarAsync(
            AsociacionesTarjetasInputModel Model
        );
    }
}
