﻿using API_Toka_Pay.Models.Usuarios.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface IUsuariosRepository
    {
        Task<Dictionary<int, object>> ObtenerEstadoAsync(
            string NumeroTelefono = null,
            int? IdUsuario = null
        );

        Task<Dictionary<int, object>> ActualizarEstadoAsync(
            int IdEstadoUsuario,
            string NumeroTelefono = null,
            int? IdUsuario = null
        );

        Task<Dictionary<int, object>> ActualizarPasswordAsync(
            UsuariosInputModel model
        );
    }
}
