﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface IMovimientosRepository
    {
        Task<Dictionary<int, object>> ObtenerAsync(
            string ProxyNumber,
            int IdUsuario,
            string Mes,
            string Anio
        );
    }
}
