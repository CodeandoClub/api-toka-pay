﻿using API_Toka_Pay.Models.Direcciones.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface IDireccionesRepository
    {
        Task<Dictionary<int, object>> ActualizarAsync(DireccionesInputModel model);
    }
}
