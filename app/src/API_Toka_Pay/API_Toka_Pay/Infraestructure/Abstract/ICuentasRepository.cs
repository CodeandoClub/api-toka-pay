﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface ICuentasRepository
    {
        Task<Dictionary<int, object>> AgregarAsync();

        Task<Dictionary<int, object>> ValidarAsync();
    }
}
