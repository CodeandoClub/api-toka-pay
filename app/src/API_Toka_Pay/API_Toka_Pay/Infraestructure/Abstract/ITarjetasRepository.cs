﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface ITarjetasRepository
    {
        Task<Dictionary<int, object>> ObtenerAsync(int IdUsuario);

        Task<Dictionary<int, object>> CambiarEstadoAsync(string ProxyNumber, int IdUsuario, int IdEstado);

        Task<Dictionary<int, object>> CambiarNIPAsync(string ProxyNumber, int IdUsuario, string NIP);

        Task<Dictionary<int, object>> CambiarNombreAsync(string ProxyNumber, int IdUsuario, string Nombre);
    }
}
