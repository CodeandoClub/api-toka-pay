﻿using API_Toka_Pay.Models.Autorizacion.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface IAutorizacionRepository
    {
        Task<Dictionary<int, object>> AuthAsync(AutorizacionInputModel model);

        Task<Dictionary<int, object>> ObtenerNombreAsync(string Correo);

        Task<Dictionary<int, object>> AuthBiometricoAsync(string Correo);
    }
}
