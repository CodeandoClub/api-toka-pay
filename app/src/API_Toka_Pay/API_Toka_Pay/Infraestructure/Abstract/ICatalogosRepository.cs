﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface ICatalogosRepository
    {
        Task<Dictionary<int, object>> ObtenerEstadosAsync(
            int? IdEstado = null,
            string ClaveEstado = null
        );

        Task<Dictionary<int, object>> ObtenerGenerosAsync(
            int? IdGenero = null,
            string ClaveGenero = null
        );

        Task<Dictionary<int, object>> ObtenerActividadesEconomicasAsync(
            int? IdActividadEconomica = null
        );

        Task<Dictionary<int, object>> ObtenerNacionalidadesAsync(
            int? IdNacionalidad = null,
            string ClaveNacionalidad = null
        );

        Task<Dictionary<int, object>> ObtenerParentescosAsync(
            int? IdParentesco = null
        );

        Task<Dictionary<int, object>> ObtenerDireccionesAsync(
            int? IdDireccion = null,
            string CodigoPostal = null
        );

        Task<Dictionary<int, object>> ObtenerComisionesAsync(
            int? Idcomision = null
        );
    }
}
