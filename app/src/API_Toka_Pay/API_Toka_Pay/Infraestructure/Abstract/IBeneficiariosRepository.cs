﻿using API_Toka_Pay.Models.Beneficiarios.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface IBeneficiariosRepository
    {
        Task<Dictionary<int, object>> ActualizarAsync(BeneficiariosInputModel model);

        Task<Dictionary<int, object>> EliminarAsync(int IdUsuario);

        Task<Dictionary<int, object>> ObtenerAsync(int IdUsuario);
    }
}
