﻿using API_Toka_Pay.Models.FirmaElectronica.Input;
using API_Toka_Pay.Models.NivelesCuenta.Input;
using API_Toka_Pay.Models.Registro.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface INivelesCuentaRepository
    {
        Task<Dictionary<int, object>> ActualizarNivelAsync(NivelesCuentaInputModel model);
    }
}
