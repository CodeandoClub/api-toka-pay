﻿using API_Toka_Pay.Models.SolicitudesTarjetas.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface ISolicitudTarjetasRepository
    {
        Task<Dictionary<int, object>> ObtenerAsync(int IdSolicitud, int IdUsuario);

        Task<Dictionary<int, object>> AgregarAsync(SolicitudesTarjetasInputModel model);
    }
}
