﻿using API_Toka_Pay.Models.SMS.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface ISMSRepository
    {
        Task<Dictionary<int, object>> EnviarAsync(string NumeroTelefono);

        Task<Dictionary<int, object>> ComprobarAsync(SMSComprobarInputModel model);

        Task<Dictionary<int, object>> EnviarValidacionAsync(string NumeroTelefono);

        Task<Dictionary<int, object>> ComprobarValidacionAsync(SMSComprobarInputModel model);
    }
}
