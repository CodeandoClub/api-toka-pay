﻿using API_Toka_Pay.Models.FirmaElectronica.Input;
using API_Toka_Pay.Models.NivelesCuenta.Input;
using API_Toka_Pay.Models.Registro.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Abstract
{
    public interface IRegistroRepository
    {
        Task<Dictionary<int, object>> ActualizarAsync(string NumeroTelefono, RegistroInputModel model);

        Task<Dictionary<int, object>> ObtenerAsync(string NumeroTelefono);

        Task<Dictionary<int, object>> AgregarFirmaAsync(FirmaElectronicaInputModel model);

        Task<Dictionary<int, object>> ActualizarNivelAsync(NivelesCuentaInputModel model);
    }
}
