﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.CambiarEstado.Input;
using API_Toka_Pay.Models.CambiarNIP.Input;
using API_Toka_Pay.Models.CambiarNombre.Input;
using API_Toka_Pay.Models.Errores.Output;
using API_Toka_Pay.Models.Tarjetas.Output;
using API_Toka_Pay.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFTarjetasRepository : ITarjetasRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        private readonly IConfiguration _Configuration;
        #endregion

        public EFTarjetasRepository(
            DBContext dBContext,
            IConfiguration iConfiguration
        )
        {
            _Context = dBContext;
            _Configuration = iConfiguration;
        }

        public async Task<Dictionary<int, object>> CambiarEstadoAsync(
            string ProxyNumber,
            int IdUsuario,
            int IdEstado
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(async () =>
                {
                    // Generamos el request (Cuenta)
                    var _Data = new CambiarEstadoInputModel
                    {
                        IdEstado = IdEstado
                    };

                    // Enviamos el código generico
                    var client = new HttpClient();

                    var httpRequestMessage = new HttpRequestMessage
                    {
                        Method = HttpMethod.Put,
                        RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "tarjetas", "estado?ProxyNumber=" + ProxyNumber.ToString() + "&IdUsuario=" + IdUsuario.ToString())),
                        Headers = {
                            { HttpRequestHeader.ContentType.ToString(), "application/json" },
                            { HttpRequestHeader.Accept.ToString(), "application/json" },
                            { HttpRequestHeader.Authorization.ToString(), "basic " + _Configuration["URL:token"] }
                        },
                        Content = new StringContent(JsonConvert.SerializeObject(_Data), Encoding.UTF8, "application/json")
                    };

                    var response = await client.SendAsync(httpRequestMessage);

                    var contents = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        var _Success = JsonConvert.DeserializeObject<TarjetasCollectionModel>(contents);

                        _y = new Dictionary<int, object>
                        {
                            { 1, true },
                            { 2, _Success }
                        };
                    }
                    else
                    {
                        var _ErrorHttp = JsonConvert.DeserializeObject<List<ErrorHttpModel>>(contents);

                        var _Error = new ErrorModel
                        {
                            Code = _ErrorHttp.FirstOrDefault().Code,
                            IdError = _ErrorHttp.FirstOrDefault().IdError,
                            Status = _ErrorHttp.FirstOrDefault().Status,
                            Title = _ErrorHttp.FirstOrDefault().Title
                        };

                        _y = new Dictionary<int, object>
                        {
                            { 1, false },
                            { 2, _Error }
                        };
                    }

                    return _y;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> CambiarNIPAsync(
            string ProxyNumber,
            int IdUsuario,
            string NIP
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(async () =>
                {
                    // Generamos el request (Cuenta)
                    var _Data = new CambiarNIPInputModel
                    {
                        NIP = NIP
                    };

                    // Enviamos el código generico
                    var client = new HttpClient();

                    var httpRequestMessage = new HttpRequestMessage
                    {
                        Method = HttpMethod.Put,
                        RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "tarjetas", "nip?ProxyNumber=" + ProxyNumber.ToString() + "&IdUsuario=" + IdUsuario.ToString())),
                        Headers = {
                            { HttpRequestHeader.ContentType.ToString(), "application/json" },
                            { HttpRequestHeader.Accept.ToString(), "application/json" },
                            { HttpRequestHeader.Authorization.ToString(), "basic " + _Configuration["URL:token"] }
                        },
                        Content = new StringContent(JsonConvert.SerializeObject(_Data), Encoding.UTF8, "application/json")
                    };

                    var response = await client.SendAsync(httpRequestMessage);

                    var contents = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        var _Success = JsonConvert.DeserializeObject<TarjetasCollectionModel>(contents);

                        _y = new Dictionary<int, object>
                        {
                            { 1, true },
                            { 2, _Success }
                        };
                    }
                    else
                    {
                        var _ErrorHttp = JsonConvert.DeserializeObject<List<ErrorHttpModel>>(contents);

                        var _Error = new ErrorModel
                        {
                            Code = _ErrorHttp.FirstOrDefault().Code,
                            IdError = _ErrorHttp.FirstOrDefault().IdError,
                            Status = _ErrorHttp.FirstOrDefault().Status,
                            Title = _ErrorHttp.FirstOrDefault().Title
                        };

                        _y = new Dictionary<int, object>
                        {
                            { 1, false },
                            { 2, _Error }
                        };
                    }

                    return _y;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> CambiarNombreAsync(
            string ProxyNumber,
            int IdUsuario,
            string Nombre
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(async () =>
                {
                    // Generamos el request (Cuenta)
                    var _Data = new CambiarNombreInputModel
                    {
                        NombreEmbosado = Nombre
                    };

                    // Enviamos el código generico
                    var client = new HttpClient();

                    var httpRequestMessage = new HttpRequestMessage
                    {
                        Method = HttpMethod.Put,
                        RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "tarjetas", "nombreembosado?ProxyNumber=" + ProxyNumber.ToString() + "&IdUsuario=" + IdUsuario.ToString())),
                        Headers = {
                            { HttpRequestHeader.ContentType.ToString(), "application/json" },
                            { HttpRequestHeader.Accept.ToString(), "application/json" },
                            { HttpRequestHeader.Authorization.ToString(), "basic " + _Configuration["URL:token"] }
                        },
                        Content = new StringContent(JsonConvert.SerializeObject(_Data), Encoding.UTF8, "application/json")
                    };

                    var response = await client.SendAsync(httpRequestMessage);

                    var contents = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        var _Success = JsonConvert.DeserializeObject<TarjetasCollectionModel>(contents);

                        _y = new Dictionary<int, object>
                        {
                            { 1, true },
                            { 2, _Success }
                        };
                    }
                    else
                    {
                        var _ErrorHttp = JsonConvert.DeserializeObject<List<ErrorHttpModel>>(contents);

                        var _Error = new ErrorModel
                        {
                            Code = _ErrorHttp.FirstOrDefault().Code,
                            IdError = _ErrorHttp.FirstOrDefault().IdError,
                            Status = _ErrorHttp.FirstOrDefault().Status,
                            Title = _ErrorHttp.FirstOrDefault().Title
                        };

                        _y = new Dictionary<int, object>
                        {
                            { 1, false },
                            { 2, _Error }
                        };
                    }

                    return _y;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerAsync(
            int IdUsuario
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(async () =>
                {
                    var client = new HttpClient();

                    var httpRequestMessage = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "tarjetas?IdUsuario="+IdUsuario.ToString())),
                        Headers = {
                            { HttpRequestHeader.Authorization.ToString(), "basic " + _Configuration["URL:token"] }
                        },
                    };

                    var response = await client.SendAsync(httpRequestMessage);

                    var contents = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        var _Success = JsonConvert.DeserializeObject<TarjetasCollectionModel>(contents);

                        // Obtenemos la cuenta clabe del usuario
                        var _Usuarios = _Context.Sp_tp_UsuariosObtener.FromSql(
                        $"call sp_tp_UsuariosObtener(@_NumeroTelefono,@_Correo,@_IdUsuario)",
                            new MySqlParameter("_NumeroTelefono", DBNull.Value),
                            new MySqlParameter("_Correo", DBNull.Value),
                            new MySqlParameter("_IdUsuario", (object)IdUsuario ?? DBNull.Value)
                        ).ToList();

                        _y = new Dictionary<int, object>
                        {
                            { 1, true },
                            { 2, _Success },
                            { 3, _Usuarios ?? new List<Sp_tp_UsuariosObtener_Result>() }
                        };
                    }
                    else
                    {
                        var _ErrorHttp = JsonConvert.DeserializeObject<ErrorHttpModel>(contents);

                        var _Error = new ErrorModel
                        {
                            Code = _ErrorHttp.Code,
                            IdError = _ErrorHttp.IdError,
                            Status = _ErrorHttp.Status,
                            Title = _ErrorHttp.Title
                        };

                        _y = new Dictionary<int, object>
                        {
                            { 1, false },
                            { 2, _Error }
                        };
                    }

                    return _y;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }
    }
}
