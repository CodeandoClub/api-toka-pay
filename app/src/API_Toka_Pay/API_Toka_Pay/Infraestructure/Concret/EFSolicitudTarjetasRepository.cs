﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.Errores.Output;
using API_Toka_Pay.Models.SolicitudesTarjetas.Input;
using API_Toka_Pay.Models.SolicitudesTarjetas.Output;
using API_Toka_Pay.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFSolicitudTarjetasRepository : ISolicitudTarjetasRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        private readonly IConfiguration _Configuration;
        #endregion

        public EFSolicitudTarjetasRepository(
            DBContext dBContext,
            IConfiguration iConfiguration
        )
        {
            _Context = dBContext;
            _Configuration = iConfiguration;
        }

        public async Task<Dictionary<int, object>> AgregarAsync(
            SolicitudesTarjetasInputModel model
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(async () =>
                {
                    // Enviamos el código generico
                    var client = new HttpClient();

                    var httpRequestMessage = new HttpRequestMessage
                    {
                        Method = HttpMethod.Post,
                        RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "solicitudestarjetas")),
                        Headers = {
                            { HttpRequestHeader.ContentType.ToString(), "application/json" },
                            { HttpRequestHeader.Accept.ToString(), "application/json" },
                            { HttpRequestHeader.Authorization.ToString(), "basic " + _Configuration["URL:token"] }
                        },
                        Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
                    };

                    var response = await client.SendAsync(httpRequestMessage);

                    var contents = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        var _SuccessSolicitud = JsonConvert.DeserializeObject<SolicitudesTarjetasCollectionModel>(contents);

                        _y = new Dictionary<int, object>
                        {
                            { 1, true },
                            { 2, _SuccessSolicitud }
                        };
                    }
                    else
                    {
                        var _ErrorHttp = JsonConvert.DeserializeObject<ErrorHttpModel>(contents);

                        var _Error = new ErrorModel
                        {
                            Code = _ErrorHttp.Code,
                            IdError = _ErrorHttp.IdError,
                            Status = _ErrorHttp.Status,
                            Title = _ErrorHttp.Title
                        };

                        _y = new Dictionary<int, object>
                        {
                            { 1, false },
                            { 2, _Error }
                        };
                    }

                    return _y;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerAsync(
            int IdSolicitud,
            int IdUsuario
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(async () =>
                {
                    // Enviamos el código generico
                    var client = new HttpClient();

                    var httpRequestMessage = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "solicitudestarjetas?IdSolicitud="+IdSolicitud.ToString()+"&IdUsuario="+IdUsuario.ToString())),
                        Headers = {
                            // { HttpRequestHeader.ContentType.ToString(), "application/json" },
                            // { HttpRequestHeader.Accept.ToString(), "application/json" },
                            { HttpRequestHeader.Authorization.ToString(), "basic " + _Configuration["URL:token"] }
                        },
                        // Content = new StringContent(JsonConvert.SerializeObject(_DataSolicitud), Encoding.UTF8, "application/json")
                    };

                    var response = await client.SendAsync(httpRequestMessage);

                    var contents = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        var _SuccessSolicitud = JsonConvert.DeserializeObject<SolicitudesTarjetasCollectionModel>(contents);
                        
                        _y = new Dictionary<int, object>
                        {
                            { 1, true },
                            { 2, _SuccessSolicitud }
                        };
                    }
                    else
                    {
                        var _ErrorHttp = JsonConvert.DeserializeObject<ErrorHttpModel>(contents);

                        var _Error = new ErrorModel
                        {
                            Code = _ErrorHttp.Code,
                            IdError = _ErrorHttp.IdError,
                            Status = _ErrorHttp.Status,
                            Title = _ErrorHttp.Title
                        };

                        _y = new Dictionary<int, object>
                        {
                            { 1, false },
                            { 2, _Error }
                        };
                    }

                    return _y;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }
    }
}
