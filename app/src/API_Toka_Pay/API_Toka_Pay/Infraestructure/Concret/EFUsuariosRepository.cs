﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Helpers;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models.Usuarios.Input;
using API_Toka_Pay.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFUsuariosRepository : IUsuariosRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        private readonly Regex _PasswordRegex;
        #endregion

        public EFUsuariosRepository(
            DBContext dBContext
        )
        {
            _Context = dBContext;

            _PasswordRegex = new Regex(@"(^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,})");
        }

        public async Task<Dictionary<int, object>> ActualizarEstadoAsync(
            int IdEstadoUsuario,
            string NumeroTelefono = null,
            int? IdUsuario = null
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    // Generamos la contraseña
                    var x = _Context.Sp_tp_UsuariosActualizarEstado.FromSql(
                        $"call sp_tp_UsuariosActualizarEstado(@_NumeroTelefono,@_IdUsuario,@_IdEstadoUsuario)",
                        new MySqlParameter("_NumeroTelefono", (object)NumeroTelefono ?? DBNull.Value),
                        new MySqlParameter("_IdUsuario", (object)DBNull.Value),
                        new MySqlParameter("_IdEstadoUsuario", (object)IdEstadoUsuario ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_UsuariosActualizarEstado_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ActualizarPasswordAsync(
            UsuariosInputModel model
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    // Obtenemos el usuario
                    var _Usuarios = _Context.Sp_tp_UsuariosObtener.FromSql(
                    $"call sp_tp_UsuariosObtener(@_NumeroTelefono,@_Correo,@_IdUsuario)",
                        new MySqlParameter("_NumeroTelefono", DBNull.Value),
                        new MySqlParameter("_Correo", DBNull.Value),
                        new MySqlParameter("_IdUsuario", (object)model.IdUsuario ?? DBNull.Value)
                    ).ToList();

                    if (_Usuarios.Count > 0)
                    {
                        // Validamos la contraseña anterior
                        var _Ctr = true;
                        if (model.PasswordAnterior != null)
                        {
                            if(!CriptoHelper.ValidarContrasenia(_Usuarios.FirstOrDefault().Contrasenia, model.PasswordAnterior, _Usuarios.FirstOrDefault().Salt))
                            {
                                _Ctr = false;
                            }
                        }

                        if (_Ctr)
                        {
                            // Validamos que la contraseña se igual
                            if (model.PasswordNuevo1.Equals(model.PasswordNuevo2))
                            {
                                // Validamos la expresion regular
                                if (_PasswordRegex.IsMatch(model.PasswordNuevo1))
                                {
                                    // Generamos la contraseña
                                    string _Salt = string.Empty;
                                    string _ContraseniaCifrada = CriptoHelper.ObtenerContraseniaCifrada(model.PasswordNuevo1, out _Salt);

                                    var x = _Context.Sp_tp_UsuariosActualizarPassword.FromSql(
                                        $"call sp_tp_UsuariosActualizarPassword(@_NumeroTelefono,@_Salt,@_Password)",
                                        new MySqlParameter("_NumeroTelefono", (object)_Usuarios.FirstOrDefault().NumeroTelefono ?? DBNull.Value),
                                        new MySqlParameter("_Salt", (object)_Salt ?? DBNull.Value),
                                        new MySqlParameter("_Password", (object)_ContraseniaCifrada ?? DBNull.Value)
                                    ).ToList();

                                    _tmp.Add(1, true);
                                    _tmp.Add(2, x ?? new List<Sp_tp_UsuariosActualizarPassword_Result>());
                                }
                                else
                                {
                                    _tmp.Add(1, false);
                                    _tmp.Add(2, SharedResource.Password_No_Valido);
                                    _tmp.Add(3, 50004);
                                }
                            }
                            else
                            {
                                _tmp.Add(1, false);
                                _tmp.Add(2, SharedResource.Password_No_Coincide);
                                _tmp.Add(3, 50003);
                            }
                        }
                        else
                        {
                            _tmp.Add(1, false);
                            _tmp.Add(2, SharedResource.Password_Anterior_No_Valido);
                            _tmp.Add(3, 50002);
                        }
                    }
                    else
                    {
                        _tmp.Add(1, false);
                        _tmp.Add(2, SharedResource.Usuario_No_Se_Encontrado);
                        _tmp.Add(3, 50001);
                    }

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada },
                    { 3, 50000 }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerEstadoAsync(
            string NumeroTelefono = null,
            int? IdUsuario = null
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_UsuariosObtenerEstado.FromSql(
                        $"call sp_tp_UsuariosObtenerEstado(@_NumeroTelefono,@_IdUsuario)",
                        new MySqlParameter("_NumeroTelefono", (object)NumeroTelefono ?? DBNull.Value),
                        new MySqlParameter("_IdUsuario", (object)IdUsuario ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_UsuariosObtenerEstado_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }
    }
}
