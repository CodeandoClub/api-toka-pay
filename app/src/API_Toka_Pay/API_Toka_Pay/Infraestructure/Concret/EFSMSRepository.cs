﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Helpers;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.SMS.Input;
using API_Toka_Pay.Models.SMS.Output;
using API_Toka_Pay.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFSMSRepository : ISMSRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        private readonly IConfiguration _Configuration;
        #endregion

        public EFSMSRepository(
            DBContext dBContext,
            IConfiguration configuration
        )
        {
            _Context = dBContext;
            _Configuration = configuration;
        }

        public async Task<Dictionary<int, object>> ComprobarAsync(
            SMSComprobarInputModel model
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_SMSActualizar.FromSql(
                        $"call sp_tp_SMSActualizar(@_IdSmsCodigo,@_NumeroTelefono,@_Codigo)",
                        new MySqlParameter("_IdSmsCodigo", DBNull.Value),
                        new MySqlParameter("_NumeroTelefono", (object)model.NumeroTelefono ?? DBNull.Value),
                        new MySqlParameter("_Codigo", (object)model.Codigo ?? DBNull.Value)
                    ).ToList();

                    // Validamos
                    if(x.FirstOrDefault().Id > 0)
                    {
                        _tmp.Add(1, true);
                        _tmp.Add(2, x ?? new List<Sp_tp_SMSActualizar_Result>());
                    }
                    else
                    {
                        _tmp.Add(1, false);
                        _tmp.Add(2, x.FirstOrDefault().Mensaje);
                        _tmp.Add(3, x.FirstOrDefault().Id * -1);
                    }

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada },
                    { 3, 50000 }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ComprobarValidacionAsync(
            SMSComprobarInputModel model
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_SMSValidacionActualizar.FromSql(
                        $"call sp_tp_SMSValidacionActualizar(@_IdSmsCodigo,@_NumeroTelefono,@_Codigo)",
                        new MySqlParameter("_IdSmsCodigo", DBNull.Value),
                        new MySqlParameter("_NumeroTelefono", (object)model.NumeroTelefono ?? DBNull.Value),
                        new MySqlParameter("_Codigo", (object)model.Codigo ?? DBNull.Value)
                    ).ToList();

                    // Validamos
                    if (x.FirstOrDefault().Id > 0)
                    {
                        _tmp.Add(1, true);
                        _tmp.Add(2, x ?? new List<Sp_tp_SMSValidacionActualizar_Result>());
                    }
                    else
                    {
                        _tmp.Add(1, false);
                        _tmp.Add(2, x.FirstOrDefault().Mensaje);
                        _tmp.Add(3, x.FirstOrDefault().Id * -1);
                    }

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada },
                    { 3, 50000 }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> EnviarAsync(
            string NumeroTelefono
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el número de teléfono
                var _Validate = _Context.Sp_tp_SMSValidar.FromSql(
                    $"call sp_tp_SMSValidar(@_NumeroTelefono)",
                    new MySqlParameter("_NumeroTelefono", NumeroTelefono)
                ).ToList();

                if (_Validate.FirstOrDefault().Id > 0)
                {
                    // Obtenemos el código generico
                    var _Code = Codigo.ObtenerCodigo();

                    // Generamos el request
                    var _Data = new SMSInputModel
                    {
                        Celular = NumeroTelefono,
                        Texto = "Tu código de confirmación Tokapay es: " + _Code.ToString() + ", ingrésalo para continuar con tu registro."
                    };

                    // Enviamos el código generico
                    var client = new HttpClient();

                    var httpRequestMessage = new HttpRequestMessage
                    {
                        Method = HttpMethod.Post,
                        RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "enviosms")),
                        Headers = {
                            { HttpRequestHeader.ContentType.ToString(), "application/json" },
                            { HttpRequestHeader.Accept.ToString(), "application/json" },
                            { HttpRequestHeader.Authorization.ToString(), "basic WFBTUlNGRkszVlY0UFRGQi9MRUJFVDBTQ1RHV0lHMFNXUVM4WkNTQ1ZCRT06UUlUVFFZTFBRVUhUR1lKTkpHQ0gzV01RUExZTkNMRVRBTVlBT0hVUEZBRz0=" }
                        },
                        Content = new StringContent(JsonConvert.SerializeObject(_Data), Encoding.UTF8, "application/json")
                    };

                    var response = await client.SendAsync(httpRequestMessage);

                    var contents = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        var _Success = JsonConvert.DeserializeObject<SMSOuputModel>(contents);

                        _y = new Dictionary<int, object>
                        {
                            { 1, true },
                            { 2, _Success }
                        };
                    }
                    else
                    {
                        var _Error = JsonConvert.DeserializeObject<ErrorModel>(contents);

                        _y = new Dictionary<int, object>
                        {
                            { 1, false },
                            { 2, _Error }
                        };
                    }

                    // Almacenamos el código generico
                    var x = _Context.Sp_tp_SMSAgregar.FromSql(
                        $"call sp_tp_SMSAgregar(@_NumeroTelefono,@_Codigo)",
                        new MySqlParameter("_NumeroTelefono", NumeroTelefono),
                        new MySqlParameter("_Codigo", _Code)
                    ).ToList();

                    _y = new Dictionary<int, object>{
                        { 1, true },
                        { 2, x ?? new List<Sp_tp_SMSAgregar_Result>() }
                    };
                }
                else
                {
                    _y = new Dictionary<int, object>{
                        { 1, false },
                        { 2, _Validate.FirstOrDefault().Mensaje }
                    };
                }
            }
            catch(Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> EnviarValidacionAsync(
            string NumeroTelefono
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Obtenemos el código generico
                var _Code = Codigo.ObtenerCodigo();

                // Generamos el request
                var _Data = new SMSInputModel
                {
                    Celular = NumeroTelefono,
                    Texto = "Tu código de verificación Tokapay es: " + _Code.ToString() + ", ingrésalo para continuar."
                };

                // Enviamos el código generico
                var client = new HttpClient();

                var httpRequestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "enviosms")),
                    Headers = {
                        { HttpRequestHeader.ContentType.ToString(), "application/json" },
                        { HttpRequestHeader.Accept.ToString(), "application/json" },
                        { HttpRequestHeader.Authorization.ToString(), "basic WFBTUlNGRkszVlY0UFRGQi9MRUJFVDBTQ1RHV0lHMFNXUVM4WkNTQ1ZCRT06UUlUVFFZTFBRVUhUR1lKTkpHQ0gzV01RUExZTkNMRVRBTVlBT0hVUEZBRz0=" }
                    },
                    Content = new StringContent(JsonConvert.SerializeObject(_Data), Encoding.UTF8, "application/json")
                };

                var response = await client.SendAsync(httpRequestMessage);

                var contents = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    var _Success = JsonConvert.DeserializeObject<SMSOuputModel>(contents);

                    _y = new Dictionary<int, object>
                    {
                        { 1, true },
                        { 2, _Success }
                    };
                }
                else
                {
                    var _Error = JsonConvert.DeserializeObject<ErrorModel>(contents);

                    _y = new Dictionary<int, object>
                    {
                        { 1, false },
                        { 2, _Error }
                    };
                }

                // Almacenamos el código generico
                var x = _Context.Sp_tp_SMSValidacionAgregar.FromSql(
                    $"call sp_tp_SMSValidacionAgregar(@_NumeroTelefono,@_Codigo)",
                    new MySqlParameter("_NumeroTelefono", NumeroTelefono),
                    new MySqlParameter("_Codigo", _Code)
                ).ToList();

                _y = new Dictionary<int, object>{
                    { 1, true },
                    { 2, x ?? new List<Sp_tp_SMSValidacionAgregar_Result>() }
                };
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }
    }
}
