﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Helpers;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.Errores.Output;
using API_Toka_Pay.Models.Movimientos.Output;
using API_Toka_Pay.Resources;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFMovimientosRepository : IMovimientosRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        private readonly IConfiguration _Configuration;
        #endregion

        public EFMovimientosRepository(
            DBContext dBContext,
            IConfiguration iConfiguration
        )
        {
            _Context = dBContext;
            _Configuration = iConfiguration;
        }

        public async Task<Dictionary<int, object>> ObtenerAsync(
            string ProxyNumber,
            int IdUsuario,
            string Mes,
            string Anio
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(async () =>
                {
                    // Obtenemos las fechas
                    DateTime _FechaInicio = DateTime.Parse(FechaHelper.Parse("01-"+(Mes.Length == 2 ? Mes : "0"+Mes) + "-" + Anio));
                    DateTime _FechaFin = DateTime.Parse(FechaHelper.Parse("01-" + (Mes.Length == 2 ? Mes : "0" + Mes) + "-" + Anio)).AddMonths(1).AddDays(-1);

                    // Enviamos el código generico
                    var client = new HttpClient();

                    var httpRequestMessage = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "movimientos?ProxyNumber=" + ProxyNumber.ToString() + "&IdUsuario=" + IdUsuario.ToString()+"&FechaInicial="+_FechaInicio.ToString("dd-MM-yyyy")+"&FechaFinal="+_FechaFin.ToString("dd-MM-yyyy"))),
                        Headers = {
                            { HttpRequestHeader.Authorization.ToString(), "basic " + _Configuration["URL:token"] }
                        }
                    };

                    var response = await client.SendAsync(httpRequestMessage);

                    var contents = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        var _Success = JsonConvert.DeserializeObject<MovimientosCollectionModel>(contents);

                        _y = new Dictionary<int, object>
                        {
                            { 1, true },
                            { 2, _Success }
                        };
                    }
                    else
                    {
                        var _ErrorHttp = JsonConvert.DeserializeObject<List<ErrorHttpModel>>(contents);

                        var _Error = new ErrorModel
                        {
                            Code = _ErrorHttp.FirstOrDefault().Code,
                            IdError = _ErrorHttp.FirstOrDefault().IdError,
                            Status = _ErrorHttp.FirstOrDefault().Status,
                            Title = _ErrorHttp.FirstOrDefault().Title
                        };

                        _y = new Dictionary<int, object>
                        {
                            { 1, false },
                            { 2, _Error }
                        };
                    }

                    return _y;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }
    }
}
