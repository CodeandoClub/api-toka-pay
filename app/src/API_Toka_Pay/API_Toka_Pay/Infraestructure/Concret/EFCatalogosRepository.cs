﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Resources;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFCatalogosRepository : ICatalogosRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        #endregion

        public EFCatalogosRepository(
            DBContext dBContext
        )
        {
            _Context = dBContext;
        }

        public async Task<Dictionary<int, object>> ObtenerActividadesEconomicasAsync(
            int? IdActividadEconomica = null
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_ActividadesEconomicasObtener.FromSql(
                        $"call sp_tp_ActividadesEconomicasObtener(@_IdActividadEconomica)",
                        new MySqlParameter("_IdActividadEconomica", (object)IdActividadEconomica ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_ActividadesEconomicasObtener_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerComisionesAsync(
            int? Idcomision = null
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_ComisionesObtener.FromSql(
                        $"call sp_tp_ComisionesObtener(@_IdComision)",
                        new MySqlParameter("_IdComision", (object)Idcomision ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_ComisionesObtener_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerDireccionesAsync(
            int? IdDireccion = null,
            string CodigoPostal = null
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_DireccionesObtener.FromSql(
                        $"call sp_tp_DireccionesObtener(@_IdDireccion,@_CodigoPostal)",
                        new MySqlParameter("_IdActividadEconomica", (object)IdDireccion ?? DBNull.Value),
                        new MySqlParameter("_CodigoPostal", (object)CodigoPostal ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_DireccionesObtener_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerEstadosAsync(
            int? IdEstado = null,
            string ClaveEstado = null
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_EstadosObtener.FromSql(
                        $"call sp_tp_EstadosObtener(@_IdEstado,@_ClaveEstado)",
                        new MySqlParameter("_IdEstado", (object)IdEstado ?? DBNull.Value),
                        new MySqlParameter("_ClaveEstado", (object)IdEstado ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_EstadosObtener_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerGenerosAsync(
            int? IdGenero = null, 
            string ClaveGenero = null
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_GenerosObtener.FromSql(
                        $"call sp_tp_GenerosObtener(@_IdGenero,@_ClaveGenero)",
                        new MySqlParameter("_IdGenero", (object)IdGenero ?? DBNull.Value),
                        new MySqlParameter("_ClaveGenero", (object)ClaveGenero ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_GenerosObtener_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerNacionalidadesAsync(
            int? IdNacionalidad = null,
            string ClaveNacionalidad = null
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_NacionalidadesObtener.FromSql(
                        $"call sp_tp_NacionalidadesObtener(@_IdNacionalidad,@_ClaveNacionalidad)",
                        new MySqlParameter("_IdNacionalidad", (object)IdNacionalidad ?? DBNull.Value),
                        new MySqlParameter("_ClaveNacionalidad", (object)ClaveNacionalidad ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_NacionalidadesObtener_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerParentescosAsync(
            int? IdParentesco = null
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_ParentescosObtener.FromSql(
                        $"call sp_tp_ParentescosObtener(@_IdParentesco)",
                        new MySqlParameter("_IdParentesco", (object)IdParentesco ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_ParentescosObtener_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }
    }
}
