﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Helpers;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models.FirmaElectronica.Input;
using API_Toka_Pay.Models.NivelesCuenta.Input;
using API_Toka_Pay.Models.Registro.Input;
using API_Toka_Pay.Resources;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFRegistroRepository : IRegistroRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        private readonly Regex _PasswordRegex;
        #endregion

        public EFRegistroRepository(DBContext dBContext)
        {
            _Context = dBContext;

            _PasswordRegex = new Regex(@"(^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,})");
        }

        public async Task<Dictionary<int, object>> ActualizarAsync(
            string NumeroTelefono,
            RegistroInputModel model
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    // Mapeamos la fecha de nacimiento
                    DateTime? _FechaNacimiento = null;
                    if(model.FechaNacimiento != null)
                    {
                        _FechaNacimiento = DateTime.Parse(FechaHelper.Parse(model.FechaNacimiento));
                    }

                    // Validamos el IdRegistro 1 - Parte 1, 2 - Parte 2, 3 - Contraseña

                    if(model.IdRegistro == 1 || model.IdRegistro == 2)
                    {
                        var x = _Context.Sp_tp_UsuariosActualizar.FromSql(
                            $"call sp_tp_UsuariosActualizar(@_Nombre,@_Paterno,@_Materno,@_FechaNacimiento,@_Correo,@_NumeroTelefono,@_IdGenero,@_IdNacionalidad,@_IdEstado,@_IdActividadEconomica)",
                            new MySqlParameter("_Nombre", (object)model.Nombre ?? DBNull.Value),
                            new MySqlParameter("_Paterno", (object)model.Paterno ?? DBNull.Value),
                            new MySqlParameter("_Materno", (object)model.Materno ?? DBNull.Value),
                            new MySqlParameter("_FechaNacimiento", (object)_FechaNacimiento ?? DBNull.Value),
                            new MySqlParameter("_Correo", (object)model.Correo ?? DBNull.Value),
                            new MySqlParameter("_NumeroTelefono", (object)NumeroTelefono ?? DBNull.Value),
                            new MySqlParameter("_IdGenero", (object)model.IdGenero ?? DBNull.Value),
                            new MySqlParameter("_IdNacionalidad", (object)model.IdNacionalidad ?? DBNull.Value),
                            new MySqlParameter("_IdEstado", (object)model.IdEstado ?? DBNull.Value),
                            new MySqlParameter("_IdActividadEconomica", (object)model.IdActividadEconomica ?? DBNull.Value)
                        ).ToList();

                        _tmp.Add(1, true);
                        _tmp.Add(2, x ?? new List<Sp_tp_UsuariosActualizar_Result>());
                    }
                    else if(model.IdRegistro == 3)
                    {
                        // Validamos que la contraseña se igual
                        if (model.Password.Equals(model.Password2))
                        {
                            // Validamos la expresion regular
                            if (_PasswordRegex.IsMatch(model.Password))
                            {
                                // Generamos la contraseña
                                string _Salt = string.Empty;
                                string _ContraseniaCifrada = CriptoHelper.ObtenerContraseniaCifrada(model.Password, out _Salt);

                                var x = _Context.Sp_tp_UsuariosActualizarPassword.FromSql(
                                    $"call sp_tp_UsuariosActualizarPassword(@_NumeroTelefono,@_Salt,@_Password)",
                                    new MySqlParameter("_NumeroTelefono", (object)NumeroTelefono ?? DBNull.Value),
                                    new MySqlParameter("_Salt", (object)_Salt ?? DBNull.Value),
                                    new MySqlParameter("_Password", (object)_ContraseniaCifrada ?? DBNull.Value)
                                ).ToList();

                                _tmp.Add(1, true);
                                _tmp.Add(2, x ?? new List<Sp_tp_UsuariosActualizarPassword_Result>());
                            }
                            else
                            {
                                _tmp.Add(1, false);
                                _tmp.Add(2, SharedResource.Password_No_Valido);
                            }
                        }
                        else
                        {
                            _tmp.Add(1, false);
                            _tmp.Add(2, SharedResource.Password_No_Coincide);
                        }
                    }

                    return _tmp;
                });
            }
            catch(Exception e)
            {
                _Logger.Error(e);
            }

            return _y;
        }

        public Task<Dictionary<int, object>> ActualizarNivelAsync(
            NivelesCuentaInputModel model
        )
        {
            // Consumimos la API de Mati

            throw new NotImplementedException();
        }

        public async Task<Dictionary<int, object>> AgregarFirmaAsync(
            FirmaElectronicaInputModel model
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    // Obtenemos la imagen
                    byte[] _Imagen = Convert.FromBase64String(model.Imagen);

                    // Obtenemos la información del usuario
                    var _Usuario = _Context.Sp_tp_UsuariosObtener.FromSql(
                        $"call sp_tp_UsuariosObtener(@_NumeroTelefono,@_Correo,@_IdUsuario)",
                        new MySqlParameter("_NumeroTelefono", (object)model.NumeroTelefono ?? DBNull.Value),
                        new MySqlParameter("_Correo", DBNull.Value),
                        new MySqlParameter("_IdUsuario", DBNull.Value)
                    ).ToList();

                    if(_Usuario.Count > 0)
                    {
                        // Creamos la firma
                        string _Firma = DateTime.Now.ToString("dd/MM/yyyyhh:mm:ss") + model.NumeroTelefono + _Usuario.FirstOrDefault().IdUsuario.ToString();
                        string _FirmaElectronica = Codigo.EncriptarToken(_Firma);

                        // Guardamos el acceso
                        var _Acceso = _Context.Sp_tp_UsuariosAccesosAgregar.FromSql(
                            $"call sp_tp_UsuariosAccesosAgregar(@_IdUsuario,@_Latitud,@_Longitud)",
                            new MySqlParameter("_IdUsuario", (object)_Usuario.FirstOrDefault().IdUsuario ?? DBNull.Value),
                            new MySqlParameter("_Latitud", (object)model.Latitud ?? DBNull.Value),
                            new MySqlParameter("_Longitud", (object)model.Longitud ?? DBNull.Value)
                        ).ToList();

                        // Almacenamos la firma
                        var x = _Context.Sp_tp_UsuariosAgregarFirmaElectronica.FromSql(
                            $"call sp_tp_UsuariosAgregarFirmaElectronica(@_NumeroTelefono,@_IdUsuario,@_FirmaElectronica)",
                            new MySqlParameter("_NumeroTelefono", (object)model.NumeroTelefono ?? DBNull.Value),
                            new MySqlParameter("_IdUsuario", (object)_Usuario.FirstOrDefault().IdUsuario ?? DBNull.Value),
                            new MySqlParameter("_FirmaElectronica", (object)_FirmaElectronica ?? DBNull.Value)
                        ).ToList();

                        // Almacenamos la imagen
                        if(FileHelper.Save(_Imagen, _Usuario.FirstOrDefault().IdUsuario.ToString() + "_" + _Usuario.FirstOrDefault().NumeroTelefono + ".jpg", "FirmaElectronica"))
                        {
                            _tmp.Add(1, true);
                            _tmp.Add(2, x ?? new List<Sp_tp_UsuariosAgregarFirmaElectronica_Result>());
                        }
                        else
                        {
                            _tmp.Add(1, true);
                            _tmp.Add(2, SharedResource.Imagen_No_Almacenada);
                        }
                    }
                    else
                    {
                        _tmp.Add(1, false);
                        _tmp.Add(2, SharedResource.NumeroTelefono_No_Encontrado);
                    }

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerAsync(
            string NumeroTelefono
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_UsuariosObtener.FromSql(
                        $"call sp_tp_UsuariosObtener(@_NumeroTelefono,@_Correo,@_IdUsuario)",
                        new MySqlParameter("_NumeroTelefono", (object)NumeroTelefono ?? DBNull.Value),
                        new MySqlParameter("_Correo", DBNull.Value),
                        new MySqlParameter("_IdUsuario", DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_UsuariosObtener_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return _y;
        }
    }
}
