﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Helpers;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models.Beneficiarios.Input;
using API_Toka_Pay.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFBeneficiariosRepository : IBeneficiariosRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        private readonly IConfiguration _Configuration;
        #endregion

        public EFBeneficiariosRepository(
            DBContext dBContext,
            IConfiguration iConfiguration
        )
        {
            _Context = dBContext;
            _Configuration = iConfiguration;
        }

        public async Task<Dictionary<int, object>> ActualizarAsync(
            BeneficiariosInputModel model
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    // Mapeamos la fecha de nacimiento
                    DateTime? _FechaNacimiento = null;
                    if (model.FechaNacimiento != null)
                    {
                        _FechaNacimiento = DateTime.Parse(FechaHelper.Parse(model.FechaNacimiento));
                    }

                    // Validamos el IdRegistro 1 - Parte 1, 2 - Parte 2, 3 - Contraseña

                    var x = _Context.Sp_tp_UsuariosBeneficiariosActualizar.FromSql(
                        $"call sp_tp_UsuariosBeneficiariosActualizar(@_IdUsuario,@_Nombre,@_Paterno,@_Materno,@_FechaNacimiento,@_Correo,@_NumeroTelefono,@_IdParentesco,@_Direccion,@_Activo)",
                        new MySqlParameter("_IdUsuario", (object)model.IdUsuario ?? DBNull.Value), 
                        new MySqlParameter("_Nombre", (object)model.Nombre ?? DBNull.Value),
                        new MySqlParameter("_Paterno", (object)model.Paterno ?? DBNull.Value),
                        new MySqlParameter("_Materno", (object)model.Materno ?? DBNull.Value),
                        new MySqlParameter("_FechaNacimiento", (object)_FechaNacimiento ?? DBNull.Value),
                        new MySqlParameter("_Correo", (object)model.Correo ?? DBNull.Value),
                        new MySqlParameter("_NumeroTelefono", (object)model.NumeroTelefono ?? DBNull.Value),
                        new MySqlParameter("_IdParentesco", (object)model.IdParentesco ?? DBNull.Value),
                        new MySqlParameter("_Direccion", (object)model.Direccion ?? DBNull.Value),
                        new MySqlParameter("_Activo", (object)true ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_UsuariosBeneficiariosActualizar_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> EliminarAsync(
            int IdUsuario
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_UsuariosBeneficiariosActualizar.FromSql(
                        $"call sp_tp_UsuariosBeneficiariosActualizar(@_IdUsuario,@_Nombre,@_Paterno,@_Materno,@_FechaNacimiento,@_Correo,@_NumeroTelefono,@_IdParentesco,@_Direccion,@_Activo)",
                        new MySqlParameter("_IdUsuario", (object)IdUsuario ?? DBNull.Value),
                        new MySqlParameter("_Nombre", DBNull.Value),
                        new MySqlParameter("_Paterno", DBNull.Value),
                        new MySqlParameter("_Materno", DBNull.Value),
                        new MySqlParameter("_FechaNacimiento", DBNull.Value),
                        new MySqlParameter("_Correo", DBNull.Value),
                        new MySqlParameter("_NumeroTelefono", DBNull.Value),
                        new MySqlParameter("_IdParentesco", DBNull.Value),
                        new MySqlParameter("_Direccion", DBNull.Value),
                        new MySqlParameter("_Activo", (object)false ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_UsuariosBeneficiariosActualizar_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerAsync(
            int IdUsuario
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_UsuariosBeneficiariosObtener.FromSql(
                        $"call sp_tp_UsuariosBeneficiariosObtener(@_IdUsuario)",
                        new MySqlParameter("_IdUsuario", (object)IdUsuario ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_UsuariosBeneficiariosObtener_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }
    }
}
