﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.Cuentas.Input;
using API_Toka_Pay.Models.Cuentas.Output;
using API_Toka_Pay.Models.Errores.Output;
using API_Toka_Pay.Models.Mati.Input;
using API_Toka_Pay.Models.SolicitudesTarjetas.Input;
using API_Toka_Pay.Models.SolicitudesTarjetas.Output;
using API_Toka_Pay.Models.Tarjetas.Output;
using API_Toka_Pay.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFCuentasRepository : ICuentasRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        private readonly IConfiguration _Configuration;
        private readonly ITarjetasRepository _ITarjetasRepository;
        #endregion

        public EFCuentasRepository(
            DBContext dBContext,
            IConfiguration iConfiguration,
            ITarjetasRepository iTarjetasRepository
        )
        {
            _Context = dBContext;
            _Configuration = iConfiguration;
            _ITarjetasRepository = iTarjetasRepository;
    }

        public async Task<Dictionary<int, object>> AgregarAsync()
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(async () =>
                {
                    // Obtenemos los usuarios para generar cuentas
                    var _Usuarios = _Context.Sp_tp_CuentasGenerarObtener.FromSql(
                        $"call sp_tp_CuentasGenerarObtener()"
                    ).ToList();

                    if (_Usuarios.Count == 0)
                    {
                        _y = new Dictionary<int, object>
                        {
                            { 1, true },
                            { 2, SharedResource.Usuario_No_Se_Encontrado }
                        };
                    }

                    // Recorremos los servicios
                    foreach(var val in _Usuarios)
                    {
                        // Obtenemos los datos del usuario
                        var _Usuario = _Context.Sp_tp_UsuariosObtener.FromSql(
                            $"call sp_tp_UsuariosObtener(@_NumeroTelefono,@_Correo,@_IdUsuario)",
                            new MySqlParameter("_NumeroTelefono", (object)val.NumeroTelefono ?? DBNull.Value),
                            new MySqlParameter("_Correo", DBNull.Value),
                            new MySqlParameter("_IdUsuario", DBNull.Value)
                        ).ToList();

                        // Generamos el request (Cuenta)
                        var _Data = new CuentasInputModel
                        {
                            EstadoNacimiento = _Usuario.FirstOrDefault().ClaveEstado,
                            FechaNacimiento = ((DateTime)_Usuario.FirstOrDefault().FechaNacimiento).ToString("dd-MM-yyyy"),
                            IdUsuario = _Usuario.FirstOrDefault().IdUsuario,
                            Materno = _Usuario.FirstOrDefault().Materno,
                            Paterno = _Usuario.FirstOrDefault().Paterno,
                            Nombres = _Usuario.FirstOrDefault().Nombre,
                            Sexo = _Usuario.FirstOrDefault().ClaveGenero
                        };

                        // Enviamos el código generico
                        var client = new HttpClient();

                        var httpRequestMessage = new HttpRequestMessage
                        {
                            Method = HttpMethod.Post,
                            RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "cuentas")),
                            Headers = {
                                { HttpRequestHeader.ContentType.ToString(), "application/json" },
                                { HttpRequestHeader.Accept.ToString(), "application/json" },
                                { HttpRequestHeader.Authorization.ToString(), "basic " + _Configuration["URL:token"] }
                            },
                            Content = new StringContent(JsonConvert.SerializeObject(_Data), Encoding.UTF8, "application/json")
                        };

                        var response = await client.SendAsync(httpRequestMessage);

                        var contents = await response.Content.ReadAsStringAsync();

                        if (response.IsSuccessStatusCode)
                        {
                            var _Success = JsonConvert.DeserializeObject<CuentasCollectionModel>(contents);

                            // Solicitamos la tarjeta
                            var _DataSolicitud = new SolicitudesTarjetasInputModel
                            {
                                IdUsuario = _Usuario.FirstOrDefault().IdUsuario,
                                Virtual = true
                            };

                            // Enviamos el código generico
                            var clientSolicitud = new HttpClient();

                            var httpRequestMessageSolicitud = new HttpRequestMessage
                            {
                                Method = HttpMethod.Post,
                                RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "solicitudestarjetas")),
                                Headers = {
                                { HttpRequestHeader.ContentType.ToString(), "application/json" },
                                { HttpRequestHeader.Accept.ToString(), "application/json" },
                                { HttpRequestHeader.Authorization.ToString(), "basic " + _Configuration["URL:token"] }
                            },
                                Content = new StringContent(JsonConvert.SerializeObject(_DataSolicitud), Encoding.UTF8, "application/json")
                            };

                            var responseSolicitud = await clientSolicitud.SendAsync(httpRequestMessageSolicitud);

                            var contentsSolicitud = await responseSolicitud.Content.ReadAsStringAsync();

                            if (responseSolicitud.IsSuccessStatusCode)
                            {
                                var _SuccessSolicitud = JsonConvert.DeserializeObject<SolicitudesTarjetasCollectionModel>(contentsSolicitud);

                                // Actualizamos la cuenta
                                var _Cuentas = _Context.Sp_tp_CuentasGenerarAgregar.FromSql(
                                    $"call sp_tp_CuentasGenerarAgregar(@_NumeroTelefono)",
                                    new MySqlParameter("_NumeroTelefono", (object)val.NumeroTelefono ?? DBNull.Value)
                                ).ToList();

                                // actualizamos la cuenta clabe
                                var _DiccCuentas = _Context.Sp_tp_UsuariosCuentaClabeAgregar.FromSql(
                                    $"call sp_tp_UsuariosCuentaClabeAgregar(@_IdUsuario,@_CuentaClabe,@_DigitoVerificador)",
                                    new MySqlParameter("_IdUsuario", (object)_Usuario.FirstOrDefault().IdUsuario ?? DBNull.Value),
                                    new MySqlParameter("_CuentaClabe", (object)_Success.Data.FirstOrDefault().Data.Attributes.CuentaClabe.Substring(0,17) ?? DBNull.Value),
                                    new MySqlParameter("_DigitoVerificador", (object)_Success.Data.FirstOrDefault().Data.Attributes.CuentaClabe.Substring(17,1) ?? DBNull.Value)
                                ).ToList();

                                _y = new Dictionary<int, object>
                                {
                                    { 1, true },
                                    { 2, _SuccessSolicitud }
                                };
                            }
                            else
                            {
                                var _ErrorHttp = JsonConvert.DeserializeObject<ErrorHttpModel>(contents);

                                var _Error = new ErrorModel
                                {
                                    Code = _ErrorHttp.Code,
                                    IdError = _ErrorHttp.IdError,
                                    Status = _ErrorHttp.Status,
                                    Title = _ErrorHttp.Title
                                };

                                _y = new Dictionary<int, object>
                                {
                                    { 1, false },
                                    { 2, _Error }
                                };
                            }
                        }
                        else
                        {
                            var _ErrorHttp = JsonConvert.DeserializeObject<ErrorHttpModel>(contents);

                            var _Error = new ErrorModel
                            {
                                Code = _ErrorHttp.Code,
                                IdError = _ErrorHttp.IdError,
                                Status = _ErrorHttp.Status,
                                Title = _ErrorHttp.Title
                            };

                            _y = new Dictionary<int, object>
                            {
                                { 1, false },
                                { 2, _Error }
                            };
                        }
                    };

                    return _y;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ValidarAsync()
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try {
                _y = await Task.Run(async () =>
                {
                    // Obtenemos los usuarios para generar cuentas
                    var _Usuarios = _Context.Sp_tp_CuentasValidarObtener.FromSql(
                        $"call sp_tp_CuentasValidarObtener()"
                    ).ToList();

                    if (_Usuarios.Count == 0)
                    {
                        _y = new Dictionary<int, object>
                        {
                            { 1, true },
                            { 2, SharedResource.Usuario_No_Se_Encontrado }
                        };
                    }

                    // Recorremos los servicios
                    foreach (var val in _Usuarios)
                    {
                        // Obtenemos los datos del usuario
                        var _Usuario = _Context.Sp_tp_UsuariosObtener.FromSql(
                            $"call sp_tp_UsuariosObtener(@_NumeroTelefono,@_Correo,@_IdUsuario)",
                            new MySqlParameter("_NumeroTelefono", (object)val.NumeroTelefono ?? DBNull.Value),
                            new MySqlParameter("_Correo", DBNull.Value),
                            new MySqlParameter("_IdUsuario", DBNull.Value)
                        ).ToList();

                        // Obtenemos el proxynumber
                        var _DiccTarjetas = await _ITarjetasRepository.ObtenerAsync(_Usuario.FirstOrDefault().IdUsuario);

                        if ((bool)_DiccTarjetas.Where(x => x.Key == 1).FirstOrDefault().Value)
                        {
                            var _Tarjetas = (TarjetasCollectionModel)_DiccTarjetas.Where(x => x.Key == 2).FirstOrDefault().Value;

                            // Generamos el request (Cuenta)
                            var _Data = new NivelesCuentaInputModelRequest
                            {
                                IdNivel = (int)_Usuario.FirstOrDefault().IdNivelPendiente
                            };

                            // Enviamos el código generico
                            var client = new HttpClient();

                            var _ProxyNumber = _Tarjetas.Data.FirstOrDefault().Data.ProxyNumber;

                            var httpRequestMessage = new HttpRequestMessage
                            {
                                Method = HttpMethod.Put,
                                RequestUri = new Uri(Path.Combine(_Configuration["URL:tokapay"], "nivelescuenta?ProxyNumber=" + _ProxyNumber .ToString() + "&IdUsuario=" + _Usuario.FirstOrDefault().IdUsuario.ToString())),
                                Headers = {
                                        { HttpRequestHeader.ContentType.ToString(), "application/json" },
                                        { HttpRequestHeader.Accept.ToString(), "application/json" },
                                        { HttpRequestHeader.Authorization.ToString(), "basic " + _Configuration["URL:token"] }
                                    },
                                Content = new StringContent(JsonConvert.SerializeObject(_Data), Encoding.UTF8, "application/json")
                            };

                            var response = await client.SendAsync(httpRequestMessage);

                            var contents = await response.Content.ReadAsStringAsync();

                            if (response.IsSuccessStatusCode)
                            {
                                // Actualizamos la respuesta
                                var _Actualizar = _Context.Sp_tp_CuentasValidarActualizar.FromSql(
                                    $"call sp_tp_CuentasValidarActualizar(@_IdUsuario,@_IdNivel)",
                                    new MySqlParameter("_IdUsuario", (object)_Usuario.FirstOrDefault().IdUsuario ?? DBNull.Value),
                                    new MySqlParameter("_IdNivel", (object)_Usuario.FirstOrDefault().IdNivelPendiente ?? DBNull.Value)
                                ).ToList();

                                _y = new Dictionary<int, object>
                                {
                                    { 1, true },
                                    { 2, SharedResource.Nivel_Actualizado }
                                };
                            }
                            else
                            {
                                var _ErrorHttp = JsonConvert.DeserializeObject<ErrorHttpModel>(contents);

                                var _Error = new ErrorModel
                                {
                                    Code = _ErrorHttp.Code,
                                    IdError = _ErrorHttp.IdError,
                                    Status = _ErrorHttp.Status,
                                    Title = _ErrorHttp.Title
                                };

                                _y = new Dictionary<int, object>
                                {
                                    { 1, false },
                                    { 2, _Error }
                                };
                            }
                        }
                        else
                        {
                            _y = new Dictionary<int, object>
                            {
                                { 1, false },
                                { 2, SharedResource.Tarjeta_No_Encontrada }
                            };
                        }
                    };

                    return _y;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }
    }
}
