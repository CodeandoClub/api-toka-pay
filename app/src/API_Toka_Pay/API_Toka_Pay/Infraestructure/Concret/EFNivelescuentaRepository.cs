﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Helpers;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models.FirmaElectronica.Input;
using API_Toka_Pay.Models.Mati.Input;
using API_Toka_Pay.Models.Mati.Output;
using API_Toka_Pay.Models.NivelesCuenta.Input;
using API_Toka_Pay.Models.Registro.Input;
using API_Toka_Pay.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFNivelesCuentaRepository : INivelesCuentaRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        private readonly IConfiguration _Configuration;
        #endregion

        public EFNivelesCuentaRepository(
            DBContext dBContext,
            IConfiguration iConfiguration
        )
        {
            _Context = dBContext;
            _Configuration = iConfiguration;
        }

        public async Task<Dictionary<int, object>> ActualizarNivelAsync(
            NivelesCuentaInputModel model
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                if (model.IdNivelCuenta == 1)
                {
                    var _Usuarios = _Context.Sp_tp_UsuariosObtener.FromSql(
                        $"call sp_tp_UsuariosObtener(@_NumeroTelefono,@_Correo,@_IdUsuario)",
                        new MySqlParameter("_NumeroTelefono", (object)model.NumeroTelefono ?? DBNull.Value),
                        new MySqlParameter("_Correo", DBNull.Value),
                        new MySqlParameter("_IdUsuario", DBNull.Value)
                    ).ToList();

                    // Generamos el request
                    var _Data = new CreateIdentityInputModel
                    {
                        IdApp = 1,
                        IdNivelCuenta = model.IdNivelCuenta,
                        IdUsuario = _Usuarios.FirstOrDefault().IdUsuario.ToString(),
                        Nombre = _Usuarios.FirstOrDefault().Nombre + " " + _Usuarios.FirstOrDefault().Paterno + " " + _Usuarios.FirstOrDefault().Materno
                    };

                    if (_Usuarios.Count > 0)
                    {
                        // Enviamos el código generico
                        var client = new HttpClient();

                        var httpRequestMessage = new HttpRequestMessage
                        {
                            Method = HttpMethod.Post,
                            RequestUri = new Uri(Path.Combine(_Configuration["URL:mati"], "createidentity")),
                            Headers = {
                                { HttpRequestHeader.ContentType.ToString(), "application/json" },
                                { HttpRequestHeader.Accept.ToString(), "application/json" },
                                // { HttpRequestHeader.Authorization.ToString(), "basic WFBTUlNGRkszVlY0UFRGQi9MRUJFVDBTQ1RHV0lHMFNXUVM4WkNTQ1ZCRT06UUlUVFFZTFBRVUhUR1lKTkpHQ0gzV01RUExZTkNMRVRBTVlBT0hVUEZBRz0=" }
                            },
                            Content = new StringContent(JsonConvert.SerializeObject(_Data), Encoding.UTF8, "application/json")
                        };

                        var response = await client.SendAsync(httpRequestMessage);

                        var contents = await response.Content.ReadAsStringAsync();

                        if (response.IsSuccessStatusCode)
                        {
                            var _Success = JsonConvert.DeserializeObject<CreateIdentitySuccessOutputModel>(contents);

                            var _DataDocuments = new VerificationInputModel
                            {
                                DocumentBack = model.IneBack,
                                DocumentFront = model.IneFront,
                                IdApp = 1,
                                IdUsuario = _Usuarios.FirstOrDefault().IdUsuario,
                                Passport = model.Passport,
                                Selfie = model.Selfie,
                                _Id = _Success.Id
                            };

                            // Mandamos los documentos
                            var clientDocuments = new HttpClient();

                            var httpRequestMessageDocuments = new HttpRequestMessage
                            {
                                Method = HttpMethod.Post,
                                RequestUri = new Uri(Path.Combine(_Configuration["URL:mati"], "verification")),
                                Headers = {
                                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                                    { HttpRequestHeader.Accept.ToString(), "application/json" },
                                    // { HttpRequestHeader.Authorization.ToString(), "basic WFBTUlNGRkszVlY0UFRGQi9MRUJFVDBTQ1RHV0lHMFNXUVM4WkNTQ1ZCRT06UUlUVFFZTFBRVUhUR1lKTkpHQ0gzV01RUExZTkNMRVRBTVlBT0hVUEZBRz0=" }
                                },
                                Content = new StringContent(JsonConvert.SerializeObject(_DataDocuments), Encoding.UTF8, "application/json")
                            };

                            var responseDocuments = await client.SendAsync(httpRequestMessageDocuments);

                            var contentsDocuments = await responseDocuments.Content.ReadAsStringAsync();

                            if (response.IsSuccessStatusCode)
                            {
                                var _SuccessDocuments = JsonConvert.DeserializeObject<List<CreateIdentitySuccessOutputModel>>(contents);

                                // Almacenamos los documentos

                                if (model.IneFront != null)
                                {
                                    byte[] _INE_Front = Convert.FromBase64String(model.IneFront);
                                    FileHelper.Save(_INE_Front, _Usuarios.FirstOrDefault().IdUsuario.ToString() + "_INE_FRONT" + ".jpg", "Mati");
                                }

                                if (model.IneBack != null)
                                {
                                    byte[] _INE_Back = Convert.FromBase64String(model.IneBack);
                                    FileHelper.Save(_INE_Back, _Usuarios.FirstOrDefault().IdUsuario.ToString() + "_INE_BACK" + ".jpg", "Mati");
                                }

                                if (model.Passport != null)
                                {
                                    byte[] _Passport = Convert.FromBase64String(model.Passport);
                                    FileHelper.Save(_Passport, _Usuarios.FirstOrDefault().IdUsuario.ToString() + "_PASSPORT" + ".jpg", "Mati");
                                }

                                if (model.Selfie != null)
                                {
                                    byte[] _Selfie = Convert.FromBase64String(model.Selfie);
                                    FileHelper.Save(_Selfie, _Usuarios.FirstOrDefault().IdUsuario.ToString() + "_SELFIE" + ".jpg", "Mati");
                                }

                                _y = new Dictionary<int, object>
                                {
                                    { 1, true },
                                    { 2, _Success }
                                };
                            }
                            else
                            {
                                var _Error = JsonConvert.DeserializeObject<VerificationErrorOutputModel>(contents);

                                _y = new Dictionary<int, object>
                                {
                                    { 1, false },
                                    { 2, _Error }
                                };
                            }
                        }
                        else
                        {
                            var _Error = JsonConvert.DeserializeObject<CreateIdentityErrorOutputModel>(contents);

                            _y = new Dictionary<int, object>
                            {
                                { 1, false },
                                { 2, _Error }
                            };
                        }
                    }
                    else
                    {
                        _y = new Dictionary<int, object>{
                            { 1, true },
                            { 2, SharedResource.Usuario_No_Se_Encontrado }
                        };
                    }
                }
                else
                {
                    _y = new Dictionary<int, object>{
                        { 1, false },
                        { 2, SharedResource.Nivel_No_Valido }
                    };
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }
    }
}
