﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models.Direcciones.Input;
using API_Toka_Pay.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFDireccionesRepository : IDireccionesRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        private readonly IConfiguration _Configuration;
        #endregion

        public EFDireccionesRepository(
            DBContext dBContext,
            IConfiguration iConfiguration
        )
        {
            _Context = dBContext;
            _Configuration = iConfiguration;
        }

        public async Task<Dictionary<int, object>> ActualizarAsync(
            DireccionesInputModel model
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                _y = await Task.Run(() =>
                {
                    var _tmp = new Dictionary<int, object>();

                    var x = _Context.Sp_tp_UsuariosDireccionActualizar.FromSql(
                        $"call sp_tp_UsuariosDireccionActualizar(@_IdUsuario,@_Calle,@_NumeroExterior,@_NumeroInterior,@_IdDireccion)",
                        new MySqlParameter("_IdUsuario", (object)model.IdUsuario ?? DBNull.Value),
                        new MySqlParameter("_Calle", (object)model.Calle ?? DBNull.Value),
                        new MySqlParameter("_NumeroExterior", (object)model.Numeroexterior ?? DBNull.Value),
                        new MySqlParameter("_NumeroInterior", (object)model.NumeroInterior ?? DBNull.Value),
                        new MySqlParameter("_IdDireccion", (object)model.IdDireccion ?? DBNull.Value)
                    ).ToList();

                    _tmp.Add(1, true);
                    _tmp.Add(2, x ?? new List<Sp_tp_UsuariosDireccionActualizar_Result>());

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }
    }
}
