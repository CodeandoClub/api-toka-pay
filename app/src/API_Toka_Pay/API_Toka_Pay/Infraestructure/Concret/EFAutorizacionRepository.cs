﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Helpers;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models.Autorizacion.Input;
using API_Toka_Pay.Models.Tarjetas.Output;
using API_Toka_Pay.Resources;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Infraestructure.Concret
{
    public class EFAutorizacionRepository : IAutorizacionRepository
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        private readonly ITarjetasRepository _ITarjetasRepository;
        #endregion

        public EFAutorizacionRepository(
            DBContext dBContext,
            ITarjetasRepository iTarjetasRepository
        )
        {
            _Context = dBContext;
            _ITarjetasRepository = iTarjetasRepository;
        }

        public async Task<Dictionary<int, object>> AuthAsync(
            AutorizacionInputModel model
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(async () =>
                {
                    var _tmp = new Dictionary<int, object>();
                    
                    // Obtenemos los datos del usuario
                    var _Usuarios = _Context.Sp_tp_UsuariosObtener.FromSql(
                        $"call sp_tp_UsuariosObtener(@_NumeroTelefono,@_Correo,@_IdUsuario)",
                        new MySqlParameter("_NumeroTelefono", DBNull.Value),
                        new MySqlParameter("_Correo", (object)model.Usuario ?? DBNull.Value),
                        new MySqlParameter("_IdUsuario", DBNull.Value)
                    ).ToList();

                    if(_Usuarios.Count > 0)
                    {
                        if (CriptoHelper.ValidarContrasenia(_Usuarios.FirstOrDefault().Contrasenia, model.Password, _Usuarios.FirstOrDefault().Salt))
                        {
                            // Obtenemos el ultimo acceso
                            var _Accesos = _Context.Sp_tp_UsuariosAccesosObtener.FromSql(
                                $"call sp_tp_UsuariosAccesosObtener(@_IdUsuario)",
                                new MySqlParameter("_IdUsuario", (object)_Usuarios.FirstOrDefault().IdUsuario ?? DBNull.Value)
                            ).ToList();

                            // Guardamos el acceso
                            var _DiccAcceso = _Context.Sp_tp_UsuariosAccesosAgregar.FromSql(
                                $"call sp_tp_UsuariosAccesosAgregar(@_IdUsuario,@_Latitud,@_Longitud)",
                                new MySqlParameter("_IdUsuario", (object)_Usuarios.FirstOrDefault().IdUsuario ?? DBNull.Value),
                                new MySqlParameter("_Latitud", (object)model.Latitud ?? DBNull.Value),
                                new MySqlParameter("_Longitud", (object)model.Longitud ?? DBNull.Value)
                            ).ToList();

                            // Obtenemos las tarjetas
                            var _DiccTarjetas = await _ITarjetasRepository.ObtenerAsync(_Usuarios.FirstOrDefault().IdUsuario);

                            if ((bool)_DiccTarjetas.Where(x => x.Key == 1).FirstOrDefault().Value)
                            {
                                var _Tarjetas = (TarjetasCollectionModel)_DiccTarjetas.Where(x => x.Key == 2).FirstOrDefault().Value;

                                _tmp.Add(1, true);
                                _tmp.Add(2, _Usuarios ?? new List<Sp_tp_UsuariosObtener_Result>());
                                _tmp.Add(3, _Accesos ?? new List<Sp_tp_UsuariosAccesosObtener_Result>());
                                _tmp.Add(4, _Tarjetas ?? new TarjetasCollectionModel());
                            }
                            else
                            {
                                _tmp.Add(1, true);
                                _tmp.Add(2, _Usuarios ?? new List<Sp_tp_UsuariosObtener_Result>());
                                _tmp.Add(3, _Accesos ?? new List<Sp_tp_UsuariosAccesosObtener_Result>());
                                _tmp.Add(4, new TarjetasCollectionModel());
                            }
                        }
                        else
                        {
                            _tmp.Add(1, false);
                            _tmp.Add(2, SharedResource.Usuario_No_Valido);
                        }
                    }
                    else
                    {
                        _tmp.Add(1, false);
                        _tmp.Add(2, SharedResource.Correo_No_Encontrado);
                    }

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> AuthBiometricoAsync(
            string Correo
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(async () =>
                {
                    var _tmp = new Dictionary<int, object>();

                    // Obtenemos los datos del usuario
                    var _Usuarios = _Context.Sp_tp_UsuariosObtener.FromSql(
                        $"call sp_tp_UsuariosObtener(@_NumeroTelefono,@_Correo,@_IdUsuario)",
                        new MySqlParameter("_NumeroTelefono", DBNull.Value),
                        new MySqlParameter("_Correo", (object)Correo ?? DBNull.Value),
                        new MySqlParameter("_IdUsuario", DBNull.Value)
                    ).ToList();

                    if (_Usuarios.Count > 0)
                    {
                        // Obtenemos el ultimo acceso
                        var _Accesos = _Context.Sp_tp_UsuariosAccesosObtener.FromSql(
                            $"call sp_tp_UsuariosAccesosObtener(@_IdUsuario)",
                            new MySqlParameter("_IdUsuario", (object)_Usuarios.FirstOrDefault().IdUsuario ?? DBNull.Value)
                        ).ToList();

                        // Guardamos el acceso
                        var _DiccAcceso = _Context.Sp_tp_UsuariosAccesosAgregar.FromSql(
                            $"call sp_tp_UsuariosAccesosAgregar(@_IdUsuario,@_Latitud,@_Longitud)",
                            new MySqlParameter("_IdUsuario", (object)_Usuarios.FirstOrDefault().IdUsuario ?? DBNull.Value),
                            new MySqlParameter("_Latitud", DBNull.Value),
                            new MySqlParameter("_Longitud", DBNull.Value)
                        ).ToList();

                        // Obtenemos las tarjetas
                        var _DiccTarjetas = await _ITarjetasRepository.ObtenerAsync(_Usuarios.FirstOrDefault().IdUsuario);

                        if ((bool)_DiccTarjetas.Where(x => x.Key == 1).FirstOrDefault().Value)
                        {
                            var _Tarjetas = (TarjetasCollectionModel)_DiccTarjetas.Where(x => x.Key == 2).FirstOrDefault().Value;

                            _tmp.Add(1, true);
                            _tmp.Add(2, _Usuarios ?? new List<Sp_tp_UsuariosObtener_Result>());
                            _tmp.Add(3, _Accesos ?? new List<Sp_tp_UsuariosAccesosObtener_Result>());
                            _tmp.Add(4, _Tarjetas ?? new TarjetasCollectionModel());
                        }
                        else
                        {
                            _tmp.Add(1, true);
                            _tmp.Add(2, _Usuarios ?? new List<Sp_tp_UsuariosObtener_Result>());
                            _tmp.Add(3, _Accesos ?? new List<Sp_tp_UsuariosAccesosObtener_Result>());
                            _tmp.Add(4, new TarjetasCollectionModel());
                        }
                    }
                    else
                    {
                        _tmp.Add(1, false);
                        _tmp.Add(2, SharedResource.Correo_No_Encontrado);
                    }

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }

        public async Task<Dictionary<int, object>> ObtenerNombreAsync(
            string Correo
        )
        {
            Dictionary<int, object> _y = new Dictionary<int, object>();

            try
            {
                // Validamos el código generico
                _y = await Task.Run(async () =>
                {
                    var _tmp = new Dictionary<int, object>();

                    // Obtenemos los datos del usuario
                    var _Usuarios = _Context.Sp_tp_UsuariosObtener.FromSql(
                        $"call sp_tp_UsuariosObtener(@_NumeroTelefono,@_Correo,@_IdUsuario)",
                        new MySqlParameter("_NumeroTelefono", DBNull.Value),
                        new MySqlParameter("_Correo", (object)Correo ?? DBNull.Value),
                        new MySqlParameter("_IdUsuario", DBNull.Value)
                    ).ToList();

                    if (_Usuarios.Count > 0)
                    {
                        _tmp.Add(1, true);
                        _tmp.Add(2, _Usuarios ?? new List<Sp_tp_UsuariosObtener_Result>());
                    }
                    else
                    {
                        _tmp.Add(1, false);
                        _tmp.Add(2, SharedResource.Correo_No_Encontrado);
                    }

                    return _tmp;
                });
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                _y = new Dictionary<int, object>{
                    { 1, false },
                    { 2, SharedResource.Excepcion_No_Controlada }
                };
            }

            return _y;
        }
    }
}
