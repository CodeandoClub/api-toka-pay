﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        #endregion

        // GET api/values
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<string>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ApiExplorerSettings(IgnoreApi = true)]
        // [Authorize]
        public ActionResult<IEnumerable<string>> Get()
        {
            _Logger.Debug("Ingresa al controlador.");

            var _IP = Request.HttpContext.Connection.RemoteIpAddress;
            // _IP = Request.HttpContext.Connection.LocalIpAddress;
            var port  = Request.HttpContext.Connection.LocalPort;
            // port = Request.HttpContext.Connection.RemotePort;

            return new string[] { "value1", "value2" };
        }
    }
}
