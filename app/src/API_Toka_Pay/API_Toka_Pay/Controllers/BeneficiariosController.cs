﻿using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Models.Beneficiarios.Output;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Threading.Tasks;
using API_Toka_Pay.Models.Beneficiarios.Input;
using API_Toka_Pay.Entities.Models;
using Microsoft.AspNetCore.Authorization;
using API_Toka_Pay.Models.Usuario.Output;
using API_Toka_Pay.Models.EstadosUsuario.Output;
using API_Toka_Pay.Models.EstadosUsuario.Input;
using API_Toka_Pay.Models.FirmaElectronica.Input;
using API_Toka_Pay.Models.FirmaElectronica.Output;
using API_Toka_Pay.Models.NivelesCuenta.Output;
using API_Toka_Pay.Models.NivelesCuenta.Input;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeneficiariosController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly IBeneficiariosRepository _IBeneficiariosRepository;
        #endregion

        public BeneficiariosController(
            IBeneficiariosRepository iBeneficiariosRepository
        )
        {
            _IBeneficiariosRepository = iBeneficiariosRepository;
        }

        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BeneficiariosCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Get(int IdUsuario)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Enviamos el mensaje
                var _DiccUsuario = await _IBeneficiariosRepository.ObtenerAsync(IdUsuario);

                if ((bool)_DiccUsuario.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Usuario = (List<Sp_tp_UsuariosBeneficiariosObtener_Result>)_DiccUsuario.Where(x => x.Key == 2).FirstOrDefault().Value;

                    if (_Usuario.Count > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new BeneficiariosCollectionModel
                        {
                            Data = new List<DataBeneficiariosCollectionModel>()
                        };

                        _Data.Data = _Usuario.Select(al => new DataBeneficiariosCollectionModel
                        {
                            Data = new BeneficiariosModel
                            {
                                Attributes = new BeneficiariosAttributesModel
                                {
                                    Correo = al.Correo,
                                    Direccion = al.Direccion,
                                    FechaNacimiento = al.FechaNacimiento,
                                    IdParentesco = al.IdParentesco,
                                    IdUsuario = al.IdUsuario,
                                    Materno = al.Materno,
                                    Nombre = al.Nombre,
                                    NumeroTelefono = al.NumeroTelefono,
                                    Paterno = al.Paterno
                                },
                                IdUsuario = al.IdUsuario,
                                Type = "Beneficiarios"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/beneficiarios?IdUsuario="+IdUsuario.ToString()
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Beneficiario_No_Encontrado
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BeneficiariosCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Put(BeneficiariosInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccBeneficiarios = await _IBeneficiariosRepository.ActualizarAsync(model);

                if ((bool)_DiccBeneficiarios.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Beneficiarios = (List<Sp_tp_UsuariosBeneficiariosActualizar_Result>)_DiccBeneficiarios.Where(x => x.Key == 2).FirstOrDefault().Value;

                    if (_Beneficiarios.Count > 0 && (int)_Beneficiarios.FirstOrDefault().Id > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new BeneficiariosActionCollectionModel
                        {
                            Data = new List<DataBeneficiariosActionCollectionModel>()
                        };

                        _Data.Data = _Beneficiarios.Select(al => new DataBeneficiariosActionCollectionModel
                        {
                            Data = new BeneficiariosActionModel
                            {
                                Attributes = new BeneficiariosActionAttributesModel
                                {
                                    Estado = "APROBADO"
                                },
                                IdUsuario = al.Id,
                                Type = "Beneficiarios"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/Beneficiarios"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = _Beneficiarios.FirstOrDefault().Id,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = _Beneficiarios.FirstOrDefault().Id.ToString(),
                            Title = _Beneficiarios.FirstOrDefault().Mensaje
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Resp = (string)_DiccBeneficiarios.Where(x => x.Key == 2).FirstOrDefault().Value;

                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = _Resp
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch(Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpDelete]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BeneficiariosCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Delete(int IdUsuario)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccBeneficiarios = await _IBeneficiariosRepository.EliminarAsync(IdUsuario);

                if ((bool)_DiccBeneficiarios.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Beneficiarios = (List<Sp_tp_UsuariosBeneficiariosActualizar_Result>)_DiccBeneficiarios.Where(x => x.Key == 2).FirstOrDefault().Value;

                    if (_Beneficiarios.Count > 0 && (int)_Beneficiarios.FirstOrDefault().Id > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new BeneficiariosActionCollectionModel
                        {
                            Data = new List<DataBeneficiariosActionCollectionModel>()
                        };

                        _Data.Data = _Beneficiarios.Select(al => new DataBeneficiariosActionCollectionModel
                        {
                            Data = new BeneficiariosActionModel
                            {
                                Attributes = new BeneficiariosActionAttributesModel
                                {
                                    Estado = "APROBADO"
                                },
                                IdUsuario = al.Id,
                                Type = "Beneficiarios"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/Beneficiarios"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = _Beneficiarios.FirstOrDefault().Id,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = _Beneficiarios.FirstOrDefault().Id.ToString(),
                            Title = _Beneficiarios.FirstOrDefault().Mensaje
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Resp = (string)_DiccBeneficiarios.Where(x => x.Key == 2).FirstOrDefault().Value;

                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = _Resp
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
