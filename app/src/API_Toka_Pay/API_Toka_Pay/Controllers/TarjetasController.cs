﻿using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.CambiarEstado.Input;
using API_Toka_Pay.Models.CambiarEstado.Output;
using API_Toka_Pay.Models.CambiarNIP.Input;
using API_Toka_Pay.Models.CambiarNIP.Output;
using API_Toka_Pay.Models.CambiarNombre.Input;
using API_Toka_Pay.Models.CambiarNombre.Output;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Models.Tarjetas.Output;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TarjetasController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly ITarjetasRepository _ITarjetasRepository;
        #endregion

        public TarjetasController(
            ITarjetasRepository iTarjetasRepository
        )
        {
            _ITarjetasRepository = iTarjetasRepository;
        }

        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CambiarEstadoCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Get(int IdUsuario)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Enviamos el mensaje
                var _DiccTarjetas = await _ITarjetasRepository.ObtenerAsync(IdUsuario);

                if ((bool)_DiccTarjetas.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Tarjetas = (TarjetasCollectionModel)_DiccTarjetas.Where(x => x.Key == 2).FirstOrDefault().Value;
                    var _Usuarios = (List<Sp_tp_UsuariosObtener_Result>)_DiccTarjetas.Where(x => x.Key == 3).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new TarjetasCollectionModel
                    {
                        Data = new List<DataTarjetasCollectionModel>()
                    };

                    _Data.Data = _Tarjetas.Data.Select(al => {
                        return new DataTarjetasCollectionModel
                        {
                            Data = new TarjetasModel
                            {
                                Attributes = new TarjetasAttributesModel
                                {
                                    IdUsuario = al.Data.Attributes.IdUsuario,
                                    CVV = al.Data.Attributes.CVV,
                                    Dispersiones = al.Data.Attributes.Dispersiones,
                                    Estado = al.Data.Attributes.Estado,
                                    NombreEmbosado = al.Data.Attributes.NombreEmbosado,
                                    NumeroTarjeta = al.Data.Attributes.NumeroTarjeta,
                                    ProxyNumber = al.Data.Attributes.ProxyNumber,
                                    Retiros = al.Data.Attributes.Retiros,
                                    Saldo = al.Data.Attributes.Saldo,
                                    Vigencia = al.Data.Attributes.Vigencia,
                                    Virtual = al.Data.Attributes.Virtual
                                },
                                CuentaClabe = _Usuarios.FirstOrDefault().CuentaClabe,
                                ProxyNumber = al.Data.Attributes.ProxyNumber,
                                Type = "Tarjetas"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/tarjetas?IdUsuario=" + IdUsuario.ToString()
                            }
                        };
                    }).ToList();

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Error = (ErrorModel)_DiccTarjetas.Where(x => x.Key == 2).FirstOrDefault().Value;

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPut("estado")]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CambiarEstadoCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Estado(string ProxyNumber, int IdUsuario, CambiarEstadoInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Enviamos el mensaje
                var _DiccCambiarEstado = await _ITarjetasRepository.CambiarEstadoAsync(ProxyNumber, IdUsuario, model.IdEstado);

                if ((bool)_DiccCambiarEstado.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _CambiarEstado = (TarjetasCollectionModel)_DiccCambiarEstado.Where(x => x.Key == 2).FirstOrDefault().Value;
                    
                    // Armamos la respuesta
                    var _Data = new CambiarEstadoCollectionModel
                    {
                        Data = new List<DataCambiarEstadoCollectionModel>()
                    };

                    _Data.Data.Add(new DataCambiarEstadoCollectionModel
                    {
                        Data = new CambiarEstadoModel
                        {
                            Attributes = new CambiarEstadoAttributesModel
                            {
                                Estado = "Aprobado"
                            },
                            ProxyNumber = ProxyNumber,
                            Type = "Estado"
                        },
                        Links = new LinksModel
                        {
                            Self = "/api/tarjetas/estado?ProxyNumber=" + ProxyNumber.ToString() + "?IdUsuario=" + IdUsuario.ToString()
                        }
                    });

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Error = (ErrorModel)_DiccCambiarEstado.Where(x => x.Key == 2).FirstOrDefault().Value;

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch(Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPut("nip")]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CambiarNIPCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Nip(string ProxyNumber, int IdUsuario, CambiarNIPInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Enviamos el mensaje
                var _DiccCambiarEstado = await _ITarjetasRepository.CambiarNIPAsync(ProxyNumber, IdUsuario, model.NIP);

                if ((bool)_DiccCambiarEstado.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _CambiarEstado = (TarjetasCollectionModel)_DiccCambiarEstado.Where(x => x.Key == 2).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new CambiarNIPCollectionModel
                    {
                        Data = new List<DataCambiarNIPCollectionModel>()
                    };

                    _Data.Data.Add(new DataCambiarNIPCollectionModel
                    {
                        Data = new CambiarNIPModel
                        {
                            Attributes = new CambiarNIPAttributesModel
                            {
                                Estado = "Aprobado"
                            },
                            ProxyNumber = ProxyNumber,
                            Type = "NIP"
                        },
                        Links = new LinksModel
                        {
                            Self = "/api/tarjetas/nip?ProxyNumber=" + ProxyNumber.ToString() + "?IdUsuario=" + IdUsuario.ToString()
                        }
                    });

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Error = (ErrorModel)_DiccCambiarEstado.Where(x => x.Key == 2).FirstOrDefault().Value;

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPut("nombreembosado")]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CambiarNombreCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> NombreEmbosado(string ProxyNumber, int IdUsuario, CambiarNombreInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Enviamos el mensaje
                var _DiccCambiarNombre = await _ITarjetasRepository.CambiarNombreAsync(ProxyNumber, IdUsuario, model.NombreEmbosado);

                if ((bool)_DiccCambiarNombre.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _CambiarNombre = (TarjetasCollectionModel)_DiccCambiarNombre.Where(x => x.Key == 2).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new CambiarNombreCollectionModel
                    {
                        Data = new List<DataCambiarNombreCollectionModel>()
                    };

                    _Data.Data.Add(new DataCambiarNombreCollectionModel
                    {
                        Data = new CambiarNombreModel
                        {
                            Attributes = new CambiarNombreAttributesModel
                            {
                                Estado = "Aprobado"
                            },
                            ProxyNumber = ProxyNumber,
                            Type = "NombreEmbosado"
                        },
                        Links = new LinksModel
                        {
                            Self = "/api/tarjetas/nombreembosado?ProxyNumber="+ProxyNumber.ToString()+"?IdUsuario="+IdUsuario.ToString()
                        }
                    });

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Error = (ErrorModel)_DiccCambiarNombre.Where(x => x.Key == 2).FirstOrDefault().Value;

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
