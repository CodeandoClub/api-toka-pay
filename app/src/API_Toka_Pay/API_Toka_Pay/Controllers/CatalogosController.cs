﻿using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Models.Autorizacion.Output;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Threading.Tasks;
using API_Toka_Pay.Models.Autorizacion.Input;
using API_Toka_Pay.Helpers;
using API_Toka_Pay.Models.Estados.Output;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Models.Generos.Output;
using API_Toka_Pay.Models.ActividadesEconomicas.Output;
using API_Toka_Pay.Models.Nacionalidades.Output;
using Microsoft.AspNetCore.Authorization;
using API_Toka_Pay.Models.Parentescos.Output;
using API_Toka_Pay.Models.Direcciones.Output;
using API_Toka_Pay.Models.Comisiones.Output;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogosController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly ICatalogosRepository _ICatalogosRepository;
        #endregion

        public CatalogosController(ICatalogosRepository iCatalogosRepository)
        {
            _ICatalogosRepository = iCatalogosRepository;
        }

        [HttpGet("estados")]
        // [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EstadosCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Estados(int? IdEstado = null)
        {
            ObjectResult _HttpResponse;

            try
            {
                var _DiccEstados = await _ICatalogosRepository.ObtenerEstadosAsync(IdEstado);

                if ((bool)_DiccEstados.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Estados = (List<Sp_tp_EstadosObtener_Result>)_DiccEstados.Where(x => x.Key == 2).FirstOrDefault().Value;
                    

                    if(_Estados.Count > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new EstadosCollectionModel
                        {
                            Data = new List<DataEstadosCollectionModel>()
                        };

                        _Data.Data = _Estados.Select(al => new DataEstadosCollectionModel
                        {
                            Data = new EstadosModel{
                                Attributes = new EstadosAttributesModel
                                {
                                    ClaveEstado = al.ClaveEstado,
                                    IdEstado = al.IdEstado,
                                    Nombre = al.Nombre
                                },
                                IdEstado = al.IdEstado,
                                Type = "Estados",
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/estados"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Excepcion_No_Controlada
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch(Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpGet("generos")]
        // [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GenerosCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Generos(int? IdGenero = null)
        {
            ObjectResult _HttpResponse;

            try
            {
                var _DiccGeneros = await _ICatalogosRepository.ObtenerGenerosAsync(IdGenero);

                if ((bool)_DiccGeneros.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Generos = (List<Sp_tp_GenerosObtener_Result>)_DiccGeneros.Where(x => x.Key == 2).FirstOrDefault().Value;


                    if (_Generos.Count > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new GenerosCollectionModel
                        {
                            Data = new List<DataGenerosCollectionModel>()
                        };

                        _Data.Data = _Generos.Select(al => new DataGenerosCollectionModel
                        {
                            Data = new GenerosModel
                            {
                                Attributes = new GenerosAttributesModel
                                {
                                    ClaveGenero = al.ClaveGenero,
                                    IdGenero = al.IdGenero,
                                    Nombre = al.Nombre
                                },
                                IdGenero = al.IdGenero,
                                Type = "Generos",
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/Generos"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Excepcion_No_Controlada
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpGet("actividadeseconomicas")]
        // [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ActividadesEconomicasCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> ActividadesEconomicas(int? IdActividadEconomica = null)
        {
            ObjectResult _HttpResponse;

            try
            {
                var _DiccActividadesEconomicas = await _ICatalogosRepository.ObtenerActividadesEconomicasAsync(IdActividadEconomica);

                if ((bool)_DiccActividadesEconomicas.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _ActividadesEconomicas = (List<Sp_tp_ActividadesEconomicasObtener_Result>)_DiccActividadesEconomicas.Where(x => x.Key == 2).FirstOrDefault().Value;


                    if (_ActividadesEconomicas.Count > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new ActividadesEconomicasCollectionModel
                        {
                            Data = new List<DataActividadesEconomicasCollectionModel>()
                        };

                        _Data.Data = _ActividadesEconomicas.Select(al => new DataActividadesEconomicasCollectionModel
                        {
                            Data = new ActividadesEconomicasModel
                            {
                                Attributes = new ActividadesEconomicasAttributesModel
                                {
                                    IdActividadEconomica = al.IdActividadEconomica,
                                    Nombre = al.Nombre
                                },
                                IdActividadEconomica = al.IdActividadEconomica,
                                Type = "ActividadesEconomicas",
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/actividadeseconomicas"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Excepcion_No_Controlada
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpGet("nacionalidades")]
        // [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(NacionalidadesCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Nacionalidades(int? IdNacionalidad = null)
        {
            ObjectResult _HttpResponse;

            try
            {
                var _DiccNacionalidades = await _ICatalogosRepository.ObtenerNacionalidadesAsync(IdNacionalidad);

                if ((bool)_DiccNacionalidades.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Nacionalidades = (List<Sp_tp_NacionalidadesObtener_Result>)_DiccNacionalidades.Where(x => x.Key == 2).FirstOrDefault().Value;


                    if (_Nacionalidades.Count > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new NacionalidadesCollectionModel
                        {
                            Data = new List<DataNacionalidadesCollectionModel>()
                        };

                        _Data.Data = _Nacionalidades.Select(al => new DataNacionalidadesCollectionModel
                        {
                            Data = new NacionalidadesModel
                            {
                                Attributes = new NacionalidadesAttributesModel
                                {
                                    ClaveNacionalidad = al.ClaveNacionalidad,
                                    IdNacionalidad = al.IdNacionalidad,
                                    Nombre = al.Nombre
                                },
                                IdNacionalidad = al.IdNacionalidad,
                                Type = "Nacionalidades",
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/Nacionalidades"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Excepcion_No_Controlada
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpGet("parentescos")]
        // [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ParentescosCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Parentescos(int? IdParentesco = null)
        {
            ObjectResult _HttpResponse;

            try
            {
                var _DiccParentescos = await _ICatalogosRepository.ObtenerParentescosAsync(IdParentesco);

                if ((bool)_DiccParentescos.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Parentescos = (List<Sp_tp_ParentescosObtener_Result>)_DiccParentescos.Where(x => x.Key == 2).FirstOrDefault().Value;


                    if (_Parentescos.Count > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new ParentescosCollectionModel
                        {
                            Data = new List<DataParentescosCollectionModel>()
                        };

                        _Data.Data = _Parentescos.Select(al => new DataParentescosCollectionModel
                        {
                            Data = new ParentescosModel
                            {
                                Attributes = new ParentescosAttributesModel
                                {
                                    IdParentesco = al.IdParentesco,
                                    Nombre = al.Nombre
                                },
                                IdParentesco = al.IdParentesco,
                                Type = "Parentescos",
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/parentescos"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Excepcion_No_Controlada
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpGet("direcciones")]
        // [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DireccionesCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Direcciones(int? IdDireccion = null, string CodigoPostal = null)
        {
            ObjectResult _HttpResponse;

            try
            {
                var _DiccDirecciones = await _ICatalogosRepository.ObtenerDireccionesAsync(IdDireccion, CodigoPostal);

                if ((bool)_DiccDirecciones.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Direcciones = (List<Sp_tp_DireccionesObtener_Result>)_DiccDirecciones.Where(x => x.Key == 2).FirstOrDefault().Value;


                    if (_Direcciones.Count > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new DireccionesCollectionModel
                        {
                            Data = new List<DataDireccionesCollectionModel>()
                        };

                        _Data.Data = _Direcciones.Select(al => new DataDireccionesCollectionModel
                        {
                            Data = new DireccionesModel
                            {
                                Attributes = new DireccionesAttributesModel
                                {
                                    Ciudad = al.Ciudad,
                                    CodigoPostal = al.CodigoPostal,
                                    Colonia = al.Colonia,
                                    Estado = al.Estado,
                                    IdDireccion = al.IdDireccion,
                                    Municipio = al.Municipio
                                },
                                IdDireccion = al.IdDireccion,
                                Type = "Direcciones",
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/direcciones"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Excepcion_No_Controlada
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpGet("comisiones")]
        // [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ComisionesCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Comisiones(int? IdComision = null)
        {
            ObjectResult _HttpResponse;

            try
            {
                var _DiccComisiones = await _ICatalogosRepository.ObtenerComisionesAsync(IdComision);

                if ((bool)_DiccComisiones.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Comisiones = (List<Sp_tp_ComisionesObtener_Result>)_DiccComisiones.Where(x => x.Key == 2).FirstOrDefault().Value;


                    if (_Comisiones.Count > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new ComisionesCollectionModel
                        {
                            Data = new List<DataComisionesCollectionModel>()
                        };

                        _Data.Data = _Comisiones.Select(al => new DataComisionesCollectionModel
                        {
                            Data = new ComisionesModel
                            {
                                Attributes = new ComisionesAttributesModel
                                {
                                    Base = al.Base,
                                    IdComision = al.IdComision,
                                    IVA = al.IVA,
                                    Costo = al.Costo,
                                    Nombre = al.Nombre,
                                    Tipo = al.Tipo
                                },
                                IdComision = al.IdComision,
                                Type = "Comisiones",
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/comisiones"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Excepcion_No_Controlada
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
