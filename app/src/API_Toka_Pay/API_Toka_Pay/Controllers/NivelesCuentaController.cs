﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.Mati.Output;
using API_Toka_Pay.Models.NivelesCuenta.Input;
using API_Toka_Pay.Models.NivelesCuenta.Output;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NivelesCuentaController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly INivelesCuentaRepository _INivelesCuentaRepository;
        #endregion

        public NivelesCuentaController(
            INivelesCuentaRepository iNivelesCuentaRepository
        )
        {
            _INivelesCuentaRepository = iNivelesCuentaRepository;
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(NivelesCuentaCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Post(NivelesCuentaInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccNivel = await _INivelesCuentaRepository.ActualizarNivelAsync(model);

                if ((bool)_DiccNivel.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Niveles = (List<VerificationSuccessOutputModel>)_DiccNivel.Where(x => x.Key == 2).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new NivelesCuentaCollectionModel
                    {
                        Data = new List<DataNivelesCuentaCollectionModel>()
                    };

                    string _MensajeMati = string.Empty;
                    string _TipoDocumento = string.Empty;
                    int _IdEstadoDocumento = 0;
                    int _ControlDocumentos = 0;

                    // Obtenemos los estados de los documentos
                    foreach (var _N in _Niveles)
                    {
                        if (model.Passport == null || model.Passport.Equals(""))
                        {
                            if (_ControlDocumentos == 0) { _TipoDocumento = "INE Frente"; }
                            if (_ControlDocumentos == 1) { _TipoDocumento = "INE Reverso"; }
                            if (_ControlDocumentos == 2) { _TipoDocumento = "Selfie"; }
                        }
                        else
                        {
                            if (_ControlDocumentos == 0) { _TipoDocumento = "Pasaporte"; }
                            if (_ControlDocumentos == 1) { _TipoDocumento = "Selfie"; }
                        }

                        if (_N.Error.Type == null || _N.Error.Code.Equals("input.locked"))
                        {
                            _IdEstadoDocumento = 3;
                        }
                        else
                        {
                            _IdEstadoDocumento = 1;
                            _MensajeMati = _N.Error.Type.ToString() + ": " + _N.Error.Code.ToString();
                        }

                        _Data.Data.Add(new DataNivelesCuentaCollectionModel
                        {
                            Data = new NivelesCuentaModel
                            {
                                Attributes = new NivelesCuentaAttributesModel
                                {
                                    Estado = _IdEstadoDocumento == 3 ? "APROBADO" : "PENDIENTE",
                                    Mensaje = _MensajeMati
                                },
                                NumeroTelefono = model.NumeroTelefono,
                                Type = "NivelesCuenta"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/nivelescuenta"
                            }
                        });

                        _ControlDocumentos++;
                    }

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
