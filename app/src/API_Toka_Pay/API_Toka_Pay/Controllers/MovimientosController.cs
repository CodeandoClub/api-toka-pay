﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.Mati.Output;
using API_Toka_Pay.Models.Movimientos.Input;
using API_Toka_Pay.Models.Movimientos.Output;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovimientosController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly IMovimientosRepository _IMovimientosRepository;
        #endregion

        public MovimientosController(
            IMovimientosRepository iMovimientosRepository
        )
        {
            _IMovimientosRepository = iMovimientosRepository;
        }

        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MovimientosCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Get(string ProxyNumber, int IdUsuario, int Mes, int Anio)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccMovimientos = await _IMovimientosRepository.ObtenerAsync(ProxyNumber, IdUsuario, Mes.ToString(), Anio.ToString());

                if ((bool)_DiccMovimientos.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Movimientos = (MovimientosCollectionModel)_DiccMovimientos.Where(x => x.Key == 2).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new MovimientosCollectionModel
                    {
                        Data = new List<DataMovimientosCollectionModel>()
                    };

                    _Data.Data = _Movimientos.Data.Select(al => {
                        return new DataMovimientosCollectionModel
                        {
                            Data = new MovimientosModel
                            {
                                Attributes = new MovimientosAttributesModel
                                {
                                    CARD_LAST_FOUR_DIGITS = al.Data.Attributes.CARD_LAST_FOUR_DIGITS,
                                    CLIENT_NAME = al.Data.Attributes.CLIENT_NAME,
                                    COD_AUTH = al.Data.Attributes.COD_AUTH,
                                    ID = al.Data.Attributes.ID,
                                    MERCHANT_CITY = al.Data.Attributes.MERCHANT_CITY,
                                    MERCHANT_NAME = al.Data.Attributes.MERCHANT_NAME,
                                    OPERATION_CODE = al.Data.Attributes.OPERATION_CODE,
                                    ORIGINAL_AMOUNT = al.Data.Attributes.ORIGINAL_AMOUNT,
                                    PROXYNUMBER = al.Data.Attributes.PROXYNUMBER,
                                    RUBRO = al.Data.Attributes.RUBRO,
                                    TRANSACTION_DATE = al.Data.Attributes.TRANSACTION_DATE,
                                    TRANSACTION_STATE  = al.Data.Attributes.TRANSACTION_STATE
                                },
                                ProxyNumber = ProxyNumber,
                                Type = "Movimientos"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/Movimientos?ProxyNumber=" + ProxyNumber + "&IdUsuario=" + IdUsuario.ToString() + "&Mes=" + Mes.ToString() + "&Anio=" + Anio.ToString()
                            }
                        };
                    }).ToList();

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Movimientos);
                }
                else
                {
                    var _Error = (ErrorModel)_DiccMovimientos.Where(x => x.Key == 2).FirstOrDefault().Value;

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
