﻿using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Models.Autorizacion.Output;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Threading.Tasks;
using API_Toka_Pay.Models.Autorizacion.Input;
using API_Toka_Pay.Helpers;
using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Models.Tarjetas.Output;
using API_Toka_Pay.Models.Correo.Output;
using Microsoft.AspNetCore.Authorization;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutorizacionController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly IAutorizacionRepository _IAutorizacionRepository;
        #endregion

        public AutorizacionController(IAutorizacionRepository iAutorizacionRepository)
        {
            _IAutorizacionRepository = iAutorizacionRepository;
        }

        [HttpGet]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CorreoCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        // [Authorize]
        public async Task<ActionResult> Get(string Correo)
        {
            ObjectResult _HttpResponse;

            try
            {
                var _DiccCorreo = await _IAutorizacionRepository.ObtenerNombreAsync(Correo);

                if ((bool)_DiccCorreo.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Usuario = (List<Sp_tp_UsuariosObtener_Result>)_DiccCorreo.Where(x => x.Key == 2).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new CorreoCollectionModel
                    {
                        Data = new List<DataCorreoCollectionModel>()
                    };

                    _Data.Data.Add(new DataCorreoCollectionModel
                    {
                        Data = new CorreoModel
                        {
                            Attributes = new CorreoAttributesModel
                            {
                                Nombre = _Usuario.FirstOrDefault().Nombre
                                // Paterno = _Usuario.FirstOrDefault().Paterno,
                                // Materno = _Usuario.FirstOrDefault().Materno
                            },
                            Correo = Correo,
                            Type = "Autorizacion"
                        },
                        Links = new LinksModel
                        {
                            Self = "/api/autorizacion?Correo=" + Correo.ToString()
                        }
                    });

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AutorizacionCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        // [Authorize]
        public async Task<ActionResult> Post(AutorizacionInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                var _DiccAutorizacion = await _IAutorizacionRepository.AuthAsync(model);

                if ((bool)_DiccAutorizacion.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Autorizacion = (List<Sp_tp_UsuariosObtener_Result>)_DiccAutorizacion.Where(x => x.Key == 2).FirstOrDefault().Value;
                    var _Accesos = (List<Sp_tp_UsuariosAccesosObtener_Result>)_DiccAutorizacion.Where(x => x.Key == 3).FirstOrDefault().Value;
                    var _Tarjetas = (TarjetasCollectionModel)_DiccAutorizacion.Where(x => x.Key == 4).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new AutorizacionCollectionModel
                    {
                        Data = new List<DataAutorizacionCollectionModel>()
                    };

                    bool _Biometrico = (model.TouchId == null ? false: model.TouchId) ?? false;

                    // Obtenemos el token
                    string _Token = JWTHelper.GenerarTokenJWT("", 1, false);

                    // Validamos el touchId
                    string _TokenBiometrico = null;

                    if (_Biometrico)
                    {
                        _TokenBiometrico = JWTHelper.GenerarTokenJWT("", 1, true);
                    }

                    _Data.Data.Add(new DataAutorizacionCollectionModel
                    {
                        Data = new AutorizacionModel {
                            Attributes = new AutorizacionAttributesModel
                            {
                                UltimoAcceso = _Accesos.FirstOrDefault().UltimoAcceso,
                                Latitud = _Accesos.FirstOrDefault().Latitud,
                                Longitud = _Accesos.FirstOrDefault().Longitud,
                                CuentaClabe = _Autorizacion.FirstOrDefault().CuentaClabe,
                                IdNivelCuenta = _Autorizacion.FirstOrDefault().IdNivel,
                                IdUsuario = _Autorizacion.FirstOrDefault().IdUsuario,
                                Materno = _Autorizacion.FirstOrDefault().Materno,
                                Nombre = _Autorizacion.FirstOrDefault().Nombre,
                                Paterno = _Autorizacion.FirstOrDefault().Paterno
                            },
                            IdUsuario = _Autorizacion.FirstOrDefault().IdUsuario,
                            Type = "Autorizacion",
                            Token = _Token,
                            TokenBiometrico = _TokenBiometrico
                        },
                        Links = new LinksModel
                        {
                            Self = "/api/autorizacion"
                        },
                        Relationships = new RelationshipTarjetasModel
                        {
                            Links = new LinksModel {
                                Self = "/api/tarjetas"
                            },
                            Data = _Tarjetas.Data == null ? null : _Tarjetas.Data.Select(x =>{ return x.Data.Attributes; }).ToList()
                        }
                    });

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Resp = (string)_DiccAutorizacion.Where(x => x.Key == 2).FirstOrDefault().Value;

                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = _Resp
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch(Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpGet("biometrico")]
        // [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AutorizacionCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Biometrico(string Correo)
        {
            ObjectResult _HttpResponse;

            try
            {
                var _DiccAutorizacion = await _IAutorizacionRepository.AuthBiometricoAsync(Correo);

                if ((bool)_DiccAutorizacion.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Autorizacion = (List<Sp_tp_UsuariosObtener_Result>)_DiccAutorizacion.Where(x => x.Key == 2).FirstOrDefault().Value;
                    var _Accesos = (List<Sp_tp_UsuariosAccesosObtener_Result>)_DiccAutorizacion.Where(x => x.Key == 3).FirstOrDefault().Value;
                    var _Tarjetas = (TarjetasCollectionModel)_DiccAutorizacion.Where(x => x.Key == 4).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new AutorizacionCollectionModel
                    {
                        Data = new List<DataAutorizacionCollectionModel>()
                    };

                    // Obtenemos el token
                    string _Token = JWTHelper.GenerarTokenJWT("", 1, false);

                    // Validamos el touchId
                    string _TokenBiometrico = JWTHelper.GenerarTokenJWT("", 1, true);

                    _Data.Data.Add(new DataAutorizacionCollectionModel
                    {
                        Data = new AutorizacionModel
                        {
                            Attributes = new AutorizacionAttributesModel
                            {
                                UltimoAcceso = _Accesos.FirstOrDefault().UltimoAcceso,
                                Latitud = _Accesos.FirstOrDefault().Latitud,
                                Longitud = _Accesos.FirstOrDefault().Longitud,
                                CuentaClabe = _Autorizacion.FirstOrDefault().CuentaClabe,
                                IdNivelCuenta = _Autorizacion.FirstOrDefault().IdNivel,
                                IdUsuario = _Autorizacion.FirstOrDefault().IdUsuario,
                                Materno = _Autorizacion.FirstOrDefault().Materno,
                                Nombre = _Autorizacion.FirstOrDefault().Nombre,
                                Paterno = _Autorizacion.FirstOrDefault().Paterno
                            },
                            IdUsuario = _Autorizacion.FirstOrDefault().IdUsuario,
                            Type = "Autorizacion",
                            Token = _Token,
                            TokenBiometrico = _TokenBiometrico
                        },
                        Links = new LinksModel
                        {
                            Self = "/api/autorizacion/biometrico"
                        },
                        Relationships = new RelationshipTarjetasModel
                        {
                            Links = new LinksModel
                            {
                                Self = "/api/tarjetas"
                            },
                            Data = _Tarjetas.Data == null ? null : _Tarjetas.Data.Select(x => { return x.Data.Attributes; }).ToList()
                        }
                    });

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Resp = (string)_DiccAutorizacion.Where(x => x.Key == 2).FirstOrDefault().Value;

                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = _Resp
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AutorizacionCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        // [Authorize]
        public async Task<ActionResult> Put(AutorizacionRefreshInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Validamos la firma del token
                if(JWTHelper.ValidarFirma(model.Token))
                {
                    // Armamos la respuesta
                    var _Data = new AutorizacionCollectionModel
                    {
                        Data = new List<DataAutorizacionCollectionModel>()
                    };

                    // Obtenemos el token
                    string _Token = JWTHelper.GenerarTokenJWT("", 1, false);

                    _Data.Data.Add(new DataAutorizacionCollectionModel
                    {
                        Data = new AutorizacionModel
                        {
                            Attributes = new AutorizacionAttributesModel
                            {

                            },
                            IdUsuario = 0,
                            Type = "Autorizacion",
                            Token = _Token
                        },
                        Links = new LinksModel
                        {
                            Self = "/api/autorizacion"
                        }
                    });

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Token_No_Valido
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
