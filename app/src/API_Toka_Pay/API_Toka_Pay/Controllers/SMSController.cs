﻿using API_Toka_Pay.Entities.Models;
using API_Toka_Pay.Helpers;
using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Models.SMS.Input;
using API_Toka_Pay.Models.SMS.Output;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Threading.Tasks;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SMSController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly ISMSRepository _ISMSRepository;
        private readonly IUsuariosRepository _IUsuariosRepository;
        #endregion

        public SMSController(
            ISMSRepository iSMSRepository,
            IUsuariosRepository iUsuariosRepository
        )
        {
            _ISMSRepository = iSMSRepository;
            _IUsuariosRepository = iUsuariosRepository;
        }

        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SMSCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        // [Authorize]
        public async Task<ActionResult> Get(string NumeroTelefono)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Enviamos el mensaje
                var _DiccSms = await _ISMSRepository.EnviarAsync(NumeroTelefono);

                if ((bool)_DiccSms.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Sms = (List<Sp_tp_SMSAgregar_Result>)_DiccSms.Where(x => x.Key == 2).FirstOrDefault().Value;
                    
                    if(_Sms.Count > 0 && (int)_Sms.FirstOrDefault().Id > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new SMSCollectionModel
                        {
                            Data = new List<DataSMSCollectionModel>()
                        };

                        _Data.Data = _Sms.Select(al => new DataSMSCollectionModel
                        {
                            Data = new SMSModel{
                                Attributes = new SMSAttributesModel
                                {
                                    NumeroTelefono = NumeroTelefono,
                                    Estado = "Aprobado"
                                },
                                NumeroTelefono = NumeroTelefono,
                                Type = "SMS"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/sms"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Excepcion_No_Controlada
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Resp = (string)_DiccSms.Where(x => x.Key == 2).FirstOrDefault().Value;

                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = _Resp
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch(Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SMSCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        // [Authorize]
        public async Task<ActionResult> Post(SMSComprobarInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Enviamos el mensaje
                var _DiccSms = await _ISMSRepository.ComprobarAsync(model);

                if ((bool)_DiccSms.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Sms = (List<Sp_tp_SMSActualizar_Result>)_DiccSms.Where(x => x.Key == 2).FirstOrDefault().Value;

                    if (_Sms.Count > 0 && (int)_Sms.FirstOrDefault().Id > 0)
                    {
                        // Obtenemos el estado del usuario
                        var _DiccUsuarios = await _IUsuariosRepository.ObtenerEstadoAsync(model.NumeroTelefono, null);

                        if ((bool)_DiccUsuarios.Where(x => x.Key == 1).FirstOrDefault().Value)
                        {
                            var _Usuario = (List<Sp_tp_UsuariosObtenerEstado_Result>)_DiccUsuarios.Where(x => x.Key == 2).FirstOrDefault().Value;

                            if (_Usuario.Count > 0 && (int)_Usuario.FirstOrDefault().IdEstadoUsuario > 0)
                            {
                                // Generamos el token
                                string _Token = JWTHelper.GenerarTokenTemporalJWT(model.NumeroTelefono);

                                // Armamos la respuesta
                                var _Data = new SMSCollectionModel
                                {
                                    Data = new List<DataSMSCollectionModel>()
                                };

                                _Data.Data = _Sms.Select(al => new DataSMSCollectionModel
                                {
                                    Data = new SMSModel
                                    {
                                        Attributes = new SMSAttributesModel
                                        {
                                            NumeroTelefono = model.NumeroTelefono,
                                            Estado = "Aprobado",
                                            IdEstadoUsuario = _Usuario.FirstOrDefault().IdEstadoUsuario
                                        },
                                        NumeroTelefono = model.NumeroTelefono,
                                        Token = _Token,
                                        Type = "SMS"
                                    },
                                    Links = new LinksModel
                                    {
                                        Self = "/api/sms"
                                    }
                                }).ToList();

                                _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                            }
                            else
                            {
                                var _Error = new ErrorModel
                                {
                                    IdError = 50000,
                                    Status = StatusCodes.Status500InternalServerError.ToString(),
                                    Code = "50000",
                                    Title = SharedResource.Estado_Usuario_No_Encontrado
                                };

                                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                            }
                        }
                        else
                        {
                            var _Error = new ErrorModel
                            {
                                IdError = 50000,
                                Status = StatusCodes.Status500InternalServerError.ToString(),
                                Code = "50000",
                                Title = SharedResource.Excepcion_No_Controlada
                            };

                            _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                        }
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Excepcion_No_Controlada
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    // Error personalizado
                    var _Resp = _DiccSms.Where(x => x.Key == 2).FirstOrDefault().Value;
                    var _Code = (int)_DiccSms.Where(x => x.Key == 3).FirstOrDefault().Value;

                    var _Error = new ErrorModel
                    {
                        IdError = _Code,
                        Status = _Code == 50000 ? StatusCodes.Status500InternalServerError.ToString() : StatusCodes.Status404NotFound.ToString(),
                        Code = _Code.ToString(),
                        Title = _Resp.ToString()
                    };

                    _HttpResponse = StatusCode(_Code == 50000 ? StatusCodes.Status500InternalServerError : StatusCodes.Status404NotFound, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpGet("validacion")]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SMSCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        // [Authorize]
        public async Task<ActionResult> GetValidacion(string NumeroTelefono)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Enviamos el mensaje
                var _DiccSms = await _ISMSRepository.EnviarValidacionAsync(NumeroTelefono);

                if ((bool)_DiccSms.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Sms = (List<Sp_tp_SMSValidacionAgregar_Result>)_DiccSms.Where(x => x.Key == 2).FirstOrDefault().Value;

                    if (_Sms.Count > 0 && (int)_Sms.FirstOrDefault().Id > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new SMSCollectionModel
                        {
                            Data = new List<DataSMSCollectionModel>()
                        };

                        _Data.Data = _Sms.Select(al => new DataSMSCollectionModel
                        {
                            Data = new SMSModel
                            {
                                Attributes = new SMSAttributesModel
                                {
                                    NumeroTelefono = NumeroTelefono,
                                    Estado = "Aprobado"
                                },
                                NumeroTelefono = NumeroTelefono,
                                Type = "SMSValidacion"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/sms/Validacion"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Excepcion_No_Controlada
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Resp = (string)_DiccSms.Where(x => x.Key == 2).FirstOrDefault().Value;

                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = _Resp
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPost("Validacion")]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SMSCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        // [Authorize]
        public async Task<ActionResult> PostValidacion(SMSComprobarInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Enviamos el mensaje
                var _DiccSms = await _ISMSRepository.ComprobarValidacionAsync(model);

                if ((bool)_DiccSms.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Sms = (List<Sp_tp_SMSValidacionActualizar_Result>)_DiccSms.Where(x => x.Key == 2).FirstOrDefault().Value;

                    if (_Sms.Count > 0 && (int)_Sms.FirstOrDefault().Id > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new SMSCollectionModel
                        {
                            Data = new List<DataSMSCollectionModel>()
                        };

                        _Data.Data = _Sms.Select(al => new DataSMSCollectionModel
                        {
                            Data = new SMSModel
                            {
                                Attributes = new SMSAttributesModel
                                {
                                    NumeroTelefono = model.NumeroTelefono,
                                    Estado = "Aprobado"
                                },
                                NumeroTelefono = model.NumeroTelefono,
                                Type = "SMSValidacion"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/sms/validacion"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Excepcion_No_Controlada
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    // Error personalizado
                    var _Resp = _DiccSms.Where(x => x.Key == 2).FirstOrDefault().Value;
                    var _Code = (int)_DiccSms.Where(x => x.Key == 3).FirstOrDefault().Value;

                    var _Error = new ErrorModel
                    {
                        IdError = _Code,
                        Status = _Code == 50000 ? StatusCodes.Status500InternalServerError.ToString() : StatusCodes.Status404NotFound.ToString(),
                        Code = _Code.ToString(),
                        Title = _Resp.ToString()
                    };

                    _HttpResponse = StatusCode(_Code == 50000 ? StatusCodes.Status500InternalServerError : StatusCodes.Status404NotFound, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
