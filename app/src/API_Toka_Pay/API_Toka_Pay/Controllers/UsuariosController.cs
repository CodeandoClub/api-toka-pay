﻿using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.Usuarios.Input;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using API_Toka_Pay.Models.Usuario.Output;
using API_Toka_Pay.Entities.Models;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly IUsuariosRepository _IUsuariosRepository;
        #endregion

        public UsuariosController(
            IUsuariosRepository iUsuariosRepository
        )
        {
            _IUsuariosRepository = iUsuariosRepository;
        }

        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UsuarioCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<IActionResult> Post(UsuariosInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccUsuarios = await _IUsuariosRepository.ActualizarPasswordAsync(model);

                if ((bool)_DiccUsuarios.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Usuario = (List<Sp_tp_UsuariosActualizarPassword_Result>)_DiccUsuarios.Where(x => x.Key == 2).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new UsuarioPasswordCollectionModel
                    {
                        Data = new List<DataUsuarioPasswordCollectionModel>()
                    };

                    _Data.Data = _Usuario.Select(al => {
                        return new DataUsuarioPasswordCollectionModel
                        {
                            Data = new UsuarioPasswordModel
                            {
                                Attributes = new UsuarioPasswordAttributesModel
                                {
                                    Estado = "APROBADO"
                                },
                                IdUsuario = model.IdUsuario,
                                Type = "Usuarios"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/usuarios"
                            }
                        };
                    }).ToList();

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Resp = (string)_DiccUsuarios.Where(x => x.Key == 2).FirstOrDefault().Value;
                    var _Estado = (int)_DiccUsuarios.Where(x => x.Key == 3).FirstOrDefault().Value;

                    var _Error = new ErrorModel
                    {
                        IdError = _Estado,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = _Estado.ToString(),
                        Title = _Resp
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
