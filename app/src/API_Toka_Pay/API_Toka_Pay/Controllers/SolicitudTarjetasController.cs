﻿using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Models.SolicitudesTarjetas.Input;
using API_Toka_Pay.Models.SolicitudesTarjetas.Output;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SolicitudTarjetasController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly ISolicitudTarjetasRepository _ISolicitudTarjetasRepository;
        #endregion

        public SolicitudTarjetasController(ISolicitudTarjetasRepository iSolicitudTarjetasRepository)
        {
            _ISolicitudTarjetasRepository = iSolicitudTarjetasRepository;
        }
        
        [HttpGet]
        // [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SolicitudesTarjetasCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<IActionResult> Get(int IdSolicitud, int IdUsuario)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccSolicitudTarjetas = await _ISolicitudTarjetasRepository.ObtenerAsync(IdSolicitud, IdUsuario);

                if ((bool)_DiccSolicitudTarjetas.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Solicitud = (SolicitudesTarjetasCollectionModel)_DiccSolicitudTarjetas.Where(x => x.Key == 2).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new SolicitudesTarjetasCollectionModel
                    {
                        Data = new List<DataSolicitudesTarjetasCollectionModel>()
                    };

                    _Data.Data = _Solicitud.Data.Select(al => {
                        return new DataSolicitudesTarjetasCollectionModel
                        {
                            Data = new SolicitudesTarjetasModel
                            {
                                Attributes = new SolicitudesTarjetasAttributesModel
                                {
                                    IdUsuario = al.Data.Attributes.IdUsuario,
                                    Estado = al.Data.Attributes.Estado,
                                    Virtual = al.Data.Attributes.Virtual,
                                    IdSolicitud = IdSolicitud,
                                    ProxyNumber = al.Data.Attributes.ProxyNumber
                                },
                                IdSolicitud = IdSolicitud,
                                Type = "Tarjetas"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/tarjetas?IdUsuario=" + IdUsuario.ToString()
                            }
                        };
                    }).ToList();

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Error = (ErrorModel)_DiccSolicitudTarjetas.Where(x => x.Key == 2).FirstOrDefault().Value;

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SolicitudesTarjetasCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<IActionResult> Post(SolicitudesTarjetasInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccSolicitudTarjetas = await _ISolicitudTarjetasRepository.AgregarAsync(model);

                if ((bool)_DiccSolicitudTarjetas.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Solicitud = (SolicitudesTarjetasCollectionModel)_DiccSolicitudTarjetas.Where(x => x.Key == 2).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new SolicitudesTarjetasCollectionModel
                    {
                        Data = new List<DataSolicitudesTarjetasCollectionModel>()
                    };

                    _Data.Data = _Solicitud.Data.Select(al => {
                        return new DataSolicitudesTarjetasCollectionModel
                        {
                            Data = new SolicitudesTarjetasModel
                            {
                                Attributes = new SolicitudesTarjetasAttributesModel
                                {
                                    IdUsuario = al.Data.Attributes.IdUsuario,
                                    Estado = al.Data.Attributes.Estado,
                                    Virtual = al.Data.Attributes.Virtual,
                                    IdSolicitud = al.Data.Attributes.IdSolicitud,
                                    ProxyNumber = al.Data.Attributes.ProxyNumber
                                },
                                IdSolicitud = al.Data.Attributes.IdSolicitud,
                                Type = "Tarjetas"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/tarjetas"
                            }
                        };
                    }).ToList();

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Error = (ErrorModel)_DiccSolicitudTarjetas.Where(x => x.Key == 2).FirstOrDefault().Value;

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
