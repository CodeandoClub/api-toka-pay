﻿using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.Cuentas.Output;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CuentasController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly ICuentasRepository _ICuentasRepository;
        #endregion

        public CuentasController(ICuentasRepository iCuentasRepository)
        {
            _ICuentasRepository = iCuentasRepository;
        }
        
        [HttpGet]
        // [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [ApiExplorerSettings(IgnoreApi = true)]
        // [Authorize]
        public async Task<IActionResult> Get()
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccFirma = await _ICuentasRepository.AgregarAsync();

                if ((bool)_DiccFirma.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    _HttpResponse = StatusCode(StatusCodes.Status200OK, "cuentas generadas.");
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
