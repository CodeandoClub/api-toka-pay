﻿using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Models.Registro.Output;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Threading.Tasks;
using API_Toka_Pay.Models.Registro.Input;
using API_Toka_Pay.Entities.Models;
using Microsoft.AspNetCore.Authorization;
using API_Toka_Pay.Models.Usuario.Output;
using API_Toka_Pay.Models.EstadosUsuario.Output;
using API_Toka_Pay.Models.EstadosUsuario.Input;
using API_Toka_Pay.Models.FirmaElectronica.Input;
using API_Toka_Pay.Models.FirmaElectronica.Output;
using API_Toka_Pay.Models.NivelesCuenta.Output;
using API_Toka_Pay.Models.NivelesCuenta.Input;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistroController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly IRegistroRepository _IRegistroRepository;
        private readonly IUsuariosRepository _IUsuariosRepository;
        #endregion

        public RegistroController(
            IRegistroRepository iRegistroRepository,
            IUsuariosRepository iUsuariosRepository
        )
        {
            _IRegistroRepository = iRegistroRepository;
            _IUsuariosRepository = iUsuariosRepository;
        }

        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UsuarioCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Get(string NumeroTelefono)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Enviamos el mensaje
                var _DiccUsuario = await _IRegistroRepository.ObtenerAsync(NumeroTelefono);

                if ((bool)_DiccUsuario.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Usuario = (List<Sp_tp_UsuariosObtener_Result>)_DiccUsuario.Where(x => x.Key == 2).FirstOrDefault().Value;

                    if (_Usuario.Count > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new UsuarioCollectionModel
                        {
                            Data = new List<DataUsuarioCollectionModel>()
                        };

                        _Data.Data = _Usuario.Select(al => new DataUsuarioCollectionModel
                        {
                            Data = new UsuarioModel
                            {
                                Attributes = new UsuarioAttributesModel
                                {
                                    Correo = al.Correo,
                                    FechaNacimiento = al.FechaNacimiento,
                                    IdActividadEconomica = al.IdActividadEconomica,
                                    IdEstado = al.IdEstado,
                                    IdEstadoUsuario = al.IdEstadoUsuario,
                                    IdGenero = al.IdGenero,
                                    IdNacionalidad = al.IdNacionalidad,
                                    IdUsuario = al.IdUsuario,
                                    Materno = al.Materno,
                                    Nombre = al.Nombre,
                                    NumeroTelefono = al.NumeroTelefono,
                                    Paterno = al.Paterno
                                },
                                IdUsuario = al.IdUsuario,
                                Type = "Registro"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/registro?NumeroTelefono="+NumeroTelefono
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = 50000,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = "50000",
                            Title = SharedResource.Usuario_No_Se_Encontrado
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(RegistroCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Put(string NumeroTelefono, RegistroInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccRegistro = await _IRegistroRepository.ActualizarAsync(NumeroTelefono, model);

                if ((bool)_DiccRegistro.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    if (model.IdRegistro == 3)
                    {
                        var _Registro = (List<Sp_tp_UsuariosActualizarPassword_Result>)_DiccRegistro.Where(x => x.Key == 2).FirstOrDefault().Value;

                        if (_Registro.Count > 0 && (int)_Registro.FirstOrDefault().Id > 0)
                        {
                            // Armamos la respuesta
                            var _Data = new RegistroCollectionModel
                            {
                                Data = new List<DataRegistroCollectionModel>()
                            };

                            _Data.Data = _Registro.Select(al => new DataRegistroCollectionModel
                            {
                                Data = new RegistroModel
                                {
                                    Attributes = new RegistroAttributesModel
                                    {
                                        NumeroTelefono = NumeroTelefono,
                                        Estado = "APROBADO"
                                    },
                                    IdUsuario = al.Id,
                                    Type = "Registro"
                                },
                                Links = new LinksModel
                                {
                                    Self = "/api/Registro?NumeroTelefono=" + NumeroTelefono
                                }
                            }).ToList();

                            _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                        }
                        else
                        {
                            var _Error = new ErrorModel
                            {
                                IdError = _Registro.FirstOrDefault().Id,
                                Status = StatusCodes.Status500InternalServerError.ToString(),
                                Code = _Registro.FirstOrDefault().Id.ToString(),
                                Title = _Registro.FirstOrDefault().Mensaje
                            };

                            _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                        }
                    }
                    else
                    {
                        var _Registro = (List<Sp_tp_UsuariosActualizar_Result>)_DiccRegistro.Where(x => x.Key == 2).FirstOrDefault().Value;

                        if (_Registro.Count > 0 && (int)_Registro.FirstOrDefault().Id > 0)
                        {
                            // Armamos la respuesta
                            var _Data = new RegistroCollectionModel
                            {
                                Data = new List<DataRegistroCollectionModel>()
                            };

                            _Data.Data = _Registro.Select(al => new DataRegistroCollectionModel
                            {
                                Data = new RegistroModel
                                {
                                    Attributes = new RegistroAttributesModel
                                    {
                                        NumeroTelefono = NumeroTelefono,
                                        Estado = "APROBADO"
                                    },
                                    IdUsuario = al.Id,
                                    Type = "Registro"
                                },
                                Links = new LinksModel
                                {
                                    Self = "/api/Registro?NumeroTelefono="+NumeroTelefono
                                }
                            }).ToList();

                            _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                        }
                        else
                        {
                            var _Error = new ErrorModel
                            {
                                IdError = _Registro.FirstOrDefault().Id,
                                Status = StatusCodes.Status500InternalServerError.ToString(),
                                Code = _Registro.FirstOrDefault().Id.ToString(),
                                Title = _Registro.FirstOrDefault().Mensaje
                            };

                            _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                        }
                    }
                }
                else
                {
                    var _Resp = (string)_DiccRegistro.Where(x => x.Key == 2).FirstOrDefault().Value;

                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = _Resp
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch(Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPut("estado")]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EstadosUsuarioCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> EstadoUsuario(string NumeroTelefono, EstadosUsuarioInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccEstadoUsuario = await _IUsuariosRepository.ActualizarEstadoAsync(model.IdEstadoUsuario, NumeroTelefono, null);

                if ((bool)_DiccEstadoUsuario.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _EstadoUsuario = (List<Sp_tp_UsuariosActualizarEstado_Result>)_DiccEstadoUsuario.Where(x => x.Key == 2).FirstOrDefault().Value;

                    if (_EstadoUsuario.Count > 0 && (int)_EstadoUsuario.FirstOrDefault().Id > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new EstadosUsuarioCollectionModel
                        {
                            Data = new List<DataEstadosUsuarioCollectionModel>()
                        };

                        _Data.Data = _EstadoUsuario.Select(al => new DataEstadosUsuarioCollectionModel
                        {
                            Data = new EstadosUsuarioModel
                            {
                                Attributes = new EstadosUsuarioAttributesModel
                                {
                                    NumeroTelefono = NumeroTelefono,
                                    Estado = "APROBADO",
                                    IdEstadoUsuario = model.IdEstadoUsuario
                                },
                                IdUsuario = al.Id,
                                Type = "Registro"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/registro/estado?NumeroTelefono=" + NumeroTelefono
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = _EstadoUsuario.FirstOrDefault().Id,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = _EstadoUsuario.FirstOrDefault().Id.ToString(),
                            Title = _EstadoUsuario.FirstOrDefault().Mensaje
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPost("firmaelectronica")]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FirmaElectronicaCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> FirmaElectronica(FirmaElectronicaInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccFirma = await _IRegistroRepository.AgregarFirmaAsync(model);

                if ((bool)_DiccFirma.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Firma = (List<Sp_tp_UsuariosAgregarFirmaElectronica_Result>)_DiccFirma.Where(x => x.Key == 2).FirstOrDefault().Value;

                    if (_Firma.Count > 0 && (int)_Firma.FirstOrDefault().Id > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new FirmaElectronicaCollectionModel
                        {
                            Data = new List<DataFirmaElectronicaCollectionModel>()
                        };

                        _Data.Data = _Firma.Select(al => new DataFirmaElectronicaCollectionModel
                        {
                            Data = new FirmaElectronicaModel
                            {
                                Attributes = new FirmaElectronicaAttributesModel
                                {
                                    Estado = "APROBADO"
                                },
                                NumeroTelefono = model.NumeroTelefono,
                                Type = "FirmaElectronica"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/registro/firmaelectronica"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = _Firma.FirstOrDefault().Id,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = _Firma.FirstOrDefault().Id.ToString(),
                            Title = _Firma.FirstOrDefault().Mensaje
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

        [HttpPost("nivelcuenta")]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(NivelesCuentaCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Nivelcuenta(NivelesCuentaInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccFirma = await _IRegistroRepository.ActualizarNivelAsync(model);

                if ((bool)_DiccFirma.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Firma = (List<Sp_tp_UsuariosAgregarFirmaElectronica_Result>)_DiccFirma.Where(x => x.Key == 2).FirstOrDefault().Value;

                    if (_Firma.Count > 0 && (int)_Firma.FirstOrDefault().Id > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new NivelesCuentaCollectionModel
                        {
                            Data = new List<DataNivelesCuentaCollectionModel>()
                        };

                        _Data.Data = _Firma.Select(al => new DataNivelesCuentaCollectionModel
                        {
                            Data = new NivelesCuentaModel
                            {
                                Attributes = new NivelesCuentaAttributesModel
                                {
                                    Estado = "APROBADO"
                                },
                                NumeroTelefono = model.NumeroTelefono,
                                Type = "NivelesCuenta"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/registro/nivelescuenta"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = _Firma.FirstOrDefault().Id,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = _Firma.FirstOrDefault().Id.ToString(),
                            Title = _Firma.FirstOrDefault().Mensaje
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = SharedResource.Excepcion_No_Controlada
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }

    }
}
