﻿using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Models.Direcciones.Output;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Threading.Tasks;
using API_Toka_Pay.Models.Direcciones.Input;
using API_Toka_Pay.Entities.Models;
using Microsoft.AspNetCore.Authorization;
using API_Toka_Pay.Models.Usuario.Output;
using API_Toka_Pay.Models.EstadosUsuario.Output;
using API_Toka_Pay.Models.EstadosUsuario.Input;
using API_Toka_Pay.Models.FirmaElectronica.Input;
using API_Toka_Pay.Models.FirmaElectronica.Output;
using API_Toka_Pay.Models.NivelesCuenta.Output;
using API_Toka_Pay.Models.NivelesCuenta.Input;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DireccionesController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly IDireccionesRepository _IDireccionesRepository;
        #endregion

        public DireccionesController(
            IDireccionesRepository iDireccionesRepository
        )
        {
            _IDireccionesRepository = iDireccionesRepository;
        }

        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DireccionesCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<ActionResult> Put(DireccionesInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccDirecciones = await _IDireccionesRepository.ActualizarAsync(model);

                if ((bool)_DiccDirecciones.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Direcciones = (List<Sp_tp_UsuariosDireccionActualizar_Result>)_DiccDirecciones.Where(x => x.Key == 2).FirstOrDefault().Value;

                    if (_Direcciones.Count > 0 && (int)_Direcciones.FirstOrDefault().Id > 0)
                    {
                        // Armamos la respuesta
                        var _Data = new DireccionesCollectionModel
                        {
                            Data = new List<DataDireccionesCollectionModel>()
                        };

                        _Data.Data = _Direcciones.Select(al => new DataDireccionesCollectionModel
                        {
                            Data = new DireccionesModel
                            {
                                Attributes = new DireccionesAttributesModel
                                {
                                    Estado = "APROBADO"
                                },
                                IdDireccion = al.Id,
                                Type = "Direcciones"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/Direcciones"
                            }
                        }).ToList();

                        _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                    }
                    else
                    {
                        var _Error = new ErrorModel
                        {
                            IdError = _Direcciones.FirstOrDefault().Id,
                            Status = StatusCodes.Status500InternalServerError.ToString(),
                            Code = _Direcciones.FirstOrDefault().Id.ToString(),
                            Title = _Direcciones.FirstOrDefault().Mensaje
                        };

                        _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                    }
                }
                else
                {
                    var _Resp = (string)_DiccDirecciones.Where(x => x.Key == 2).FirstOrDefault().Value;

                    var _Error = new ErrorModel
                    {
                        IdError = 50000,
                        Status = StatusCodes.Status500InternalServerError.ToString(),
                        Code = "50000",
                        Title = _Resp
                    };

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch(Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
