﻿using API_Toka_Pay.Infraestructure.Abstract;
using API_Toka_Pay.Models;
using API_Toka_Pay.Models.AsociacionesTarjetas.Input;
using API_Toka_Pay.Models.ResponsesCollection;
using API_Toka_Pay.Models.AsociacionesTarjetas.Input;
using API_Toka_Pay.Models.AsociacionesTarjetas.Output;
using API_Toka_Pay.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AsociacionesTarjetasController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly IAsociacionesTarjetasRepository _IAsociacionesTarjetasRepository;
        #endregion

        public AsociacionesTarjetasController(IAsociacionesTarjetasRepository iAsociacionesTarjetasRepository)
        {
            _IAsociacionesTarjetasRepository = iAsociacionesTarjetasRepository;
        }

        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AsociacionesTarjetasCollectionModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorModel))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<IActionResult> Post(AsociacionesTarjetasInputModel model)
        {
            ObjectResult _HttpResponse;

            try
            {
                // Registramos
                var _DiccAsociacionesTarjetas = await _IAsociacionesTarjetasRepository.AgregarAsync(model);

                if ((bool)_DiccAsociacionesTarjetas.Where(x => x.Key == 1).FirstOrDefault().Value)
                {
                    var _Solicitud = (AsociacionesTarjetasCollectionModel)_DiccAsociacionesTarjetas.Where(x => x.Key == 2).FirstOrDefault().Value;

                    // Armamos la respuesta
                    var _Data = new AsociacionesTarjetasCollectionModel
                    {
                        Data = new List<DataAsociacionesTarjetasCollectionModel>()
                    };

                    _Data.Data = _Solicitud.Data.Select(al => {
                        return new DataAsociacionesTarjetasCollectionModel
                        {
                            Data = new AsociacionesTarjetasModel
                            {
                                Attributes = new AsociacionesTarjetasAttributesModel
                                {
                                    IdUsuario = al.Data.Attributes.IdUsuario,
                                    Estado = al.Data.Attributes.Estado,
                                    Tarjeta = al.Data.Attributes.Tarjeta
                                },
                                Tarjeta = al.Data.Tarjeta,
                                Type = "Tarjetas"
                            },
                            Links = new LinksModel
                            {
                                Self = "/api/asociacionestarjetas"
                            }
                        };
                    }).ToList();

                    _HttpResponse = StatusCode(StatusCodes.Status200OK, _Data);
                }
                else
                {
                    var _Error = (ErrorModel)_DiccAsociacionesTarjetas.Where(x => x.Key == 2).FirstOrDefault().Value;

                    _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
                }
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                var _Error = new ErrorModel
                {
                    IdError = 50000,
                    Status = StatusCodes.Status500InternalServerError.ToString(),
                    Code = "50000",
                    Title = SharedResource.Excepcion_No_Controlada
                };

                _HttpResponse = StatusCode(StatusCodes.Status500InternalServerError, _Error);
            }

            return _HttpResponse;
        }
    }
}
