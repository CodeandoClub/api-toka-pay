﻿using API_Toka_Pay.Entities;
using API_Toka_Pay.Helpers;
using API_Toka_Pay.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace API_Toka_Pay.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentosController : ControllerBase
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly DBContext _Context;
        #endregion

        public DocumentosController(DBContext dBContext)
        {
            _Context = dBContext;
        }

        // GET api/values
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(byte[]))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorModel))]
        [Authorize]
        public async Task<IActionResult> Get(int IdDocumento)
        {
            try
            {
                // Obtenemos el documento
                // var path = Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot\\Documents\\Contrato.pdf");
                var _DiccDocumentos = _Context.Sp_tp_DocumentosAppObtener.FromSql(
                    $"call sp_tp_DocumentosAppObtener(@_IdDocumento)",
                    new MySqlParameter("_IdDocumento", (object)IdDocumento ?? DBNull.Value)
                ).ToList();

                if (_DiccDocumentos.Count > 0)
                {
                    var _Documento = FileHelper.Load(_DiccDocumentos.FirstOrDefault().Nombre, "Documentos");

                    return new FileStreamResult(_Documento, "application/pdf")
                    {
                        FileDownloadName = _DiccDocumentos.FirstOrDefault().Nombre
                    };
                }
                else
                {
                    return File(new byte[0], "application/pdf", "Documento_No_Encontrado.pdf");
                }
            }
            catch(Exception e)
            {
                _Logger.Error(e);

                // En caso de error devolvemos un archivo vacio
                return File(new byte[0], "application/pdf", "Error_Descarga.pdf");
            }
        }
    }
}
