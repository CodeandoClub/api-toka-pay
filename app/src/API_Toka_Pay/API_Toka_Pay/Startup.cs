﻿using System;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using API_Toka_Pay.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.OpenApi.Models;
using API_Toka_Pay.Infraestructure.Concret;
using API_Toka_Pay.Infraestructure.Abstract;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace API_Toka_Pay
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Configuramos nuestro DbContext
            services.AddDbContext<DBContext>(options => options.UseMySql(Configuration.GetConnectionString("Mysql")));

            // Configuración de JWT
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["JWT:Issuer"],
                        ValidAudience = Configuration["JWT:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(Configuration["JWT:ClaveSecreta"])
                        )
                    };
                });


            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(SwaggerConfiguration.DocNameV1,
                            new Microsoft.OpenApi.Models.OpenApiInfo
                            {
                                Title = SwaggerConfiguration.DocInfoTitle,
                                Version = SwaggerConfiguration.DocInfoVersion,
                                Description = SwaggerConfiguration.DocInfoDescription,
                                Contact = new Microsoft.OpenApi.Models.OpenApiContact
                                {
                                    Name = SwaggerConfiguration.ContactName,
                                    Url = new Uri(SwaggerConfiguration.ContactUrl)
                                },
                                //License = new OpenApiLicense
                                //{
                                //    Name = "Use under LICX",
                                //    Url = new Uri("https://example.com/license"),
                                //}
                            }
                );

                c.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    In = ParameterLocation.Header,
                    Description = "Please enter into field the word 'Bearer' following by space and JWT."
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "bearer"
                            }
                        },
                        new string[] {}
                    }
                });
            });

            // configuración para archivos estaticos
            services.AddSingleton<IFileProvider>(new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));

            // Inyección de dependencias
            services.AddTransient<ISMSRepository, EFSMSRepository>();
            services.AddTransient<IRegistroRepository, EFRegistroRepository>();
            services.AddTransient<IAutorizacionRepository, EFAutorizacionRepository>();
            services.AddTransient<IUsuariosRepository, EFUsuariosRepository>();
            services.AddTransient<ICatalogosRepository, EFCatalogosRepository>();
            services.AddTransient<ICuentasRepository, EFCuentasRepository>();
            services.AddTransient<INivelesCuentaRepository, EFNivelesCuentaRepository>();
            services.AddTransient<ITarjetasRepository, EFTarjetasRepository>();
            services.AddTransient<IMovimientosRepository, EFMovimientosRepository>();
            services.AddTransient<ISolicitudTarjetasRepository, EFSolicitudTarjetasRepository>();
            services.AddTransient<IAsociacionesTarjetasRepository, EFAsociacionesTarjetasRepository>();
            services.AddTransient<IBeneficiariosRepository, EFBeneficiariosRepository>();
            services.AddTransient<IDireccionesRepository, EFDireccionesRepository>();

            // Configuración para archivo de recursos
            services.AddLocalization(o => o.ResourcesPath = "Resources");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory //  Agregamos el factory de log4net
        )
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(SwaggerConfiguration.EndpointUrl, SwaggerConfiguration.EndpointDescription);
                // c.RoutePrefix = string.Empty;
            });

            // Respuestas personalizadas para peticiones no autorizadas
            app.UseStatusCodePages(async context =>
            {
                if (context.HttpContext.Request.Path.StartsWithSegments("/api") &&
                    (context.HttpContext.Response.StatusCode == 401 ||
                    context.HttpContext.Response.StatusCode == 403))
                {
                    await context.HttpContext.Response.WriteAsync("Unauthorized request");
                }
            });

            // Configuramos Http overrides
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor |
                ForwardedHeaders.XForwardedProto
            });

            // Agregamos la autenticación basica
            app.UseAuthentication();

            // app.UseHttpsRedirection();
            app.UseMvc();

            // Registramnos el middleware de log4net (Registro de sistema)
            loggerFactory.AddLog4Net();
        }
    }
}
