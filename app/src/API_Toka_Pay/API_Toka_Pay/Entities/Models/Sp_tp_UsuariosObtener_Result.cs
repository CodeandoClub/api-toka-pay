﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Entities.Models
{
    public class Sp_tp_UsuariosObtener_Result
    {
		[Key]
		public int IdUsuario { get; set; }
        public string Nombre { get; set; }
		public string Paterno { get; set; }
		public string Materno { get; set; }
		public DateTime? FechaNacimiento { get; set; }
		public string Correo { get; set; }
		public string NumeroTelefono { get; set; }
		public int? IdEstado { get; set; }
        public string ClaveEstado { get; set; }
		public int? IdGenero { get; set; }
        public string ClaveGenero { get; set; }
        public int? IdNacionalidad { get; set; }
		public int? IdActividadEconomica { get; set; }
        public int? IdEstadoUsuario { get; set; }
        public string Contrasenia { get; set; }
        public string Salt { get; set; }
        public int? IdNivel { get; set; }
        public string CuentaClabe { get; set; }
        public int? IdNivelPendiente { get; set; }
    }
}
