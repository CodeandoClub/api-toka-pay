﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Entities.Models
{
    public class Sp_tp_GenerosObtener_Result
    {
        [Key]
        public int IdGenero { get; set; }
        public string Nombre { get; set; }
        public string ClaveGenero { get; set; }
    }
}
