﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Entities.Models
{
    public class Sp_tp_ActividadesEconomicasObtener_Result
    {
        [Key]
        public int IdActividadEconomica { get; set; }
        public string Nombre { get; set; }
    }
}
