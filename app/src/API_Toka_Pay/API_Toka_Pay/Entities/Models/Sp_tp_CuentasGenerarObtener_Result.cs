﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Entities.Models
{
    public class Sp_tp_CuentasGenerarObtener_Result
    {
        [Key]
        public string NumeroTelefono { get; set; }
    }
}
