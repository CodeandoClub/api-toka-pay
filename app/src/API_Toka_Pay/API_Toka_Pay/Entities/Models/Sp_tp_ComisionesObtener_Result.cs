﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Entities.Models
{
    public class Sp_tp_ComisionesObtener_Result
    {
        [Key]
        public int IdComision { get; set; }
        public string Nombre { get; set; }
        public string Base { get; set; }
        public decimal Costo { get; set; }
        public string Tipo { get; set; }
        public bool IVA { get; set; }
    }
}
