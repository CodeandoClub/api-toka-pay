﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Entities.Models
{
    public class Sp_tp_UsuariosAccesosObtener_Result
    {
        [Key]
        public int IdUsuario { get; set; }
        public DateTime UltimoAcceso { get; set; }
        public decimal? Latitud { get; set; } = null;
        public decimal? Longitud { get; set; } = null;
    }
}
