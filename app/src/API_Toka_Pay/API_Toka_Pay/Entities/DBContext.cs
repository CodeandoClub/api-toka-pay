﻿using API_Toka_Pay.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Entities
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions Options) : base(Options)
        {

        }

        // Declaramos las tablas
        public DbSet<Sp_tp_SMSAgregar_Result> Sp_tp_SMSAgregar { get; set; }
        public DbSet<Sp_tp_SMSActualizar_Result> Sp_tp_SMSActualizar { get; set; }
        public DbSet<Sp_tp_UsuariosObtenerEstado_Result> Sp_tp_UsuariosObtenerEstado { get; set; }
        public DbSet<Sp_tp_UsuariosActualizar_Result> Sp_tp_UsuariosActualizar { get; set; }
        public DbSet<Sp_tp_UsuariosActualizarPassword_Result> Sp_tp_UsuariosActualizarPassword { get; set; }
        public DbSet<Sp_tp_UsuariosObtener_Result> Sp_tp_UsuariosObtener { get; set; }
        public DbSet<Sp_tp_SMSValidar_Result> Sp_tp_SMSValidar { get; set; }
        public DbSet<Sp_tp_UsuariosActualizarEstado_Result> Sp_tp_UsuariosActualizarEstado { get; set; }
        public DbSet<Sp_tp_EstadosObtener_Result> Sp_tp_EstadosObtener { get; set; }
        public DbSet<Sp_tp_GenerosObtener_Result> Sp_tp_GenerosObtener { get; set; }
        public DbSet<Sp_tp_ActividadesEconomicasObtener_Result> Sp_tp_ActividadesEconomicasObtener { get; set; }
        public DbSet<Sp_tp_NacionalidadesObtener_Result> Sp_tp_NacionalidadesObtener { get; set; }
        public DbSet<Sp_tp_UsuariosAgregarFirmaElectronica_Result> Sp_tp_UsuariosAgregarFirmaElectronica { get; set; }
        public DbSet<Sp_tp_UsuariosAccesosAgregar_Result> Sp_tp_UsuariosAccesosAgregar { get; set; }
        public DbSet<Sp_tp_CuentasGenerarObtener_Result> Sp_tp_CuentasGenerarObtener { get; set; }
        public DbSet<Sp_tp_CuentasGenerarAgregar_Result> Sp_tp_CuentasGenerarAgregar { get; set; }
        public DbSet<Sp_tp_UsuariosAccesosObtener_Result> Sp_tp_UsuariosAccesosObtener { get; set; }
        public DbSet<Sp_tp_UsuariosCuentaClabeAgregar_Result> Sp_tp_UsuariosCuentaClabeAgregar { get; set; }
        public DbSet<Sp_tp_CuentasValidarObtener_Result> Sp_tp_CuentasValidarObtener { get; set; }
        public DbSet<Sp_tp_CuentasValidarActualizar_Result> Sp_tp_CuentasValidarActualizar { get; set; }
        public DbSet<Sp_tp_DocumentosAppObtener_Result> Sp_tp_DocumentosAppObtener { get; set; }
        public DbSet<Sp_tp_SMSValidacionAgregar_Result> Sp_tp_SMSValidacionAgregar { get; set; }
        public DbSet<Sp_tp_SMSValidacionActualizar_Result> Sp_tp_SMSValidacionActualizar { get; set; }
        public DbSet<Sp_tp_ParentescosObtener_Result> Sp_tp_ParentescosObtener { get; set; }
        public DbSet<Sp_tp_UsuariosBeneficiariosActualizar_Result> Sp_tp_UsuariosBeneficiariosActualizar { get; set; }
        public DbSet<Sp_tp_UsuariosBeneficiariosObtener_Result> Sp_tp_UsuariosBeneficiariosObtener { get; set; }
        public DbSet<Sp_tp_DireccionesObtener_Result> Sp_tp_DireccionesObtener { get; set; }
        public DbSet<Sp_tp_ComisionesObtener_Result> Sp_tp_ComisionesObtener { get; set; }
        public DbSet<Sp_tp_UsuariosDireccionActualizar_Result> Sp_tp_UsuariosDireccionActualizar { get; set; }

        // Agregamos el mapeo de los objetos 
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Sp_tp_SMSAgregar_Result>().Property(e => e.Id).HasConversion(v => v, v => Convert.ToInt32(v));
            modelBuilder.Entity<Sp_tp_SMSActualizar_Result>().Property(e => e.Id).HasConversion(v => v, v => Convert.ToInt32(v));
            modelBuilder.Entity<Sp_tp_UsuariosActualizar_Result>().Property(e => e.Id).HasConversion(v => v, v => Convert.ToInt32(v));
        }
    }
}
