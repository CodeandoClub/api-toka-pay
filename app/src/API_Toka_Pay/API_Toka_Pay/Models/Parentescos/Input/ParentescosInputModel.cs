﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Parentescos.Input
{
    public class ParentescosInputModel
    {
        public int IdParentesco { get; set; }
    }
}
