﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Parentescos.Output
{
    [DataContract]
    public class ParentescosModel
    {
        [DataMember]
        public int IdParentesco { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public ParentescosAttributesModel Attributes { get; set; }
    }
}
