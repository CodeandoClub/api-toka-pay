﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Mati.Output
{
    public class CreateIdentitySuccessOutputModel
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("alive")]
        public object Alive { get; set; }

        [JsonProperty("dateCreated")]
        public DateTimeOffset DateCreated { get; set; }

        [JsonProperty("dateUpdated")]
        public DateTimeOffset DateUpdated { get; set; }

        [JsonProperty("flowId")]
        public string FlowId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }
    }
}
