﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Mati.Input
{
    public class VerificationInputModel
    {
        public int IdUsuario { get; set; }
        public int IdApp { get; set; }
        public string _Id { get; set; }
        public string DocumentFront { get; set; }
        public string DocumentBack { get; set; }
        public string Passport { get; set; }
        public string Selfie { get; set; }
    }
}
