﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Mati.Input
{
    public class NivelesCuentaInputModelRequest
    {
        public int IdNivel { get; set; }
    }
}
