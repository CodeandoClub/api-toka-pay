﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Mati.Input
{
    public class CreateIdentityInputModel
    {
        public string Nombre { get; set; }
        public string IdUsuario { get; set; }
        public int IdApp { get; set; }
        public int IdNivelCuenta { get; set; }
    }
}
