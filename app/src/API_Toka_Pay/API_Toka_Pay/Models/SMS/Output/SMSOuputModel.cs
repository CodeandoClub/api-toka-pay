﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.SMS.Output
{
    public class SMSOuputModel
    {
        [JsonProperty("Data")]
        public Datum[] Data { get; set; }

        [JsonProperty("Incluided")]
        public object Incluided { get; set; }
    }

    public partial class Datum
    {
        [JsonProperty("Links")]
        public Links Links { get; set; }

        [JsonProperty("Data")]
        public Data Data { get; set; }

        [JsonProperty("Relationships")]
        public object Relationships { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("Celular")]
        public string Celular { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("Atributes")]
        public Attributes Attributes { get; set; }
    }

    public partial class Attributes
    {
        [JsonProperty("Celular")]
        public string Celular { get; set; }

        [JsonProperty("Estado")]
        public string Estado { get; set; }
    }

    public partial class Links
    {
        [JsonProperty("Self")]
        public Uri Self { get; set; }
    }

    public partial class SmsOuputModel
    {
        public static SmsOuputModel FromJson(string json) => JsonConvert.DeserializeObject<SmsOuputModel>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this SmsOuputModel self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
