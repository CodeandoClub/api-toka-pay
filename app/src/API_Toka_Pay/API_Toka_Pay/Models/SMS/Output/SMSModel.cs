﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.SMS.Output
{
    [DataContract]
    public class SMSModel
    {
        [DataMember]
        public string NumeroTelefono { get; set; }
        [DataMember]
        public string Token { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public SMSAttributesModel Attributes { get; set; }
    }
}
