﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.SMS.Input
{
    public class SMSInputModel
    {
        public string Celular { get; set; }
        public string Texto { get; set; }
    }
}
