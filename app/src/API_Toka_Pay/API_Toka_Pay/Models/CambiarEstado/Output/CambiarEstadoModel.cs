﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.CambiarEstado.Output
{
    [DataContract]
    public class CambiarEstadoModel
    {
        [DataMember]
        public string ProxyNumber { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public CambiarEstadoAttributesModel Attributes { get; set; }
    }
}
