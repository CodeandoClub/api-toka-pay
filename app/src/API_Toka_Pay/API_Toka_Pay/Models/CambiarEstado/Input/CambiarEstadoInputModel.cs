﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.CambiarEstado.Input
{
    public class CambiarEstadoInputModel
    {
        public int IdEstado { get; set; }
    }
}
