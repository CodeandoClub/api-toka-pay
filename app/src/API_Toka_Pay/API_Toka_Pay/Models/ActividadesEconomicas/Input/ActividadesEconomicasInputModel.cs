﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.ActividadesEconomicass.Input
{
    public class EstadosInputModel
    {
        public int IdActividadEconomica { get; set; }
    }
}
