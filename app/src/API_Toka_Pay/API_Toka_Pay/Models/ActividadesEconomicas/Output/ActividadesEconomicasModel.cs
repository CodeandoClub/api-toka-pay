﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.ActividadesEconomicas.Output
{
    [DataContract]
    public class ActividadesEconomicasModel
    {
        [DataMember]
        public int IdActividadEconomica { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public ActividadesEconomicasAttributesModel Attributes { get; set; }
    }
}
