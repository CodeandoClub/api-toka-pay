﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.ActividadesEconomicas.Output
{
    [DataContract]
    public class ActividadesEconomicasAttributesModel
    {
        [DataMember]
        public int IdActividadEconomica { get; set; }
        [DataMember]
        public string Nombre { get; set; }
    }
}
