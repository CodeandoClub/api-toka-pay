﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.FirmaElectronica.Output
{
    [DataContract]
    public class FirmaElectronicaAttributesModel
    {
        [DataMember]
        public string Estado { get; set; }
    }
}
