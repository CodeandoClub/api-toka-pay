﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.FirmaElectronica.Output
{
    [DataContract]
    public class FirmaElectronicaModel
    {
        [DataMember]
        public string NumeroTelefono { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public FirmaElectronicaAttributesModel Attributes { get; set; }
    }
}
