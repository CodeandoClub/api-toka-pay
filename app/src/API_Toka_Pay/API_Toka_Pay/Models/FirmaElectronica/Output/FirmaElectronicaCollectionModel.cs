﻿using API_Toka_Pay.Models.ResponsesCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.FirmaElectronica.Output
{
    [DataContract]
    public class FirmaElectronicaCollectionModel
    {
        [DataMember]
        public List<DataFirmaElectronicaCollectionModel> Data { get; set; }
        [DataMember]
        public List<IncluidedCollectionModel> Incluided { get; set; }
    }
}
