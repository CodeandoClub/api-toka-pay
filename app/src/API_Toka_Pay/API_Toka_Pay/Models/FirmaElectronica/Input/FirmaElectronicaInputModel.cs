﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.FirmaElectronica.Input
{
    public class FirmaElectronicaInputModel
    {
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public string NumeroTelefono { get; set; }
        public string Imagen { get; set; }
    }
}
