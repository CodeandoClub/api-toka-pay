﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Estados.Input
{
    public class EstadosInputModel
    {
        public int IdEstado { get; set; }
    }
}
