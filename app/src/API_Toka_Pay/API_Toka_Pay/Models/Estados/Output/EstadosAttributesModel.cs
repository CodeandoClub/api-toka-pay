﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Estados.Output
{
    [DataContract]
    public class EstadosAttributesModel
    {
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string ClaveEstado { get; set; }
    }
}
