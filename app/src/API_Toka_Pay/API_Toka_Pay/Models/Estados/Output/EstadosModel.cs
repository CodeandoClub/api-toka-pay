﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Estados.Output
{
    [DataContract]
    public class EstadosModel
    {
        [DataMember]
        public int IdEstado { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public EstadosAttributesModel Attributes { get; set; }
    }
}
