﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Registro.Input
{
    public class RegistroInputModel
    {
        public string Correo { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public int? IdGenero { get; set; }
        public int? IdNacionalidad { get; set; }
        public int? IdEstado { get; set; }
        public int? IdActividadEconomica { get; set; }
        public string FechaNacimiento { get; set; }
        public string Password { get; set; }
        public string Password2 { get; set; }
        public int IdRegistro { get; set; }
    }
}
