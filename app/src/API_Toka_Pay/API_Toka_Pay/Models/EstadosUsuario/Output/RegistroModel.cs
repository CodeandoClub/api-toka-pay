﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Registro.Output
{
    [DataContract]
    public class RegistroModel
    {
        [DataMember]
        public long IdUsuario { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public RegistroAttributesModel Attributes { get; set; }
    }
}
