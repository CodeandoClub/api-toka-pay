﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Registro.Output
{
    public class RegistroAttributesModel
    {
        public string NumeroTelefono { get; set; }
        public string Estado { get; set; }
    }
}
