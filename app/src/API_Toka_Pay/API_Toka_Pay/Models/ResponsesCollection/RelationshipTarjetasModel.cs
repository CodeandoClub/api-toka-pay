﻿using API_Toka_Pay.Models.Tarjetas.Output;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.ResponsesCollection
{
    [DataContract]
    public class RelationshipTarjetasModel
    {
        [DataMember]
        public LinksModel Links { get; set; }
        [DataMember]
        public List<TarjetasAttributesModel> Data { get; set; }
    }
}
