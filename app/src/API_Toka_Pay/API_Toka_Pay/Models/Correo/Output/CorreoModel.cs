﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Correo.Output
{
    [DataContract]
    public class CorreoModel
    {
        [DataMember]
        public string Correo { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public CorreoAttributesModel Attributes { get; set; }
    }
}
