﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Generos.Output
{
    [DataContract]
    public class GenerosModel
    {
        [DataMember]
        public int IdGenero { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public GenerosAttributesModel Attributes { get; set; }
    }
}
