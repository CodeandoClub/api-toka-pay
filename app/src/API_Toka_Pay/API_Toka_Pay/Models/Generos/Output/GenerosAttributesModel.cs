﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Generos.Output
{
    [DataContract]
    public class GenerosAttributesModel
    {
        [DataMember]
        public int IdGenero { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string ClaveGenero { get; set; }
    }
}
