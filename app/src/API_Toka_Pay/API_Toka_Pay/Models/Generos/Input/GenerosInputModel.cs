﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Generos.Input
{
    public class GenerosInputModel
    {
        public int IdGenero { get; set; }
    }
}
