﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Nacionalidades.Output
{
    [DataContract]
    public class NacionalidadesAttributesModel
    {
        [DataMember]
        public int IdNacionalidad { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string ClaveNacionalidad { get; set; }
    }
}
