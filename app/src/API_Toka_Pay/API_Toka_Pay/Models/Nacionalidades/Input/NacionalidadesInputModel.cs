﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Nacionalidades.Input
{
    public class NacionalidadesInputModel
    {
        public int IdNacionalidad { get; set; }
    }
}
