﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Autorizacion.Input
{
    public class AutorizacionInputModel
    {
        public string Usuario { get; set; }
        public string Password { get; set; }
        public decimal? Latitud { get; set; } = null;
        public decimal? Longitud { get; set; } = null;
        public bool? TouchId { get; set; } = null;
    }
}
