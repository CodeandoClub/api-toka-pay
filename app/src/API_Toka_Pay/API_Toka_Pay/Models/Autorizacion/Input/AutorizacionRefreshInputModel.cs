﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Autorizacion.Input
{
    public class AutorizacionRefreshInputModel
    {
        public string Token { get; set; }
    }
}
