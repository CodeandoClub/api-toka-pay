﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Autorizacion.Output
{
    [DataContract]
    public class AutorizacionModel
    {
        [DataMember]
        public long IdUsuario { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public AutorizacionAttributesModel Attributes { get; set; }
        [DataMember]
        public string Token { get; set; }
        [DataMember]
        public string TokenBiometrico { get; set; } = null;
    }
}
