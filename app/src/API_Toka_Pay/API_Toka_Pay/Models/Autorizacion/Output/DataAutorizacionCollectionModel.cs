﻿using API_Toka_Pay.Models.ResponsesCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Autorizacion.Output
{
    [DataContract]
    public class DataAutorizacionCollectionModel
    {
        [DataMember]
        public LinksModel Links { get; set; }
        [DataMember]
        public AutorizacionModel Data { get; set; }
        [DataMember]
        public RelationshipTarjetasModel Relationships { get; set; }
    }
}
