﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Autorizacion.Output
{
    [DataContract]
    public class AutorizacionAttributesModel
    {
        [DataMember]
        public DateTime UltimoAcceso { get; set; }
        [DataMember]
        public decimal? Latitud { get; set; }
        [DataMember]
        public decimal? Longitud { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Paterno { get; set; }
        [DataMember]
        public string Materno { get; set; }
        [DataMember]
        public int? IdNivelCuenta { get; set; }
        [DataMember]
        public string CuentaClabe { get; set; }
        [DataMember]
        public int IdUsuario { get; set; }
    }
}
