﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.CambiarNombre.Input
{
    public class CambiarNombreInputModel
    {
        public string NombreEmbosado { get; set; }
    }
}
