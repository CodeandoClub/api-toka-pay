﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.CambiarNombre.Output
{
    [DataContract]
    public class CambiarNombreModel
    {
        [DataMember]
        public string ProxyNumber { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public CambiarNombreAttributesModel Attributes { get; set; }
    }
}
