﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.CambiarNombre.Output
{
    [DataContract]
    public class CambiarNombreAttributesModel
    {
        [DataMember]
        public string Estado { get; set; }
    }
}
