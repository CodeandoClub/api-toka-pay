﻿using API_Toka_Pay.Models.ResponsesCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.CambiarNombre.Output
{
    [DataContract]
    public class DataCambiarNombreCollectionModel
    {
        [DataMember]
        public LinksModel Links { get; set; }
        [DataMember]
        public CambiarNombreModel Data { get; set; }
        [DataMember]
        public RelationshipModel Relationships { get; set; }
    }
}
