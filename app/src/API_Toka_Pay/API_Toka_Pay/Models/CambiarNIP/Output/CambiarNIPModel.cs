﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.CambiarNIP.Output
{
    [DataContract]
    public class CambiarNIPModel
    {
        [DataMember]
        public string ProxyNumber { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public CambiarNIPAttributesModel Attributes { get; set; }
    }
}
