﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.CambiarNIP.Output
{
    [DataContract]
    public class CambiarNIPAttributesModel
    {
        [DataMember]
        public string Estado { get; set; }
    }
}
