﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.CambiarNIP.Input
{
    public class CambiarNIPInputModel
    {
        public string NIP { get; set; }
    }
}
