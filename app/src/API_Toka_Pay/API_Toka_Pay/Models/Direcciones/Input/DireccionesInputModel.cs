﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Direcciones.Input
{
    public class DireccionesInputModel
    {
        public int IdUsuario { get; set; }
        public string Calle { get; set; }
        public string Numeroexterior { get; set; }
        public string NumeroInterior { get; set; }
        public int IdDireccion { get; set; }
    }
}
