﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Direcciones.Output
{
    [DataContract]
    public class DireccionesAttributesModel
    {
        [DataMember]
        public int IdDireccion { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public string Municipio { get; set; }
        [DataMember]
        public string Ciudad { get; set; }
        [DataMember]
        public string Colonia { get; set; }
        [DataMember]
        public string CodigoPostal { get; set; }
    }
}
