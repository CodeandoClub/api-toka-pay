﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Direcciones.Output
{
    [DataContract]
    public class DireccionesModel
    {
        [DataMember]
        public int IdDireccion { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public DireccionesAttributesModel Attributes { get; set; }
    }
}
