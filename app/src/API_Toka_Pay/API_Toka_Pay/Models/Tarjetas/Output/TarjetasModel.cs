﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Tarjetas.Output
{
    [DataContract]
    public class TarjetasModel
    {
        [DataMember]
        public string CuentaClabe { get; set; }
        [DataMember]
        public string ProxyNumber { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        [JsonProperty("atributes")]
        public TarjetasAttributesModel Attributes { get; set; }
    }
}
