﻿using API_Toka_Pay.Models.ResponsesCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Tarjetas.Output
{
    [DataContract]
    public class DataTarjetasCollectionModel
    {
        [DataMember]
        public LinksModel Links { get; set; }
        [DataMember]
        public TarjetasModel Data { get; set; }
        [DataMember]
        public RelationshipModel Relationships { get; set; }
    }
}
