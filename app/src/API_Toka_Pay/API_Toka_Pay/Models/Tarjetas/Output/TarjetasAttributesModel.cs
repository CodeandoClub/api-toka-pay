﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Tarjetas.Output
{
    [DataContract]
    public class TarjetasAttributesModel
    {
        [DataMember]
        public string ProxyNumber { get; set; }
        [DataMember]
        public int IdUsuario { get; set; }
        [DataMember]
        public decimal Saldo { get; set; }
        [DataMember]
        public int Estado { get; set; }
        [DataMember]
        public string NombreEmbosado { get; set; }
        [DataMember]
        public string CVV { get; set; }
        [DataMember]
        public string Vigencia { get; set; }
        [DataMember]
        public string NumeroTarjeta { get; set; }
        [DataMember]
        public bool Virtual { get; set; }
        [DataMember]
        public decimal Dispersiones { get; set; }
        [DataMember]
        public decimal Retiros { get; set; }
    }
}
