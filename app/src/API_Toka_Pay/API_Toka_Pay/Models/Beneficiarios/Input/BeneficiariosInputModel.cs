﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Beneficiarios.Input
{
    public class BeneficiariosInputModel
    {
        public int IdUsuario { get; set; }
        public string Correo { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public int? IdParentesco { get; set; }
        public string FechaNacimiento { get; set; }
        public string Direccion { get; set; }
        public string NumeroTelefono { get; set; }
    }
}
