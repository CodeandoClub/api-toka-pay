﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Beneficiarios.Output
{
    [DataContract]
    public class BeneficiariosAttributesModel
    {
        [DataMember]
        public int IdUsuario { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Paterno { get; set; }
        [DataMember]
        public string Materno { get; set; }
        [DataMember]
        public int IdParentesco { get; set; }
        [DataMember]
        public DateTime FechaNacimiento { get; set; }
        [DataMember]
        public string Correo { get; set; }
        [DataMember]
        public string NumeroTelefono { get; set; }
        [DataMember]
        public string Direccion { get; set; }
    }
}
