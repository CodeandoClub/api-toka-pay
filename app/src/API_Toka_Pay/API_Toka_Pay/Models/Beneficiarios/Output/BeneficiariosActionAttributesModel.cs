﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Beneficiarios.Output
{
    [DataContract]
    public class BeneficiariosActionAttributesModel
    {
        [DataMember]
        public string Estado { get; set; }
    }
}
