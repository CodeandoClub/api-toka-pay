﻿using API_Toka_Pay.Models.ResponsesCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Beneficiarios.Output
{
    [DataContract]
    public class BeneficiariosActionCollectionModel
    {
        [DataMember]
        public List<DataBeneficiariosActionCollectionModel> Data { get; set; }
        [DataMember]
        public List<IncluidedCollectionModel> Incluided { get; set; }
    }
}
