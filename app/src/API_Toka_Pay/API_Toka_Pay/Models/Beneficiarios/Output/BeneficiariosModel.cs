﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Beneficiarios.Output
{
    [DataContract]
    public class BeneficiariosModel
    {
        [DataMember]
        public int IdUsuario { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public BeneficiariosAttributesModel Attributes { get; set; }
    }
}
