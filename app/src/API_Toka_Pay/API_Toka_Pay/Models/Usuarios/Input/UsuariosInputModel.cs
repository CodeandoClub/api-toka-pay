﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Usuarios.Input
{
    public class UsuariosInputModel
    {
        public int IdUsuario { get; set; }
        public string PasswordAnterior { get; set; } = null;
        public string PasswordNuevo1 { get; set; }
        public string PasswordNuevo2 { get; set; }
    }
}
