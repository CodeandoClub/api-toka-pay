﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Usuario.Output
{
	[DataContract]
    public class UsuarioAttributesModel
    {
		[DataMember]
		public int IdUsuario { get; set; }
		[DataMember]
		public string Nombre { get; set; }
		[DataMember]
		public string Paterno { get; set; }
		[DataMember]
		public string Materno { get; set; }
		[DataMember]
		public DateTime? FechaNacimiento { get; set; }
		[DataMember]
		public string Correo { get; set; }
		[DataMember]
		public string NumeroTelefono { get; set; }
		[DataMember]
		public int? IdEstado { get; set; }
		[DataMember]
		public int? IdGenero { get; set; }
		[DataMember]
		public int? IdNacionalidad { get; set; }
		[DataMember]
		public int? IdActividadEconomica { get; set; }
		[DataMember]
		public int? IdEstadoUsuario { get; set; }
	}
}
