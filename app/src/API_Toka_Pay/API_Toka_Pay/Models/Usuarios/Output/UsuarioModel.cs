﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Usuario.Output
{
    [DataContract]
    public class UsuarioModel
    {
        [DataMember]
        public long IdUsuario { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public UsuarioAttributesModel Attributes { get; set; }
        [DataMember]
        public string Token { get; set; }
    }
}
