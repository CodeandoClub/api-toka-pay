﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.AsociacionesTarjetas.Input
{
    public class AsociacionesTarjetasInputModel
    {
        public int IdUsuario { get; set; }
        public string Tarjeta { get; set; }
        public string Vigencia { get; set; }
        public string CVV { get; set; }
    }
}
