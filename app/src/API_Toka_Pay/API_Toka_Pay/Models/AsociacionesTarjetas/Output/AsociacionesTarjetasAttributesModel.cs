﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.AsociacionesTarjetas.Output
{
    [DataContract]
    public class AsociacionesTarjetasAttributesModel
    {
        [DataMember]
        public int IdUsuario { get; set; }
        [DataMember]
        public string Tarjeta { get; set; }
        [DataMember]
        public string Estado { get; set; }
    }
}
