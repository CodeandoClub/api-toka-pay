﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.AsociacionesTarjetas.Output
{
    [DataContract]
    public class AsociacionesTarjetasModel
    {
        [DataMember]
        public string Tarjeta { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        [JsonProperty("atributes")]
        public AsociacionesTarjetasAttributesModel Attributes { get; set; }
    }
}
