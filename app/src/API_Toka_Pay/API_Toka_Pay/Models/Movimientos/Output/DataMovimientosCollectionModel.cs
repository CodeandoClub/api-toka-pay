﻿using API_Toka_Pay.Models.ResponsesCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Movimientos.Output
{
    [DataContract]
    public class DataMovimientosCollectionModel
    {
        [DataMember]
        public LinksModel Links { get; set; }
        [DataMember]
        public MovimientosModel Data { get; set; }
        [DataMember]
        public RelationshipModel Relationships { get; set; }
    }
}
