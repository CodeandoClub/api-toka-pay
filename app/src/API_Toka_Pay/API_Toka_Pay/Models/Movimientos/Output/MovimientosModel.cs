﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Movimientos.Output
{
    [DataContract]
    public class MovimientosModel
    {
        [DataMember]
        public string ProxyNumber { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        [JsonProperty("atributes")]
        public MovimientosAttributesModel Attributes { get; set; }
    }
}
