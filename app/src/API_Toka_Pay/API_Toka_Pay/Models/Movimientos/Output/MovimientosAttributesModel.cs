﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Movimientos.Output
{
    [DataContract]
    public class MovimientosAttributesModel
    {
        [DataMember]
        public string CARD_LAST_FOUR_DIGITS { get; set; }
        [DataMember]
        public string CLIENT_NAME { get; set; }
        [DataMember]
        public string COD_AUTH { get; set; }
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string MERCHANT_CITY { get; set; }
        [DataMember]
        public string MERCHANT_NAME { get; set; }
        [DataMember]
        public string OPERATION_CODE { get; set; }
        [DataMember]
        public decimal ORIGINAL_AMOUNT { get; set; }
        [DataMember]
        public string TRANSACTION_DATE { get; set; }
        [DataMember]
        public string TRANSACTION_STATE { get; set; }
        [DataMember]
        public string RUBRO { get; set; }
        [DataMember]
        public string PROXYNUMBER { get; set; }
    }
}
