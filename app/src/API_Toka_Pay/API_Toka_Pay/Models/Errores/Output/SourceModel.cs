﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Errores.Output
{
    public class SourceModel
    {
        public string Parameter { get; set; }
    }
}
