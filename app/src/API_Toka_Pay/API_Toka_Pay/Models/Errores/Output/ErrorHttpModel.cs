﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Errores.Output
{
    public class ErrorHttpModel
    {
        public int IdError { get; set; }
        public string Links { get; set; }
        public string Status { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public Dictionary<string, string> Detail { get; set; }
        public SourceModel Source { get; set; }
    }
}
