﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.EstadosUsuario.Input
{
    [DataContract]
    public class EstadosUsuarioInputModel
    {
        [DataMember]
        public int IdEstadoUsuario { get; set; }
    }
}
