﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.EstadosUsuario.Output
{
    [DataContract]
    public class EstadosUsuarioModel
    {
        [DataMember]
        public long IdUsuario { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public EstadosUsuarioAttributesModel Attributes { get; set; }
    }
}
