﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.EstadosUsuario.Output
{
    public class EstadosUsuarioAttributesModel
    {
        public string NumeroTelefono { get; set; }
        public string Estado { get; set; }
        public int IdEstadoUsuario { get; set; }
    }
}
