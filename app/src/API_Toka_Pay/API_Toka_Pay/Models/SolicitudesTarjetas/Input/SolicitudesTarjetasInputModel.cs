﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.SolicitudesTarjetas.Input
{
    public class SolicitudesTarjetasInputModel
    {
        public int IdUsuario { get; set; }
        public bool Virtual { get; set; }
        public string NombreEmbosado { get; set; } = null;
        public string Calle { get; set; } = null;
        public string Exterior { get; set; } = null;
        public string Interior { get; set; } = null;
        public int? IdDireccion { get; set; } = null;
    }
}
