﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.SolicitudesTarjetas.Output
{
    [DataContract]
    public class SolicitudesTarjetasModel
    {
        [DataMember]
        public int IdSolicitud { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        [JsonProperty("atributes")]
        public SolicitudesTarjetasAttributesModel Attributes { get; set; }
    }
}
