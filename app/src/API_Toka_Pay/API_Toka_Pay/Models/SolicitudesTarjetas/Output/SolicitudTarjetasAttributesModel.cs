﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.SolicitudesTarjetas.Output
{
    [DataContract]
    public class SolicitudesTarjetasAttributesModel
    {
        [DataMember]
        public string ProxyNumber { get; set; }
        [DataMember]
        public int IdUsuario { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public int IdSolicitud { get; set; }
        [DataMember]
        public bool Virtual { get; set; }
    }
}
