﻿using API_Toka_Pay.Models.ResponsesCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.SolicitudesTarjetas.Output
{
    [DataContract]
    public class SolicitudesTarjetasCollectionModel
    {
        [DataMember]
        public List<DataSolicitudesTarjetasCollectionModel> Data { get; set; }
        [DataMember]
        public List<IncluidedSolicitudesTarjetasCollectionModel> Incluided { get; set; }
    }
}
