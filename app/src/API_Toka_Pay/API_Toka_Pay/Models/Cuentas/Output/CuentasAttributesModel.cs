﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Cuentas.Output
{
    [DataContract]
    public class CuentasAttributesModel
    {
        [DataMember]
        public int IdUsuario { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public string CURP { get; set; }
        [DataMember]
        public string CuentaClabe { get; set; }
        [DataMember]
        public decimal Ingresos { get; set; }
        [DataMember]
        public decimal Gastos { get; set; }
    }
}
