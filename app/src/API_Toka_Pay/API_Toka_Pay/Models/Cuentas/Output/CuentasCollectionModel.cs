﻿using API_Toka_Pay.Models.ResponsesCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Cuentas.Output
{
    [DataContract]
    public class CuentasCollectionModel
    {
        [DataMember]
        public List<DataCuentasCollectionModel> Data { get; set; }
        [DataMember]
        public List<IncluidedCuentasCollectionModel> Incluided { get; set; }
    }
}
