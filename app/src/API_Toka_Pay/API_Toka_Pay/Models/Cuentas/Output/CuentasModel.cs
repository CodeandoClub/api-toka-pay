﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Cuentas.Output
{
    [DataContract]
    public class CuentasModel
    {
        [DataMember]
        public int IdUsuario { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        [JsonProperty("atributes")]
        public CuentasAttributesModel Attributes { get; set; }
    }
}
