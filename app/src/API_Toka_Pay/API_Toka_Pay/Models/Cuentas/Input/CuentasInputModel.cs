﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Cuentas.Input
{
    public class CuentasInputModel
    {
        public int IdUsuario { get; set; }
        public string Nombres { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public string FechaNacimiento { get; set; }
        public string EstadoNacimiento { get; set; }
        public string Sexo { get; set; }
    }
}
