﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.NivelesCuenta.Input
{
    public class NivelesCuentaInputModel
    {
        public string NumeroTelefono { get; set; }
        public int IdNivelCuenta { get; set; }
        public string IneFront { get; set; }
        public string IneBack { get; set; }
        public string Passport { get; set; }
        public string Selfie { get; set; }
    }
}
