﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.NivelesCuenta.Output
{
    [DataContract]
    public class NivelesCuentaModel
    {
        [DataMember]
        public string NumeroTelefono { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public NivelesCuentaAttributesModel Attributes { get; set; }
    }
}