﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.NivelesCuenta.Output
{
    [DataContract]
    public class NivelesCuentaAttributesModel
    {
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public string Mensaje { get; set; }
        [DataMember]
        public string TipoDocumento { get; set; }
    }
}
