﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Comisiones.Output
{
    [DataContract]
    public class ComisionesAttributesModel
    {
        [DataMember]
        public int IdComision { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Base { get; set; }
        [DataMember]
        public decimal Costo { get; set; }
        [DataMember]
        public string Tipo { get; set; }
        [DataMember]
        public bool IVA { get; set; }
    }
}
