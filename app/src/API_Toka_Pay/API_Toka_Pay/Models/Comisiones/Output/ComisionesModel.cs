﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace API_Toka_Pay.Models.Comisiones.Output
{
    [DataContract]
    public class ComisionesModel
    {
        [DataMember]
        public int IdComision { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public ComisionesAttributesModel Attributes { get; set; }
    }
}
