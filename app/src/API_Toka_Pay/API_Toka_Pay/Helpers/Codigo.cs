﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace API_Toka_Pay.Helpers
{
    public static class Codigo
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        #endregion

        #region Devuelve el código a 6 posiciones.
        public static string ObtenerCodigo()
        {
            string _Resp = string.Empty;

            try
            {
                // Generamos el código aleatorio
                Random generator = new Random();
                _Resp = generator.Next(0, 999999).ToString("D6");
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return _Resp;
        }
        #endregion

        #region Encriptar token
        public static string EncriptarToken(string str)
        {
            string _Resp = string.Empty;

            try
            {
                SHA256 sha256 = SHA256Managed.Create();
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] stream = null;
                StringBuilder sb = new StringBuilder();
                stream = sha256.ComputeHash(encoding.GetBytes(str));
                for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
                _Resp = sb.ToString();
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return _Resp;
        }
        #endregion
    }
}
