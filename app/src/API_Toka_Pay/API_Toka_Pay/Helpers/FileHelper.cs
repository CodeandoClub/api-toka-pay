﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay.Helpers
{
    public static class FileHelper
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        #endregion

        public static bool Save(byte[] file, string name, string path)
        {
            bool _Resp = false;

            try
            {
                // Obtememos la ruta para guardar
                var _Path = Path.Combine("C:\\Tokapay", path);

                // Validamos si existe el directorio
                if (!Directory.Exists(_Path))
                {
                    Directory.CreateDirectory(_Path);
                }

                // configuramos la ruta completa
                _Path = Path.Combine(_Path, name);

                using (Stream _File = File.OpenWrite(_Path))
                {
                    _File.Write(file, 0, file.Length);
                }

                _Resp = true;
            }
            catch(Exception e)
            {
                _Logger.Error(e);
            }

            return _Resp;
        }

        public static FileStream Load(string name, string path)
        {
            try
            {
                // Obtememos la ruta para guardar
                // var _Path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", name);
                var _Path = Path.Combine("C:\\Tokapay", path, name);

                return new FileStream(_Path, FileMode.Open, FileAccess.Read);
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return null;
        }
    }
}
