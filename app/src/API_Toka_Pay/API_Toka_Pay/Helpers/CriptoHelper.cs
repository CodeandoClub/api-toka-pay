﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace API_Toka_Pay.Helpers
{
    public static class CriptoHelper
    {
        #region Propiedades
        private static readonly log4net.ILog _Logger = log4net.LogManager.GetLogger(typeof(Program));
        #endregion

        #region Obtenemos la contraseña cifrada
        public static string ObtenerContraseniaCifrada(string contrasenia, out string salt)
        {
            string _PasswordHashed = string.Empty;

            try
            {
                _PasswordHashed = Crypto.SHA256(contrasenia);
                salt = Crypto.GenerateSalt();
                _PasswordHashed += salt;
                _PasswordHashed = Crypto.HashPassword(_PasswordHashed);
            }
            catch (Exception e)
            {
                _Logger.Error(e);

                salt = string.Empty;
            }

            return _PasswordHashed;
        }
        #endregion

        #region Validamos la contraseña del usuario
        public static bool ValidarContrasenia(string hashcontrasenia, string contrasenia, string salt)
        {
            return Crypto.VerifyHashedPassword(hashcontrasenia, Crypto.SHA256(contrasenia) + salt);
        }
        #endregion

        #region Encriptamos una cadena
        public static string Encriptar(string str)
        {
            string _StringEncriptado = string.Empty;

            try
            {
                SHA256 _SHA256 = SHA256Managed.Create();
                ASCIIEncoding _Encoding = new ASCIIEncoding();
                byte[] _Stream = null;
                StringBuilder _SB = new StringBuilder();
                _Stream = _SHA256.ComputeHash(_Encoding.GetBytes(str));
                for (int i = 0; i < _Stream.Length; i++)
                {
                    _SB.AppendFormat("{0:x2}", _Stream[i]);
                }

                _StringEncriptado = _SB.ToString();
            }
            catch (Exception e)
            {
                _Logger.Error(e);
            }

            return _StringEncriptado;
        }
        #endregion
    }
}
