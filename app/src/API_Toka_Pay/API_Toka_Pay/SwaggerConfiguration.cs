﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Toka_Pay
{
    public class SwaggerConfiguration
    {
        public const string EndpointDescription = "API Rest v1 (Description)";
        public const string EndpointUrl = "v1/swagger.json";
        public const string ContactName = "Toka Internacional";
        public const string ContactUrl = "http://toka.com.mx";
        public const string DocNameV1 = "v1";
        public const string DocInfoTitle = "API Rest";
        public const string DocInfoVersion = "v1";
        public const string DocInfoDescription = "API TokaPay - Orquestador";
    }
}
