﻿using ServiceAccounts.Class;
using System;
using Topshelf;

namespace ServiceAccounts
{
    class Program
    {
        static void Main(string[] args)
        {
            // Cuentas.Generar();
            
            HostFactory.Run(windowsService =>
            {
                windowsService.Service<ServiceExample>(s =>
                {
                    s.ConstructUsing(service => new ServiceExample());
                    s.WhenStarted(service => service.Start());
                    s.WhenStopped(service => service.Stop());
                });

                windowsService.RunAsLocalSystem();
                // windowsService.EnableServiceRecovery(r => r.RestartService(TimeSpan.FromSeconds(10)));
                windowsService.StartAutomatically();

                windowsService.SetDescription("Servicio generación de cuentas");
                windowsService.SetDisplayName("Servicio generación de cuentas");
                windowsService.SetServiceName("ServiceAccounts");
            });
        }

        class ServiceExample
        {
            private readonly int _Minutes = 1;
            System.Timers.Timer timer;

            public ServiceExample()
            {
                timer = new System.Timers.Timer(60000 * _Minutes) { AutoReset = true };
                timer.Elapsed += (sender, eventArgs) => 
                {
                    Logger.Log("Ciclo");

                    // Código a ejecutar cada cierto tiempo
                    Cuentas.Generar();
                };
            }

            public void Start()
            {
                Logger.Log("Starting");

                timer.Start();
            }

            public void Stop()
            {
                Logger.Log("Stopped");
                timer.Stop();
            }
        }
    }
}
