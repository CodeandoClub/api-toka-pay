﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ServiceAccounts.Class
{
    public static class Logger
    {
        private static string _logFileLocation = @"C:\\toka\\servicecuentas.txt";

        public static void Log(string logMessage)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(_logFileLocation));
            File.AppendAllText(_logFileLocation, DateTime.UtcNow.ToString() + " : " + logMessage + Environment.NewLine);
        }
    }
}
