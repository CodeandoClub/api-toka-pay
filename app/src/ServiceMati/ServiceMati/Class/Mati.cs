﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ServiceMati.Class
{
    public static class Mati
    {
        public static void Validar()
        {
            try
            {
                Task.Run(async () =>
                {
                    var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");

                    var _Configuration = builder.Build();

                    // Enviamos el código generico
                    using (var client = new HttpClient())
                    {
                        string _Path = Path.Combine(_Configuration["url"], _Configuration["endpoint"]).Replace("\\", "/");

                        var response = await client.GetAsync(_Path);

                        var contents = await response.Content.ReadAsStringAsync();

                        if (response.IsSuccessStatusCode)
                        {
                            Logger.Log("Las cuentas se validaron con exito.");
                        }
                        else
                        {
                            Logger.Log("Error al validar cuentas: " + contents.ToString());
                        }
                    }
                }).Wait();
            }
            catch (Exception e)
            {
                Logger.Log(e.Message);
            }
        }
    }
}
